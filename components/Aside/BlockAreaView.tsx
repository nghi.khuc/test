/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {
  useEffect, useState, useMemo, useCallback,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setPosition, setItemCodeActive } from 'stores/slices/FilmSlice';
import isEmpty from 'lodash/isEmpty';
import { getListFilmTopMv, getListFilmTopTv } from '~/services/client';
import { fetchFilmTopViewMovie, fetchFilmTopViewTV } from '~/stores/slices/TopViewSlices';
import { TOP_VIEW, TYPE_VIEWS } from '~/utils/contains';
import ListTop from './ListTop';
import { useGetIsMobile, useScreenSize } from '~/utils/useScreenSize';

const TOP_VIEW_LIST = [
  { id: 'top-viewed-day', type: TOP_VIEW.DAY, name: 'Day' },
  { id: 'top-viewed-week', type: TOP_VIEW.WEEK, name: 'Week' },
  { id: 'top-viewed-month', type: TOP_VIEW.MONTH, name: 'Month' },
  { id: 'top-viewed-top', type: TOP_VIEW.TOP, name: 'Top' },
];

const mappingType = {
  [TOP_VIEW.TOP]: 'top',
  [TOP_VIEW.DAY]: 'day',
  [TOP_VIEW.WEEK]: 'week',
  [TOP_VIEW.MONTH]: 'month',
};

const initItem = {
  poster: '/images/black-slider.jpeg',
  backdrop: '/images/black-slider.jpeg',
  views: '10',
};
interface IProps {
  title: string,
  typeList: 'movies' | 'tv-series',
  isMobileSsr: boolean
}

const defaultList = Array(10).fill(initItem);

function BlockAreaView({ title, typeList, isMobileSsr }: IProps) {
  const { isMobile: isMobileCsr } = useScreenSize();
  const isMobile = useGetIsMobile(isMobileSsr, isMobileCsr);
  const dispatch = useDispatch();
  const [tabActive, setTabActive] = useState(TOP_VIEW.DAY);
  const [pageMovie, setPageMovie] = useState(1);
  const {
    listTopViewTvs, listTopViewMV, fetchedMovie, fetchedTV,
  } = useSelector((state: any) => ({
    listTopViewTvs: state.topViewSlices.tvs,
    listTopViewMV: state.topViewSlices.movies,
    fetchedMovie: state.topViewSlices.fetchedMovie,
    fetchedTV: state.topViewSlices.fetchedTV,
  }));

  const onMouseLeave = useCallback(() => {
    dispatch(setPosition({}));
    dispatch(setItemCodeActive({}));
  }, []);

  const fetchListFilm = async (type: any) => {
    if (typeList === TYPE_VIEWS.MOVIES) {
      if (!isEmpty(listTopViewMV[mappingType[type]])) return;
      dispatch(fetchFilmTopViewMovie({ type }));
    } else {
      if (!isEmpty(listTopViewTvs[mappingType[type]])) return;
      dispatch(fetchFilmTopViewTV({ type }));
    }
  };

  const onChangTab = (value) => () => {
    setPageMovie(1);
    setTabActive(value);
    fetchListFilm(value);
  };

  useEffect(() => {
    fetchListFilm(TOP_VIEW.DAY);
  }, []);

  const tab = useMemo(() => mappingType[tabActive], [tabActive]);

  const listMovie = useMemo(() => (fetchedMovie ? listTopViewMV[tab][pageMovie] : defaultList), [fetchedMovie, tabActive, pageMovie]);
  const listTv = useMemo(() => (fetchedTV ? listTopViewTvs[tab][pageMovie] : defaultList), [fetchedTV, tabActive, pageMovie]);

  const renderListMovie = useMemo(() => <ListTop page={pageMovie} listFilms={listMovie} />, [listMovie, tabActive, pageMovie]);
  const renderListTv = useMemo(() => <ListTop page={pageMovie} listFilms={listTv} />, [listTv, tabActive, pageMovie]);

  const onChangePage = (page) => {
    if (page && typeof page === 'number') {
      setPageMovie(page);
      if (typeList === TYPE_VIEWS.MOVIES) {
        if (isEmpty(listTopViewMV[tab][page])) {
          dispatch(fetchFilmTopViewMovie({ type: tabActive, page }));
        }
      } else if (isEmpty(listTopViewTvs[tab][page])) {
        dispatch(fetchFilmTopViewTV({ type: tabActive, page }));
      }
    }
  };

  const renderPaginate = ({ lastPage = 1 }) => (
    <div className="anime-pagination mt-2 mb-2 pb-2">
      <div className="ap_-nav">
        <div
          style={isMobile ? { width: '40%' } : {}}
          className="ap__-btn ap__-btn-prev"
          onClick={() => onChangePage(pageMovie <= 1 ? null : +pageMovie - 1)}
        >
          <a
            style={{ paddingLeft: 10, paddingRight: 10 }}
            className={`btn btn-sm btn-focus more-padding ${pageMovie <= 1 ? 'disabled' : ''}`}
          >
            <i className="fas fa-angle-left" />
          </a>
        </div>
        <div className="">
          <div className="btn btn-sm btn-blank" style={{ color: '#fff' }}>
            {`${pageMovie} of ${lastPage}`}
          </div>
        </div>
        <div
          style={isMobile ? { width: '40%' } : {}}
          className="ap__-btn ap__-btn-next"
          onClick={() => onChangePage(+pageMovie >= lastPage ? null : +pageMovie + 1)}
        >
          <a
            style={{ paddingLeft: 10, paddingRight: 10 }}
            className={`btn btn-sm btn-focus more-padding ${+pageMovie >= lastPage ? 'disabled' : ''}`}
          >
            <i className="fas fa-angle-right" />
          </a>
        </div>
      </div>
    </div>
  );
  return (
    <React.Fragment>
      <section className="block_area block_area_sidebar block_area-realtime">
        <div className="block_area-header">
          <div className="float-left bah-heading mr-2">
            <h2 className="cat-heading">{title}</h2>
          </div>
          <div className="float-right bah-tab-min">
            <ul className="nav nav-pills nav-fill nav-tabs anw-tabs">
              {TOP_VIEW_LIST.map((item) => (
                <li onClick={onChangTab(item.type)} key={item.id} className="nav-item">
                  <a data-toggle="tab" href={`#${item.id}`} className={`nav-link ${item.type === tabActive ? 'active' : ''}`}>{item.name}</a>
                </li>
              ))}
            </ul>
          </div>
          <div className="clearfix" />
        </div>
        <div className="block_area-content">
          <div className="cbox cbox-list cbox-realtime">
            <div className="cbox-content">
              <div className="tab-content">
                {TOP_VIEW_LIST.map((item) => (
                  <div
                    key={item.id}
                    id={item.id}
                    className={`anime-block-ul anif-block-chart tab-pane ${item.type === tabActive ? 'active' : ''}`}
                    onMouseLeave={onMouseLeave}
                  >
                    <ul className="ulclear">
                      {typeList === 'movies' ? renderListMovie : renderListTv}
                    </ul>
                  </div>
                ))}

                <div className="clearfix" />
              </div>
              <div className="clearfix" />
            </div>
          </div>
        </div>
        {
          typeList === 'movies'
            ? renderPaginate(listTopViewMV[tab])
            : renderPaginate(listTopViewTvs[tab])
        }
      </section>
    </React.Fragment>
  );
}

export default BlockAreaView;
