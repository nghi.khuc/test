

const imgDomains = [
  'image.tmdb.org',
  'i.imgur.com'
]
module.exports = {
  webpack5: true,
  reactStrictMode: false,
  publicRuntimeConfig: {
    // Will be available on both server and client
    env: process.env.ENV || 'development',
  },
  images: {
    domains: imgDomains,
  },
};