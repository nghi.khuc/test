/* eslint-disable global-require */
/* eslint-disable react/prop-types */
// import { CKEditor } from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import React, { useState, useEffect, useRef } from 'react';
import debounce from 'lodash/debounce';
import { useRouter } from 'next/router';
import {
  getConfigContent, postConfigContent, postConfigAds, getConfigAds,
} from '~/services/client';
// import './styles.css';

const Editor = ({
  onChange, editorLoaded, name, value,
}) => {
  const editorRef = useRef<any>();
  const { CKEditor, ClassicEditor } = editorRef.current || {};

  useEffect(() => {
    editorRef.current = {
      CKEditor: require('@ckeditor/ckeditor5-react').CKEditor, // v3+
      ClassicEditor: require('@ckeditor/ckeditor5-build-classic'),
    };
  }, []);

  return (
    <div>
      {editorLoaded ? (
        <CKEditor
          type=""
          name={name}
          editor={ClassicEditor}
          data={value}
          onChange={(event, editor) => {
            const data = editor.getData();
            onChange(data);
          }}
        />
      ) : (
        <div>Editor loading</div>
      )}
    </div>
  );
};
interface Iprop {
  type: number | string,
  title: number | string,
  isAds: boolean | undefined,
}
export default function App({ type, title, isAds }: Iprop) {
  const router = useRouter();
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [data, setData] = useState('');

  const fetchConfig = async () => {
    if (isAds) {
      const responseConfig = await getConfigAds({ type });
      setData(responseConfig?.content);
      return;
    }
    const responseConfig = await getConfigContent({ type });
    setData(responseConfig?.content);
  };

  useEffect(() => {
    fetchConfig();
  }, []);

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  const changeDataDebounce = debounce((value) => {
    setData(value);
  }, 300);

  const submitData = () => {
    if (isAds) {
      postConfigAds({ content: data }, type);
      router.push('/admin');
      return;
    }
    postConfigContent({ content: data }, type);
    router.push('/admin');
  };
  return (

    <div className="App">
      <h1>{title}</h1>
      <Editor
        name="description"
        onChange={changeDataDebounce}
        editorLoaded={editorLoaded}
        value={data}
      />
      <div className="form-group">
        <div className="mt-5" />
        <button onClick={submitData} type="submit" id="btn-focus" className="btn btn-block btn-lg btn-focus">Submit</button>
      </div>
    </div>
  );
}
