/* eslint-disable react/button-has-type */
import React from 'react';
import Container from 'components/Filter';

export function Home(props) {
  return (
    <Container {...props} />
  );
}

export default Home;
