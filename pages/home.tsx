/* eslint-disable react/button-has-type */
import React, { useEffect, useState } from 'react';
import { GetServerSideProps } from 'next';
import Container from 'containers/Home';
import { useRouter } from 'next/router';
import {
  getSliders, getListFilmsTVSeries, getListFilmsMoviesSeries, getConfigContent, getConfigAds,
} from '~/services/client';
import { redisGetAsync } from '~/utils/redis';
import { KEYS_REDIS } from '~/utils/config';
import { parseStringToObject } from '~/utils/common';
import { TYPE_ADS_PAGE } from '~/utils/contains';

interface IArray {

}

const initSlider = [
  {
    backdrop: '/images/black-slider.jpeg',
  },
];

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};
export function Home(props) {
  const router = useRouter();
  const [listFilmsTVs, setListFilmsTVs] = useState(() => Array(10).fill(initItem));
  const [listFilmMovies, setListFilmMovies] = useState([]);
  const [sliders, setSliders] = useState<IArray[]>(initSlider);

  const fetchSlider = async () => {
    const result = await getSliders();
    const newSliders = [...result?.data.tv || [], ...result?.data.movie || []];
    setSliders(newSliders);
  };

  const fetchTV = async () => {
    const responseFilmsTV = await getListFilmsTVSeries({
      page: 1, length: 20, orderby: 'update_at',
    });
    setListFilmsTVs(responseFilmsTV.data || []);
  };

  const fetchMove = async () => {
    const responseFilmsMovies = await getListFilmsMoviesSeries({
      page: 1, length: 20, orderby: 'update_at',
    });

    setListFilmMovies(responseFilmsMovies.data || []);
  };

  useEffect(() => {
    if (router) {
      fetchSlider();
      fetchTV();
      fetchMove();
    }
  }, [router]);

  return (
    <Container {...props} listFilmsTVs={listFilmsTVs} listFilmMovies={listFilmMovies} sliders={sliders} />
  );
}

export default Home;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const keyContentSLider = KEYS_REDIS.SLIDER_CONTENT;
  const keyAdsSlider = TYPE_ADS_PAGE.home_under_search.key;
  const keyMovieIndex = TYPE_ADS_PAGE.home_under_movie_recently_updated.key;
  const [contentSlider, adsSlider, contentUnderMovie] = await Promise.all([
    redisGetAsync(keyContentSLider),
    redisGetAsync(keyAdsSlider),
    redisGetAsync(keyMovieIndex),
  ]);
  const contentSliderParse = parseStringToObject(contentSlider);
  const adsSliderParse = parseStringToObject(adsSlider);
  const contentUnderMoviePage = parseStringToObject(contentUnderMovie);
  return {
    props: {
      query,
      contentUnderSlider: contentSliderParse?.content || '',
      adsSliderParse: adsSliderParse?.content || '',
      contentUnderMovie: contentUnderMoviePage?.content || '',
    },
  };
};
