/* eslint-disable react/button-has-type */
import React, { useEffect, useState } from 'react';
import Container from 'containers/MostPouplar';
import { useRouter } from 'next/router';
import { getListFilmsMoviesSeries, getListFilmsTVSeries } from '~/services/client';

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};

export function Home(props) {
  const router = useRouter();
  const [listFilms, setListFilm] = useState(() => Array(10).fill(initItem));
  const fetchFilms = async () => {
    const [responseFilmsTV, responseFilmsMovies] = await Promise.all([
      getListFilmsTVSeries({
        page: 1, length: 20, orderby: 'update_at',
      }),
      getListFilmsMoviesSeries({
        page: 1, length: 20, orderby: 'update_at',
      }),
    ]);
    setListFilm([
      ...responseFilmsTV.data, ...responseFilmsMovies.data,
    ]);
  };
  useEffect(() => {
    if (router) {
      fetchFilms();
    }
  }, [router]);

  return (
    <Container {...props} listFilms={listFilms} />
  );
}
export default Home;
