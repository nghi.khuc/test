/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import qs from 'qs';

const Paginate = ({ response = {}, setPage }:any) => {
  const router = useRouter();
  //   const [url] = router.asPath.split('?');

  //   const [pageState, setPage] = useState(1);

  const onChangePage = (event) => {
    setPage(event.target.value);
    // setPage(event.target.value);
    // window.location.href = `?page=${response?.page}`;
  };

  //   const getParam = (page) => qs.stringify({
  //     ...router.query,
  //     page,
  //   });

  const onSubmitSearchPage = () => {
    // router.push(`${url}?page=${pageState}`);
    // router.push(`${url}?${getParam(pageState)}`);
  };

  const handleNextPage = () => {
    const page = response.page || 1;
    if (page >= response.lastPage) return;
    setPage(page + 1);
    window.scrollTo({ top: 0, behavior: 'smooth' });
    // router.push(`${url}?page=${+response?.page + 1}`);
    // router.push(`${url}?${getParam(+response?.page + 1)}`);
  };

  const handlePrevPage = () => {
    const page = response.page || 1;
    if (page <= 1) return;
    setPage((response.page || 1) - 1);
    window.scrollTo({ top: 0, behavior: 'smooth' });
    // if (+response?.page - 1 === 1) {
    // //   router.push(`${url}?${getParam(undefined)}`);
    //   return;
    // }
    // router.push(`${url}?page=${+response?.page - 1}`);
    // router.push(`${url}?${getParam(+response?.page - 1)}`);
  };

  return (
    <div className="anime-pagination mt-2 mb-2">
      <div className="ap_-nav">
        <div className="ap__-btn ap__-btn-prev" onClick={handlePrevPage}>
          <a
            className={`btn btn-sm btn-focus more-padding ${response.page === 1 ? 'disabled' : ''}`}
          >
            <i className="fas fa-angle-left" />
            {' '}
            Previous
          </a>
        </div>
        <div className="ap__-input">
          <div className="btn btn-sm btn-blank">page</div>
          <input
            readOnly
            className="form-control form-control-dark input-page"
            defaultValue={response?.page}
            onChange={onChangePage}
            value={response.page || 1}
            // data-url={`${url}?page=`}
            data-toggle="tooltip"
            data-placement="top"
            title="Enter a number"
          />
          {/* <button onClick={onSubmitSearchPage} type="button" className="btn btn-sm btn-focus btn-go-page">go</button> */}
          <div className="btn btn-sm btn-blank">
            {`of ${response?.lastPage || 0}`}
          </div>
        </div>
        <div className="ap__-btn ap__-btn-next" onClick={handleNextPage}>
          <a className={`btn btn-sm btn-focus more-padding ${response.page >= response.lastPage ? 'disabled' : ''}`}>
            Next
            {' '}
            <i className="fas fa-angle-right" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Paginate;
