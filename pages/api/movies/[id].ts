import axios from 'axios';
import http from 'https';
import fs from 'fs';
import redis, {
  getCurrent, getMonthLeft, redisDelAsync, redisGetAsync, redisZincrbyAsync, timeCachingSystem,
} from '~/utils/redis';
import VaultURLs from '~/utils/contains';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import { normalizeFilms } from '.';
import { trackLog } from '~/services/server';

const normalizeFilm = ({
  _id, title, type, slug, content, rating, quality, release_date,
  release_year, views, keywords, description, duration, poster, backdrop, episode_new, hagtag_films,
  genres, countries, directors,
}:any = {}) => ({
  _id,
  title,
  type,
  slug,
  content,
  rating,
  quality,
  release_date,
  release_year,
  views,
  description,
  duration,
  poster,
  backdrop,
  keywords,
  hagtag_films,
  countries: countries?.map((country) => ({ id: country.id, name: country.name, slug: country.slug })) || [],
  genres: genres?.map((genre) => ({ id: genre.id, name: genre.name, slug: genre.slug })) || [],
  directors: directors?.map((director) => director.person) || [],
});

const upView = async (id) => {
  try {
    const currentDateString = getCurrent().format('DD/MM/YYYY');
    const monthLeft = getMonthLeft();
    const client = redis();
    const key = KEYS_REDIS.MOVIE_RANK_EXPIRE;
    const value = await redisGetAsync(key);
    if (!value) {
      client.set(key, 'oke', 'EX', monthLeft);
      client.expire(KEYS_REDIS.MOVIE_RANK, monthLeft);
    }

    await redisZincrbyAsync(KEYS_REDIS.MOVIE_RANK, 1, `${id}-${currentDateString}`);

    const baseUpView = `${VaultURLs.API_URL}/api/movies/${id}/view/update`;
    await axios.post(baseUpView);
    return true;
  } catch (e) {
    return false;
  }
};

export const getInfoMovieFilm = async (id, isUpView = false) => {
  const client = redis();
  const key = `${KEYS_REDIS.MOVIE}:${id}`;

  if (isUpView) {
    await upView(id);
  }

  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const resp = await axios.get(`${VaultURLs.API_URL}/api/movies/${id}`);

  const data = JSON.parse(JSON.stringify(normalizeFilm(resp.data.data)));

  const result = {
    data: {
      data,
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

export const getByMovieSimilar = async (key, id) => {
  const client = redis();

  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const baseURLSimilar = `${VaultURLs.API_URL}/api/movies/${id}/similar?length=15`;
  const resp = await axios.get(baseURLSimilar);

  const result = {
    data: {
      data: normalizeFilms(resp.data.data),
    },
  };
  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
  return result;
};

const setCaption = async (data) => {
  const files = [...new Set(data.map((item) => item.filename))];

  const client = redis();

  const value = await Promise.all(files.map(async (file) => {
    const is = await redisGetAsync(file);
    return is || file;
  }));

  const filesNotCache = value.filter((item) => item !== 'oke');

  if (!filesNotCache.length) return true;

  filesNotCache.map((file) => client.set(file, 'oke'));

  await Promise.all(filesNotCache.map((file) => {
    const prefix = data.find((item) => item.filename === file).name || '';
    return downSaveFile(file, prefix.toUpperCase());
  }));
  return true;
};

export const getByMovieStream = async (key, id, ip) => {
  const client = redis();

  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const baseURLStream = `${VaultURLs.API_URL}/api/movies/${id}/stream_urls`;
  const resp = await axios.get(baseURLStream);

  const baseURL = (url) => `${VaultURLs.IO_CODE_URL}/api/index.php?url=${url}&ip_client=${ip}`;

  const data = await Promise.all(resp.data.data.streamurls.map(async ({ url }) => {
    const rep = await axios.get(baseURL(url));
    return rep.data;
  }));

  const captions = resp.data.data.captions || [];
  await setCaption(captions);
  const result = {
    data: {
      other: { captions },
      data: resp.data.data.streamurls.map((stream, index) => ({ ...stream, url: (data[index] as any).data[0].file })),
    },
  };
  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
  return result;
};

export const downSaveFile = async (fileName, prefix) => {
  try {
    let folder = fileName.split('/');
    folder.pop();
    folder = folder.join('/');

    const createFolder = prefix ? `public/captions/${prefix}/${folder}` : `public/captions/${folder}`;
    const fileNameWrite = prefix ? `public/captions/${prefix}/${fileName}` : `public/captions/${fileName}`;

    const resp = await fs.promises.mkdir(createFolder, { recursive: true }).catch(() => 'error');

    if (resp === 'error') return false;

    return new Promise((res) => {
      const file = fs.createWriteStream(fileNameWrite, { encoding: 'utf8' });
      http.get(`${VaultURLs.API_URL}/${fileName}`, (response) => {
        response.pipe(file);
        file.on('error', () => {
          res(1);
          file.close();
        });
        file.on('finish', () => {
          res(1);
          file.close();
        });
      });
    });
  } catch (error) {
    return false;
  }
};

export const getInfoPerson = async (key, id) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);
  const baseURLSimilar = `${VaultURLs.API_URL}/api/movies/${id}/credits?limit=20`;

  const resp = await axios.get(baseURLSimilar);

  const result = {
    data: {
      data: resp.data.data,
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

const handler = async (req, res) => {
  try {
    const {
      id, toolTip: tooltip, type, ip,
    } = req.query;

    const configRequest = req.getStructureDynamicRouterServer('');

    const key = `${KEYS_REDIS.MOVIE}:${type}-${id}`;

    if (type === 'stream') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getByMovieStream(key, id, ip);
      return res.RESP(info);
    }

    if (type === 'similar') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getByMovieSimilar(key, id);
      return res.RESP(info);
    }

    if (type === 'person') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoPerson(key, id);
      return res.RESP(info);
    }

    const info = await getInfoMovieFilm(id, !tooltip);
    return res.RESP(info);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
