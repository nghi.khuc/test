/* eslint-disable indent */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useEffect, useMemo } from 'react';
import ListFilms from 'components/ListFilms';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import LazyLoad from 'react-lazyload';
import { omit } from 'lodash';
import qs from 'qs';
import { useSelector } from 'react-redux';
import Popup from 'reactjs-popup';
import ReactStars from 'react-stars';
import Details from '../Details';
import DetailCasts from '../DetailCasts';
import Comment from '../Comment';
import Movie from './watch';
import { getMoviesById, getURLIOCodes, postRating } from '~/services/client';
import { parseURLWatch, toastMessage } from '~/utils/common';

const WatchMovies = ({
  itemFilm, topInfoFilm, botInfoFilm, ...props
}: any) => {
  const [seasons, setSeasons] = useState<any>([]);
  const [captions, setCaptions] = useState<any>([]);
  const [similar, setSimilar] = useState<any>([]);
  const [streamUrls, setStreamUrls] = useState<any>({});
  const [mounted, setMounted] = useState<any>(false);
  const router = useRouter();
  const { query: { ss = 1 } } = router;
  const { ep = 1, sv } = parseURLWatch(router.asPath);
  const query: any = omit(router.query, 'slug');
  const id = useMemo(() => itemFilm?._id, []);
  const episode = parseInt((ep || '').toString());
  const [pathName] = decodeURI(router.asPath).split('?');

  const fetchApiUrlStream = async () => {
    if (mounted) {
      setStreamUrls(seasons[+sv - 1]);
    }
  };

  useEffect(() => {
    // window.history.replaceState(window.history.state,
    //   'option-default-movie-film',
    //   `${pathName}${pathName.includes('ep-full') ? '' : '/ep-full'}?${qs.stringify({ ...query })}`);
    fetchApiUrlStream();
  }, [sv]);

  const fetchApi = async () => {
    const [responseSeasons, responseSimilars] = await Promise.all([
      await getMoviesById(id, { type: 'stream' }),
      await getMoviesById(id, { type: 'similar' }),
    ]);
    setSeasons(responseSeasons.data);
    setCaptions(responseSeasons.captions);
    setSimilar(responseSimilars.data);
    if (responseSeasons.data) {
      setStreamUrls(responseSeasons.data[+sv - 1]);
      setMounted(true);
    }
  };

  useEffect(() => {
    fetchApi();
  }, []);

  const user = useSelector((state: any) => state.userSlices?.user);

  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const openModal = () => setOpen(true);

  const [ratingPoint, setRating] = React.useState();

  const rating = (score) => {
    if (user) {
      setRating(score);
      postRating({ id: itemFilm._id, type: itemFilm.type, score });
      setOpen(false);
      return true;
    }
    toastMessage({ isWarning: true, content: 'Please login' });
    setOpen(false);
    return false;
  };

  const renderComponent = useMemo(() => (
    <Movie
      {...props}
      streamUrls={streamUrls}
      captions={captions}
      seasons={seasons}
      itemFilm={itemFilm}
      episode={episode}
      ss={ss}
      sv={sv}
      openModal={openModal}
    />
  ), [ss, sv, itemFilm, episode, streamUrls]);

  return (
    <React.Fragment>
      <Popup open={open} onClose={closeModal}>
        <div className="wrapper-modal-rating">
          <div className="label">{'Rating Film: '}</div>
          <ReactStars
            count={10}
            half={false}
            onChange={rating}
            size={24}
            color2="#ffd700"
          />
        </div>
      </Popup>
      {renderComponent}
      <LazyLoad>
        {
          !!topInfoFilm && <div className="mba-block" dangerouslySetInnerHTML={{ __html: topInfoFilm }} />
        }
        {!isEmpty(itemFilm) && <Details detail={itemFilm} rating={ratingPoint} />}
        {!isEmpty(itemFilm) && <DetailCasts detail={itemFilm} />}
        {
          !!botInfoFilm && <div className="mba-block" dangerouslySetInnerHTML={{ __html: botInfoFilm }} />
        }
        <Comment />
        <ListFilms listFilms={similar} title="Suggestions" />
        {/** Block Tag */}
        <div className="block_area block_area-detail">
          <div className="block_area-content">
            <div className="film-infor">
              <div className="item">
                <div className="item-title">
                  {'Tags: '}
                  {
                    !!itemFilm?.keywords && itemFilm?.keywords.split(',').map((a) => (
                      <a key={a} href={`/tag/${a}`}>
                        {`${a}, `}
                      </a>
                    ))
                  }
                </div>
                <div className="clearfix" />
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </LazyLoad>
    </React.Fragment>
  );
};

export default WatchMovies;
