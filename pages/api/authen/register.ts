import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import VaultURLs from '~/utils/contains';
import {
  exception, HTTP_STATUS,
  // setCookieToken,
  withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/auth/register`;
    // const loginURL = `${VaultURLs.API_URL}/api/auth/login`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const { data } = configRequest;
    const {
      email, password, username,
      // ip_address,
    } = data;

    const resp = await axios({
      url: baseURL,
      method: 'POST',
      data: {
        email, password, username,
      },
    });

    // const resp = await axios({
    //   url: loginURL,
    //   method: 'POST',
    //   data: {
    //     email,
    //     password,
    //     ip_address,
    //   },
    // });
    // const tokenInfo = resp.data;
    // const { result } = tokenInfo;
    // setCookieToken({ req, res }, result);
    return res.RESP(resp);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
