import React from 'react';
import Image from 'next/image';
import isEmpty from 'lodash/isEmpty';
import styled from 'styled-components';
import { getCredits } from '~/services/client';

const Wrapper = styled.section`
  .wrapper-avatar {
    margin: 6px;
    display: inline-flex;
    & > div{
      margin-right: 16px !important;
    }
    .person-avatar {
      border-radius: 99px;
      object-fit: cover;
    }
  }
`;

const Details = ({ detail }: any) => {
  const [persons, setPerson] = React.useState({
    actors: [],
    other: [],
  });

  const fetchPersons = async () => {
    const resp: any = await getCredits({ type: detail.type, id: detail._id });
    if (!resp.data) return false;
    const actors = resp.data
      .filter((item) => item.job === 'actor')
      .map((item) => item.person);
    const other = resp.data
      .filter((item) => item.job !== 'actor')
      .map((item) => item.person);
    setPerson({ ...persons, actors, other });
    return true;
  };

  React.useEffect(() => {
    fetchPersons();
  }, []);
  return (
    !isEmpty(persons?.actors) ? (
      <Wrapper className="block_area block_area-detail">
        <div className="block_area-content">
          <div className="film-infor">
            <div className="item">
              <div className="item-title">Casts </div>
              <div className="item-content mt-3">
                {persons?.actors?.map((director: any) => (
                  <div key={director.id} className="wrapper-avatar" style={{ width: 220 }}>
                    <Image
                      className="person-avatar"
                      src={director?.avatar || '/images/black-slider.jpeg'}
                      width={24}
                      height={24}
                    />
                    {/* <img
                      alt=""
                      className="person-avatar"
                      src={director?.avatar}
                      style={{ width: 24, height: 24, marginRight: 6 }}
                    /> */}
                    <a href={`/user/${director?.slug}.${director?.id}`}>
                      {director?.name}
                    </a>
                  </div>
                ))}
              </div>
              <div className="clearfix" />
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </Wrapper>
    ) : null
  );
};

export default Details;
