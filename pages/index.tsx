/* eslint-disable react/button-has-type */
import React from 'react';
import Container from 'containers';
import { GetServerSideProps } from 'next';
import { KEYS_REDIS } from '~/utils/config';
import { redisGetAsync } from '~/utils/redis';
import { parseStringToObject } from '~/utils/common';
import { TYPE_ADS_PAGE } from '~/utils/contains';

export function Page(props) {
  return (
    <Container {...props} />
  );
}
export default Page;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const keyContentIndex = KEYS_REDIS.INDEX_CONTENT;
  const keyAdsIndex = TYPE_ADS_PAGE.under_search_index.key;
  const [contentIndex, adsIndex] = await Promise.all([
    redisGetAsync(keyContentIndex),
    redisGetAsync(keyAdsIndex),
  ]);
  const ContentIndexParse = parseStringToObject(contentIndex);
  const adsUnderSearchParse = parseStringToObject(adsIndex);
  return {
    props: {
      query,
      contentIndex: ContentIndexParse?.content || '',
      underSearch: adsUnderSearchParse?.content || '',
    },
  };
};
