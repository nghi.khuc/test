/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from 'react';
import uniq from 'lodash/uniq';
import ListFilms from 'components/ListFilms';
import QuickFilter from 'components/QuickFilter';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import omit from 'lodash/omit';
import { GetServerSideProps } from 'next';
import {
  getFilterListFilm, getListFilmsMoviesSeries,
} from '~/services/client';
import Paginate from '~/containers/AZList/paginate';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};

const FilterContainer = ({ type: TypeFilm, ...props }: any) => {
  const router = useRouter();
  const [url] = router.asPath.split('?');
  const pathName = decodeURI(url.split('/')[2]);
  const query = omit(router.query, 'slug');
  const [listFilms, setListFilm] = useState(() => Array(10).fill(initItem));
  const [paginate, setPaginate] = useState<any>({});
  const listMenus = useSelector((state: any) => state.menuSlices.listMenus);
  const { types } = listMenus;

  // update
  const newTypes = types?.map((type) => ({ ...type, slug: type.slug.replace('type/', '') }));

  const qtypes = newTypes?.filter((item) => item.slug.trim() === pathName).map((item) => item.id)?.join('');
  query.type = qtypes;
  const fetchListFilm = async () => {
    if (query.type) {
      // const getListFilm = await getFilterListFilm(query);
      const getListFilm = await getListFilmsMoviesSeries(query);
      setListFilm(getListFilm.data);
      setPaginate(getListFilm);
    }
  };

  useEffect(() => {
    fetchListFilm();
  }, [router, types]);

  return (
    <React.Fragment>
      {query.type ? (
        <QuickFilter {...props} query={{ types: qtypes }} isBlockSideBar={false} />
      ) : null}
      <ListFilms listFilms={listFilms} />
      <div className="clearfix" />
      <Paginate response={paginate} />
    </React.Fragment>
  );
};
export default FilterContainer;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;

  const titleKey = CONFIG_TITLE_DES_PAGE.type.key;
  const { keyReplace } = CONFIG_TITLE_DES_PAGE.type;

  const gereSlug = query.slug?.toString() || '';

  const doc = await redisGetValAsync(titleKey);
  const regex = new RegExp(`\\${keyReplace}`, 'g');
  const title = (doc?.title || '').replace(regex, gereSlug);
  const description = (doc?.description || '').replace(regex, gereSlug);
  const paramQuery = omit(query, 'slug');

  return {
    props: {
      query: paramQuery,
      title,
      description,
    },
  };
};
