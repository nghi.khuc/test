import React from 'react';
import { useRouter } from 'next/router';
import LazyLoad from 'react-lazyload';
import BlockList from './BlockAreaView';

function Aside(props) {
  const router = useRouter();
  const pathNameMovie = /(\/movies.*)/i.test(router.pathname);
  const pathNameTV = /(\/tv-series.*)/i.test(router.pathname);

  if (pathNameMovie) {
    return (
      <React.Fragment>
        <BlockList {...props} title="Movies" typeList="movies" />
      </React.Fragment>
    );
  }
  if (pathNameTV) {
    return (
      <React.Fragment>
        <BlockList {...props} title="TV" typeList="tv-series" />
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      <LazyLoad>
        <BlockList {...props} title="Movies" typeList="movies" />
        <BlockList {...props} title="TV" typeList="tv-series" />
      </LazyLoad>
    </React.Fragment>
  );
}

export default Aside;
