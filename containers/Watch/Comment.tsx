/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

const Comment = () => (
  <section className="block_area block_area-comment">
    <div className="block_area-header block_area-header-tabs">
      <div className="float-left bah-heading mr-4">
        <h2 className="cat-heading">Comment</h2>
      </div>
      <div className="float-right bah-tabs">
        <a href="#" data-type="episode" className="btn btn-sm btn-comment-tab active">
          Episode
          <span id="cm-episode-number" />
        </a>
        <a href="#" data-type="anime" className="btn btn-sm btn-comment-tab">Anime</a>
      </div>
      <div className="clearfix" />
    </div>
    <div className="block_area-content">
      <div id="disqus_thread" />
      <div className="block mb-2 text-center">
        <a href="#" className="btn btn-sm btn-long btn-focus more-padding btn-load-comment">
          Click
          to load comments
        </a>
      </div>
    </div>
  </section>
);

export default Comment;
