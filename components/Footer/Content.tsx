/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

export default function Content(props: any) {
  return (
    <div className="footer-content">
      <div className="footer-copyright float-left">
        <a href="" className="footer-logo">
          <img src={props?.config?.logo} alt="" />
        </a>
        <div className="copyright">Copyright © 9anime.vc. All Rights Reserved</div>
        <div className="socials-group">
          <div className="scg-list">
            {!!props?.config?.contacts?.discord && (
              <div className="item">
                <a
                  target="_blank"
                  href={props?.config?.contacts?.discord || 'https://twitter.com/9Animevc'}
                  className="sc-social-button dc-btn"
                  rel="noreferrer"
                >
                  <i className="fab fa-discord" />
                </a>
              </div>
            )}
            {!!props?.config?.contacts?.telegram && (
              <div className="item">
                <a
                  href={props?.config?.contacts?.telegram || '/#'}
                  className="sc-social-button tl-btn"
                >
                  <i className="fab fa-telegram-plane" />
                </a>
              </div>
            )}
            {!!props?.config?.contacts?.reddit && (
              <div className="item">
                <a
                  href={props?.config?.contacts?.reddit || '/#'}
                  className="sc-social-button rd-btn"
                >
                  <i
                    className="fab fa-reddit-alien"
                  />
                </a>
              </div>
            )}
            {!!props?.config?.contacts?.facebook && (
              <div className="item">
                <a
                  href={props?.config?.contacts?.facebook || '/#'}
                  className="sc-social-button tl-btn"
                >
                  <i className="fab fa-facebook" />
                </a>
              </div>
            )}
            {!!props?.config?.contacts?.twitter && (
              <div className="item">
                <a
                  href={props?.config?.contacts?.twitter || '/#'}
                  className="sc-social-button tl-btn"
                >
                  <i className="fab fa-twitter" />
                </a>
              </div>
            )}
            {!!props?.config?.contacts?.email && (
              <div className="item">
                <a
                  href={props?.config?.contacts?.email || '/#'}
                  className="sc-social-button rd-email"
                >
                  <i className="fas fa-envelope" />
                </a>
              </div>
            )}
          </div>
          <div className="clearfix" />
        </div>
        <div className="disclaimer small">
          <p dangerouslySetInnerHTML={{ __html: props?.config?.footerText }} />
          <p>
            Disclaimer: This site does not store any files on its server.
            All contents are provided by non-affiliated third parties.
          </p>
        </div>
        <div className="clearfix" />
      </div>
      <div className="footer-links float-left">
        <ul className="ulclear">
          <li className="title">Help</li>
          <li><a href="/contacts" title="">contacts</a></li>
          <li><a href="/faq" title="">FAQ</a></li>
        </ul>
        <ul className="ulclear">
          <li className="title">Links</li>
          <li><a href="/az-list" title="A-Z List">A-Z List</a></li>
          <li><a href="/filter?is_comingsoon=1" title="Upcoming">Upcoming</a></li>
          <li><a href="/most-popular" title="Most Popular">Most Popular</a></li>
        </ul>
        <div className="clearfix" />
      </div>
      <div className="clearfix" />
    </div>
  );
}
