/* eslint-disable no-underscore-dangle */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import * as SearchApi from 'services/client';

const initialState = {
  listMenus: {
    genres: [],
    types: [],
    country: [],
  },
};

export const fetchApiMenus = createAsyncThunk(
  'anime/menus',
  async () => {
    const response = await SearchApi.getMenus();
    return response.data;
  },
);

export const menuSlices = createSlice({
  name: 'menuSlices',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchApiMenus.fulfilled, (state, action) => {
      state.listMenus = action.payload;
    });
  },
});

// Action creators are generated for each case reducer function

export default menuSlices.reducer;
