/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react';
import { postAdminConfig, getAdminConfig } from '~/services/client';
import { withLogin } from '.';

function Contact() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [renderFirst, setRenderF] = useState(true);

  const [values, setValues] = useState({
    domain: '',
    logo: '',
    timeCacheContent: 0,
    timeCacheSetting: 0,
    contacts: {
      reddit: '',
      discord: '',
      telegram: '',
      twitter: '',
      facebook: '',
      email: '',
    },
    az: false,
    title: '',
    description: '',
    analytic: '',
    footerText: '',
  });

  const fetchConfig = async () => {
    const responseConfig = await getAdminConfig();
    setValues(responseConfig);
    setRenderF(false);
  };
  useEffect(() => {
    fetchConfig();
  }, []);

  const onSubmitValue = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [
        domainTarget,
        timeCacheContentTarget,
        timeCacheSettingTarget,
        logoTarget,
        emailTarget,
        facebookTarget,
        twitterTarget,
        telegramTarget,
        discordTarget,
        redditTarget,
        azTarget,
        titleTarget,
        descriptionTarget,
        analyticTarget,
        footerTextTarget,
      ] = event.target;
      const footerText = footerTextTarget.value as string;
      const analytic = analyticTarget.value as string;
      const description = descriptionTarget.value as string;
      const title = titleTarget.value as string;
      const az = azTarget.checked as string;
      const reddit = redditTarget.value as string;
      const discord = discordTarget.value as string;
      const telegram = telegramTarget.value as string;
      const twitter = twitterTarget.value as string;
      const facebook = facebookTarget.value as string;
      const email = emailTarget.value as string;
      const logo = logoTarget.value as string;
      const timeCacheSetting = timeCacheSettingTarget.value as string;
      const timeCacheContent = timeCacheContentTarget.value as string;
      const domain = domainTarget.value as string;
      const params = {
        domain,
        logo,
        timeCacheContent: timeCacheContent ? Number(timeCacheContent) : 0,
        timeCacheSetting: timeCacheSetting ? Number(timeCacheSetting) : 0,
        contacts: {
          reddit,
          discord,
          telegram,
          twitter,
          facebook,
          email,
        },
        az,
        title,
        description,
        analytic,
        footerText,
      };
      await postAdminConfig(params);
      router.push('/admin');
    } catch {
      console.error('post config failed');
    } finally {
      setLoading(false);
    }
  };

  if (renderFirst) return (<div>loading</div>);
  return (
    <div className="information_page">
      <div className="container">
        <div className="information_page-wrap">
          <div className="information_page-content">
            <h2 className="h2-heading">Config Admin</h2>
            <p />
            <div className="row">
              <div className="col-xl-6 col-lg-8 col-md-12">
                <form className="form-dark" onSubmit={onSubmitValue}>
                  <div className="form-group">
                    <label htmlFor="post-domain">Domain</label>
                    <input
                      defaultValue={values?.domain}
                      type="text"
                      className="form-control"
                      id="post-domain"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-time-cache-content">Time cache content</label>
                    <input
                      defaultValue={values?.timeCacheContent}
                      type="text"
                      className="form-control"
                      id="post-time-cache-content"
                      placeholder="minute"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-time-cache-setting">Time cache Setting</label>
                    <input
                      defaultValue={values?.timeCacheSetting}
                      type="text"
                      className="form-control"
                      id="post-time-cache-setting"
                      placeholder="minute"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-logo">URL Logo</label>
                    <input
                      defaultValue={values?.logo}
                      type="text"
                      className="form-control"
                      id="post-logo"
                      placeholder="https://image.dep.png"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-email-contact">Email Contact</label>
                    <input
                      defaultValue={values?.contacts?.email}
                      type="email"
                      className="form-control"
                      id="post-email-contact"
                      placeholder="ex@gmail.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-fb-contact">Facebook Contact</label>
                    <input
                      defaultValue={values?.contacts?.facebook}
                      type="text"
                      className="form-control"
                      id="post-fb-contact"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-twitter-contact">Twitter Contact</label>
                    <input
                      defaultValue={values?.contacts?.twitter}
                      type="text"
                      className="form-control"
                      id="post-twitter-contact"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-tele-contact">Telegram contact</label>
                    <input
                      defaultValue={values?.contacts?.telegram}
                      type="text"
                      className="form-control"
                      id="post-tele-contact"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-discord-contact">Discord contact</label>
                    <input
                      defaultValue={values?.contacts?.discord}
                      type="text"
                      className="form-control"
                      id="post-discord-contact"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-reddit-contact">Reddit contact</label>
                    <input
                      defaultValue={values?.contacts?.reddit}
                      type="text"
                      className="form-control"
                      id="post-reddit-contact"
                      placeholder="https://google.com"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-az-checkbox">AZ radio</label>
                    <input
                      defaultChecked={values?.az}
                      type="checkbox"
                      style={{ width: 10 }}
                      className="form-control"
                      id="post-az-checkbox"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-title-text">Title text</label>
                    <input
                      defaultValue={values?.title}
                      type="text"
                      className="form-control"
                      id="post-title-text"
                      placeholder="Một đoạn văn bản"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-description-text">Description text</label>
                    <input
                      defaultValue={values?.description}
                      type="text"
                      className="form-control"
                      id="post-description-text"
                      placeholder="Một đoạn văn bản"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-analytic-text">Analytic text</label>
                    <input
                      defaultValue={values?.analytic}
                      type="text"
                      className="form-control"
                      id="post-analytic-text"
                      placeholder="Một đoạn văn bản"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-footer-text">Footer text</label>
                    <input
                      defaultValue={values?.footerText}
                      type="text"
                      className="form-control"
                      id="post-footer-text"
                      placeholder="Một đoạn văn bản"
                    />
                  </div>
                  <div className="form-group">
                    <div className="mt-5" />
                    <button type="submit" id="btn-focus" className="btn btn-block btn-lg btn-focus">Submit</button>
                  </div>
                  <div className="form-group post-btn mb-0">
                    <div className="loading-relative" id="login-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withLogin(Contact);
