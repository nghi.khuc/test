import fs from 'fs';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { promisify } from 'util';
import { trackLog } from '~/services/server';
import configSystem from '~/utils/config';
import { TYPE_ADS_PAGE } from '~/utils/contains';
import redis, { redisGetAsync, timeCachingContent } from '~/utils/redis';
import { HTTP_STATUS, verifyCookieAdmin, withMiddleware } from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    const isAccept = verifyCookieAdmin(req);
    if (!configRequest || !isAccept) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const { type } = req.query;
    const item = Object.values(TYPE_ADS_PAGE).find((key) => key.type === +type);

    if (!item) return res.status(401).send('wrong');

    const { key, file } = item;

    if (req.method === 'GET') {
      const value = await redisGetAsync(key);
      if (value) return res.status(200).json(JSON.parse(value));
      return res.status(200).json({ content: '' });
    }
    if (req.method === 'POST') {
      const client = redis();
      const data = req.body;
      const dataString = JSON.stringify(data);
      client.set(key, dataString, 'EX', await timeCachingContent(configSystem.EXPIRE_TIME_CACHING));
      await promisify(fs.writeFile)(file, dataString);
      return res.status(200).json(data);
    }
    return res.status(401).send('wrong');
  } catch (e) {
    trackLog(req, e);
    return res.status(200).json({ content: '' });
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
