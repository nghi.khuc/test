import React from 'react';
import { useRouter } from 'next/router';
import CKEditor from '~/components/CKEditor';
import { withLogin } from '../..';

import { TYPE_CONTENT_PAGE } from '~/utils/contains';
import { convertStringUpperCaseFirstChar } from '~/utils/common';

function ContentPostPage() {
  const router = useRouter();
  const { query: { slug } }: any = router;
  const type = TYPE_CONTENT_PAGE[slug]?.type;
  const title = convertStringUpperCaseFirstChar(`Page - ${TYPE_CONTENT_PAGE[slug]?.key}`);
  return (
    <CKEditor title={title} type={type} isAds={false} />
  );
}

export default withLogin(ContentPostPage);
