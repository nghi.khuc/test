/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable react/button-has-type */
import React from 'react';
import Slider from 'components/Slider';
import ListFilms from 'components/ListFilms';
import Paginate from '../AZList/paginate';
import Schedule from '~/components/Schedules';

interface IProps {
  sliders: [],
  listFilmsTVs: [],
  paginate: any,
  contentUnderMovie: string | undefined,
  adsSliderParse: string | undefined,
  contentUnderSlider: string | undefined
}

const mackHTML = (str) => !!str && str.replace(/(<p>|<\/p>)/gi, '<div></div>');

const MovieComponent = ({
  sliders, listFilmsTVs, paginate, ...props
}: IProps) => (
  <React.Fragment>
    <Schedule {...props} />
    <div className="deslide-wrap">
      <Slider sliders={sliders} />
      {/** slider  */}
    </div>
    <div id="text-home">
      {!!props.contentUnderSlider && (
        <div className="alert alert-info">
          <div dangerouslySetInnerHTML={{ __html: mackHTML(props.contentUnderSlider) }} />
        </div>
      )}
    </div>
    <div id="text-home">
      {!!props.adsSliderParse && (
        <div dangerouslySetInnerHTML={{ __html: mackHTML(props.adsSliderParse) }} />
      )}
    </div>
    <ListFilms listFilms={listFilmsTVs} title="TV Series Recently Updated" />
    <Paginate response={paginate} />
    <div className="clearfix" />
    {
      !!props.contentUnderMovie && <span className="size-s" dangerouslySetInnerHTML={{ __html: props.contentUnderMovie }} />
    }
  </React.Fragment>
);
export default MovieComponent;
