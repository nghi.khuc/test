ACCOUNT_ADMIN; // /admin
SECRET_SESSION_ADMIN; // Secret key Admin Page
SECRET_SESSION; // Secret key Session All Page
SECRET_COOKIE; // Secret key Cookie Admin Page
SECRET_TOKEN; // Secret key Token Page
SECRET_REFRESH_TOKEN; // Secret key Refresh Token Page

EXPIRE_SESSION; // Expire Secret Session
EXPIRE_TOKEN; // Expire Secret Token
EXPIRE_REFRESH_TOKEN; // Expire Secret Refresh Token
EXPIRE_TIME_HAS_REMEMBER; // Expire Refresh Token and Token while Login has checked remember

EXPIRE_TIME_CACHING; // Expire default data BE
EXPIRE_TIME_TOP_VIEW; // Expire Topview data update views

EXPIRE_TIME_MENUS; // Expire Menus
-----------------------------------
Folder

static: Statics Page
- ads: Ads Content
- language/en: Content Page
- page-config.json: Config page
