import { useRouter } from 'next/router';
import React from 'react';
import CKEditor from '~/components/CKEditor';
import { TYPE_ADS_PAGE } from '~/utils/contains';
import { withLogin } from '..';
import { convertStringUpperCaseFirstChar } from '~/utils/common';

function Ads() {
  const router = useRouter();
  const { query: { slug } }: any = router;
  const type = TYPE_ADS_PAGE[slug]?.type;
  const title = convertStringUpperCaseFirstChar(`Page - ${TYPE_ADS_PAGE[slug]?.key}`);
  return (
    <CKEditor title={title} type={type} isAds />
  );
}

export default withLogin(Ads);
