/* eslint-disable react/button-has-type */
import React from 'react';
import Container from 'containers/AZList';
import { GetServerSideProps } from 'next';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';

export function AZListPage(props) {
  return (
    <Container {...props} />
  );
}
export default AZListPage;
