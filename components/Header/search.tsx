/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from 'react';
import { fetchFilmBySearch } from 'stores/slices/FilmSlice';
import { useDispatch, useSelector } from 'react-redux';
import debounce from 'lodash/debounce';
import { buildUrl } from '~/utils/common';

const SearchHeader = ({ showSearch }: any) => {
  const [loadingState, setLoading] = useState(false);
  const [displayModalSearch, setDisplayModalSearch] = useState('none');
  const dispatch = useDispatch();
  const listFilms = useSelector((state: any) => state.filmSlices.listFilms);

  const loading = useSelector((state: any) => state.filmSlices.loading);

  const onCallApiDebounce = debounce((value: any) => {
    setLoading(false);
    dispatch(fetchFilmBySearch(value));
  }, 500);

  const onSearchFilm = (event) => {
    // setLoading(true);
    event.preventDefault();
    let value = '';
    if (event.target[0]) {
      value = event.target[0].value;
    } else {
      value = event.target.value;
    }
    if (value) {
      window.location.href = `/search?keyword=${value}`;
    }
    // if (!value) return

    // onCallApiDebounce(value);
  };

  const onChangeTextSearchFilm = (event) => {
    setLoading(true);
    onCallApiDebounce(event.target.value);
  };

  const onBlur = (e) => {
    const { currentTarget } = e;
    setTimeout(() => {
      // Check if the new activeElement is a child of the original container
      if (!currentTarget.contains(document.activeElement)) {
        // You can invoke a callback or add custom logic here
        setDisplayModalSearch('none');
      }
    }, 0);
  };

  return (
    <div
      id="search"
      // tabIndex="0"
      onFocus={() => { setDisplayModalSearch('block'); }}
      className={showSearch ? 'active' : ''}
      onBlur={onBlur}
    >
      <div className="search-content">
        <form onSubmit={onSearchFilm} action="/search" autoComplete="off">
          <input
            onChange={onChangeTextSearchFilm}
            type="text"
            className="form-control search-input"
            name="keyword"
            placeholder="Enter anime name"
          />
          <button type="submit" className="search-icon"><i className="fas fa-search" /></button>
        </form>
        <div className="nav search-result-pop" id="search-suggest" style={{ display: displayModalSearch }}>
          <div className="loading-relative" id="search-loading" style={{ display: (loading || loadingState) ? 'block' : 'none' }}>
            <div className="loading">
              <div className="span1" />
              <div className="span2" />
              <div className="span3" />
            </div>
          </div>
          <div className="result">
            {listFilms.map((film, idx: number) => {
              const url = buildUrl(film);
              return (
                <a key={`${idx}`} href={`${url}`} className="nav-item">
                  <div className="film-poster">
                    <img data-src={film.poster} className="film-poster-img ls-is-cached lazyloaded" alt={film.original_title} src={film.poster} />
                  </div>
                  <div className="srp-detail">
                    <h3 className="film-name" data-jname="Maesetsu!">{film.original_title}</h3>
                    <div className="film-infor">
                      <span>{film.release_date}</span>
                      <i className="dot" />
                      {film.type}
                      <i className="dot" />
                      <span>{`${film.duration} min`}</span>
                    </div>
                  </div>
                  <div className="clearfix" />
                </a>

              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
export default SearchHeader;
