/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from 'react';
import Menu from './Menu';
import Logo from './Logo';
import Search from './search';
import GroupShareIcon from './GroupShareIcon';
import Login from './Login';

const Header = (props) => {
  const [showSearch, setShowSearch] = useState(false);
  const onShowSearchMobile = () => {
    setShowSearch(!showSearch);
  };
  return (
    <div id="header">
      <div className="container" style={{ height: '100%' }}>
        <Menu {...props} />
        <Logo {...props} />
        <Search {...props} showSearch={showSearch} />
        <GroupShareIcon {...props} />
        <Login {...props} onShowSearchMobile={onShowSearchMobile} />
      </div>
    </div>
  );
};

export default Header;
