import axios from 'axios';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import VaultURLs from '~/utils/contains';
import redis, { redisGetAsync, timeCachingSystem } from '~/utils/redis';
import { exception, withMiddleware } from '~/utils/run';

const handler = async (req, res) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/error/log/player`;
    const {
      body: data,
    } = req;

    const key = `${KEYS_REDIS.ERROR_FILM}:${data.movieType}-${data.movieID}`;
    const client = redis();
    const value = await redisGetAsync(key);
    if (value) return res.RESP('oke');

    await axios({
      url: baseURL,
      data,
      method: 'POST',
    });
    client.set(key, 'oke', 'EX', await timeCachingSystem(configSystem.EXPIRE_ERROR_FILM_CACHING));
    return res.RESP('oke');
  } catch (e) {
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
