/* eslint-disable no-throw-literal */
// import 'isomorphic-unfetch';
import axios from 'axios';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import qs from 'qs';

export default async function request({ url, options }) {
  const params = get(options, 'params', {});
  const method = get(options, 'method', 'GET').toUpperCase();
  const body = get(options, 'body', {});
  const headers = get(options, 'headers', {});
  let urlPath = url;

  urlPath += !isEmpty(params) ? `?${qs.stringify(params)}` : '';
  let optionsFinal:any = {
    url: urlPath,
    method,
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  if (isEqual(method, 'POST') || isEqual(method, 'PUT') || isEqual(method, 'PATCH')) {
    let data:any = {};
    if (optionsFinal.headers['Content-Type'] === 'application/json') {
      data = JSON.stringify(body);
    } else {
      data = qs.stringify(body);
    }

    optionsFinal = {
      ...optionsFinal,
      data,
    };
  }

  return axios(optionsFinal)
    .then((result) => result.data)
    .catch((err) => {
      console.log('err: ', err);
      const data = get(err, 'response.data');
      const message = get(data, 'message', '');

      const error_data = get(data, 'error_data', {});
      if (!error_data?.message) {
        error_data.message = message;
      }
      throw { error: error_data, axiosErr: err };
    }).catch((error) => {
      if (!error?.axiosErr?.response || typeof window === 'undefined') throw error;

      const { status, data } = error?.axiosErr?.response;
      if (status === 401 && data.code === '00000') {
        window.location.href = '/';
      }
      throw { error: { ...data }, axiosErr: error };
    });
}
