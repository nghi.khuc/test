/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import querystring from 'querystring';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import FLITER_SCHEME from './filterSchemes';

const convertValue = (data, defaultValue = '') => (isEmpty(data) ? defaultValue : data.map((item: any) => item).join(','));
function removeEmpty(obj) {
  if (isEmpty(obj)) return {};
  const newObj = {};
  Object.keys(obj).forEach((k) => {
    if (obj[k] && typeof obj[k] === 'object') {
      newObj[k] = removeEmpty(obj[k]);
    } else if (obj[k]) {
      newObj[k] = obj[k];
    }
  });
  return newObj;
}

function converParamsObj(obj) {
  if (isEmpty(obj)) return {};
  const newObj = {};
  Object.keys(obj).forEach((k) => {
    // const value = obj[k] && typeof obj[k] === 'string' && obj[k]?.split(',');
    const value = obj[k] && typeof obj[k] === 'string' ? obj[k]?.split(',') : obj[k];
    if (/\d+/.test(value) && Array.isArray(value)) {
      newObj[k] = value?.map((item) => +item);
    } else {
      newObj[k] = value;
    }
  });
  return newObj;
}

function QuickFilter({
  isBlockSideBar = true, query,
}: any) {
  const listFilterContent = useSelector((state: any) => state.menuSlices.listMenus);
  const newQuery: any = converParamsObj(query);

  const router = useRouter();
  const [typeSelectd, setTypeSelected] = useState('');
  const [objSelected, setObjSelected] = useState(() => ({
    genres: [],
    types: [],
    language: [],
    status: [],
    year: [],
    sort: [],
    country: [],
    option: ['movies'],
    ...newQuery,
  }));

  const onClickShowMenu = (type) => {
    if (typeSelectd === type) {
      setTypeSelected('');
      return;
    }
    setTypeSelected(type);
  };

  const toggleSelected = (item, type) => {
    if (type === 'radio') {
      setObjSelected({ ...objSelected, [typeSelectd]: [item.id] });
      return;
    }
    const newSelected = objSelected[typeSelectd]?.some((val) => val === item.id)
      ? objSelected[typeSelectd].filter((val) => val !== item.id)
      : [...objSelected[typeSelectd], item.id];
    setObjSelected({ ...objSelected, [typeSelectd]: newSelected });
  };

  const onSubmit = () => {
    setTypeSelected('');
    const params = {
      ...query,
      status: convertValue(objSelected.status),
      language: convertValue(objSelected.language),
      sort: convertValue(objSelected.sort),
      genres: convertValue(objSelected.genres),
      types: convertValue(objSelected.types),
      year: convertValue(objSelected.year),
      country: convertValue(objSelected.country),
      option: convertValue(objSelected.option),
      page: 1,
    };
    router.push(`/filter?${querystring.stringify(removeEmpty(params))}`);
    return true;
  };

  return (
    <section className={`block_area ${isBlockSideBar ? 'block_area_sidebar' : ''} block_area-filter`}>
      <div className="block_area-header" onClick={() => { setTypeSelected(''); }}>
        <div className="bah-heading">
          <h2 className="cat-heading">Quick filter</h2>
        </div>
      </div>
      <div className="block_area-content">
        <div className="sidebar-filter">
          {FLITER_SCHEME.map((filter: any, index: number) => {
            const filterType: any = filter.type;
            const dataFilter = listFilterContent[filterType] || filter.value || [];
            const dataSelected = objSelected[filter.type] || [];
            const dataFiltered = dataFilter.filter((item) => dataSelected.includes(item.id));
            const nameSelected = !isEmpty(dataFiltered)
              ? dataFiltered.length > 1
                ? ` ${dataFiltered.length} selected`
                : ` ${dataFiltered[0]?.name}`
              : '';
            const dataFilterLength = dataFilter.length;
            return (
              <div key={`${filter.type}_${index}`} className="item show">
                <div onClick={() => onClickShowMenu(filter.type)} aria-haspopup="true" aria-expanded="true" className="btn btn-sm btn-filter">
                  {filter.name}
                  <span>
                    <span id={`filter-${filter.id}-selected`}>
                      {nameSelected.length > 20 ? nameSelected.slice(0, 15).replace(/\w+$/, '...') : nameSelected}
                    </span>
                    <i className="fas fa-angle-down ml-1" />
                  </span>
                </div>
                <div
                  className={`
                  dropdown-menu dropdown-menu-native 
                  ${dataFilterLength < 4 ? 'dropdown-menu-medium' : 'dropdown-menu-large'} 
                  dropdown-menu-filter ${typeSelectd === filter.type ? 'show' : ''}`}
                >
                  <ul className={`ulclear ul-filter ${dataFilterLength <= 2 ? 'ul-2col' : dataFilterLength === 3 ? 'ul-3col' : 'ul-4col'}`}>
                    {dataFilterLength > 0 ? dataFilter.map((item: any, i: number) => (
                      <li key={`${filter.type}_${item?.id}`} id={`${item.id}-${i + 1}`}>
                        <div className={`acustom custom-control custom-${filter.opts.typeInput}`}>
                          <input
                            onChange={() => { toggleSelected(item, filter.opts.typeInput); }}
                            checked={dataSelected.some((selected) => selected == item.id)}
                            type={filter.opts.typeInput}
                            className={`custom-control-input ${filter.opts.typeInput}-${filter.type}`}
                            value={i + 1}
                            id={`input-${filter.type}-${i + 1}`}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor={`input-${filter.type}-${i + 1}`}
                          >
                            {item.name}
                          </label>
                        </div>
                      </li>
                    )) : null}
                  </ul>
                  <div className="clearfix" />
                </div>
              </div>
            );
          })}
          <div className="clearfix" />
        </div>
        <div className="filter-bottom">
          <div className="submit filter">
            <div
              onClick={onSubmit}
              className="btn btn-block btn-focus"
              id="btn-filter-submit"
            >
              <i className="fas fa-filter mr-1" />
              Filter
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default QuickFilter;
