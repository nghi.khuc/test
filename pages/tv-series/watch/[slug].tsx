import Container from 'containers/Watch/TVs';
import { GetServerSideProps } from 'next';
import React from 'react';
import { getInfoTVFilm } from '~/pages/api/tv-series/[id]';
import { parseStringToObject } from '~/utils/common';
import { KEYS_REDIS } from '~/utils/config';
import { TYPE_ADS_PAGE } from '~/utils/contains';
import { redisGetAsync } from '~/utils/redis';

function Watch(props) {
  return (
    <Container {...props} />
  );
}

export const getServerSideProps: GetServerSideProps = async (props) => {
  const { slug } = props.query;
  const [id] = (slug || '').toString().split('-').reverse();

  const [underPlayer, resp, topInfoFilm, botInfoFilm] = await Promise.all([
    redisGetAsync(TYPE_ADS_PAGE.watch_under_player.key),
    getInfoTVFilm(id, true),
    redisGetAsync(TYPE_ADS_PAGE.top_information_film.key),
    redisGetAsync(TYPE_ADS_PAGE.under_information_film.key),
  ]);
  const underPlayerParse = parseStringToObject(underPlayer);
  const topInfoFilmParse = parseStringToObject(topInfoFilm);
  const botInfoFilmParse = parseStringToObject(botInfoFilm);

  return {
    props: {
      title: resp.data.data.title,
      itemFilm: resp.data.data,
      underPlayer: underPlayerParse?.content || '',
      topInfoFilm: topInfoFilmParse?.content || '',
      botInfoFilm: botInfoFilmParse?.content || '',
    },
  };
};

export default Watch;
