/* eslint-disable react/button-has-type */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/prop-types */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { postLogout } from '~/services/client';
import { getInfoAction, postChangePassword } from '~/stores/slices/UserSlices';

const Page = () => {
  const dispatch = useDispatch();
  const user = useSelector((props:any) => props.userSlices?.user);

  React.useEffect(() => {
    dispatch(getInfoAction());
  }, []);

  const changePass = async (event) => {
    event.preventDefault();
    await dispatch(postChangePassword({
      password: event.target[7].value,
      new_password: event.target[8].value,
    }));

    // window.location.href = '/home';
  };

  return (
    <div className="profile-container">
      <div className="prc_-menu">
        <div className="prc__-icon"><img src={user?.avatar || '/images/user-icon.png'} alt="" /></div>
        <div className="prc__-list">
          <div className="item">
            <a href={`/person/${user?.fullname || user?.username}.${user?.id}`} className="btn active">Edit Profile</a>
          </div>
          <div className="item">
            <a href={`/person/${user?.fullname || user?.username}.${user?.id}/watch-list`} className="btn ">Watch List</a>
          </div>
          <div className="item">
            <a href={`/person/${user?.fullname || user?.username}.${user?.id}/mal`} className="btn ">MAL Import</a>
          </div>
          <div
            className="item"
            onClick={async () => {
              await postLogout();
              setTimeout(() => {
                window.location.href = '/home';
              }, 500);
            }}
          >
            <a href="#" className="btn">Logout</a>
          </div>
          <div className="clearfix" />
        </div>
      </div>
      <div className="prc_-main">
        <section className="block_area block_area-profile">
          <div className="block_area-content">
            <div className="content-padding">
              <form className="form-dark" method="post" id="profile-form" onSubmit={changePass}>
                <input type="hidden" name="settings[auto_next]" id="st-auto-next" defaultValue={1} />
                <input type="hidden" name="settings[auto_play]" id="st-auto-play" defaultValue={1} />
                <input type="hidden" name="settings[enable_dub]" id="st-enable-dub" defaultValue={1} />
                <input type="hidden" name="settings[play_original_audio]" id="st-play-original-audio" defaultValue={0} />
                <input type="hidden" name="settings[auto_load_comments]" id="st-auto-load-comments" defaultValue={0} />
                <h5>Account Information</h5>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Email</label>
                  <div className="col-sm-9">
                    <input type="text" className="form-control" defaultValue={user?.email} readOnly />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Your Name</label>
                  <div className="col-sm-9">
                    <input type="text" className="form-control" name="name" defaultValue={user?.fullname || user?.username} readOnly />
                  </div>
                </div>
                <h5 className="mt-5 mb-3">Change Password</h5>
                <p className="small">Leave password fields empty if you don't want to change.</p>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Password Old</label>
                  <div className="col-sm-9">
                    <input type="password" className="form-control" name="new_password" />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Password New</label>
                  <div className="col-sm-9">
                    <input type="password" className="form-control" name="confirm_new_password" />
                  </div>
                </div>
                <h5 className="mt-5 mb-3">Settings</h5>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Auto Next</label>
                  <div className="col-sm-9">
                    <div className="toggle-onoff off" id="sw-auto-next"><span className="on" /></div>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Auto Play</label>
                  <div className="col-sm-9">
                    <div className="toggle-onoff off" id="sw-auto-play"><span className="on" /></div>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Play Original Audio</label>
                  <div className="col-sm-9">
                    <div className="toggle-onoff off" id="sw-play-original-audio"><span className="on" /></div>
                    <div className="small mt-2">If enabled, the player will play original audio by default.</div>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Language for anime name</label>
                  <div className="col-sm-9">
                    <div className="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="lang-en" name="settings[anime_name]" defaultValue="en" defaultChecked className="custom-control-input" />
                      <label className="custom-control-label" htmlFor="lang-en">English</label>
                    </div>
                    <div className="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="lang-jp" name="settings[anime_name]" defaultValue="jp" className="custom-control-input" />
                      <label className="custom-control-label" htmlFor="lang-jp">Japanese</label>
                    </div>
                    <div className="small mt-2">
                      Select which language you prefer to display anime
                      names.
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-form-label col-sm-3 pt-0">Auto load comments</label>
                  <div className="col-sm-9">
                    <div className="toggle-onoff off" id="sw-auto-load-comments"><span className="on" /></div>
                  </div>
                </div>
                <div className="text-center mt-5">
                  <button className="btn more-padding btn-focus">Save Changes</button>
                  <div className="loading-relative" id="profile-loading" style={{ display: 'none' }}>
                    <div className="loading">
                      <div className="span1" />
                      <div className="span2" />
                      <div className="span3" />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
      <div className="clearfix" />
    </div>
  );
};

Page.PAGE_NOT_USE_ASIDE = true;

export default Page;
