/* eslint-disable jsx-a11y/anchor-is-valid */
import moment from 'moment';
import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import ReactStars from 'react-stars';
import { useSelector } from 'react-redux';
import {
  deleteFavoriteFilm, getCredits, getRating, postFavoriteFilm, postRating,
} from '~/services/client';
import { toastMessage } from '~/utils/common';

const Wrapper = styled.section`
  .wrapper-avatar {
    margin: 6px;
    display: inline-flex;
    & > div{
      margin-right: 16px !important;
    }
    .person-avatar {
      border-radius: 99px;
    }
  }
`;

const Details = ({ detail, rating: ratingProp }: any) => {
  const [persons, setPerson] = React.useState({
    actors: [],
    other: [],
  });

  const [checked, setChecked] = React.useState(false);

  const [ratingPoint, setRating] = React.useState(ratingProp);

  const user = useSelector((state: any) => state.userSlices?.user);

  const fetchPersons = async () => {
    const resp: any = await getCredits({ type: detail.type, id: detail._id });
    if (!resp.data) return false;
    const actors = resp.data
      .filter((item) => item.job === 'actor')
      .map((item) => item.person);
    const other = resp.data
      .filter((item) => item.job !== 'actor')
      .map((item) => item.person);
    setPerson({ ...persons, actors, other });
    return true;
  };

  const onClickAddFavorite = async () => {
    const paramsSubmit = { id: detail._id, type: detail.type };
    if (checked) {
      deleteFavoriteFilm(paramsSubmit);
      setChecked(false);
      toastMessage({ content: 'This anime has been removed to "Plan to watch" folder.' });
    } else {
      postFavoriteFilm(paramsSubmit);
      setChecked(true);
      toastMessage({ content: 'This anime has been added to "Plan to watch" folder.' });
    }
  };

  const fetchRating = async () => {
    if (user) {
      await getRating({ id: detail._id, type: detail.type });
    }
  };

  React.useEffect(() => {
    fetchRating();
  }, [user]);

  React.useEffect(() => {
    fetchPersons();
  }, []);

  const rating = (score) => {
    if (!ratingProp) {
      setRating(score);
      postRating({ id: detail._id, type: detail.type, score });
    }
  };

  return (
    <Wrapper className="block_area block_area-detail">
      <div className="block_area-content">
        <div className="anime-detail">
          <div className="anime-poster">
            <div className="film-poster">
              <img src={detail?.poster} alt="" className="film-poster-img" />
            </div>
          </div>
          <div className="film-infor">
            <div className="film-infor-top">
              <div id="vote-info" />
              <h2 className="film-name dynamic-name" style={{ display: 'flex', justifyContent: 'space-between' }} data-jname={detail?.title}>
                <span>
                  {detail?.title}
                </span>
                <span>
                  {!user ? (
                    <a data-toggle="modal" data-target="#modallogin" style={{ display: 'flex', alignItems: 'center' }}>
                      <ReactStars
                        value={ratingPoint || ratingProp}
                        count={10}
                        half={false}
                        onChange={() => null}
                        size={15}
                        color2="#ffd700"
                      />
                      <div style={{ position: 'unset' }} className="qtip-fav wl-item" data-id="8143" data-type="3" data-page="qtip"><i className="fas fa-plus" /></div>
                    </a>
                  ) : (
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <ReactStars
                        value={ratingPoint || ratingProp}
                        count={10}
                        half={false}
                        onChange={rating}
                        size={15}
                        color2="#ffd700"
                      />
                      <div
                        style={{ position: 'unset' }}
                        onClick={() => onClickAddFavorite()}
                        className="qtip-fav wl-item"
                        data-id="8143"
                        data-type="3"
                        data-page="qtip"
                      >
                        <i className={`fas ${(detail.has_favorite || checked) ? 'fa-check' : 'fa-plus'}`} />
                      </div>
                    </div>
                  )}
                </span>
              </h2>
              <div
                className="alias"
                dangerouslySetInnerHTML={{ __html: detail.content }}
              />
            </div>
            {/* {!!detail.description && (
              <div className="film-description">
                <p className="shorting">{detail.description}</p>
              </div>
            )} */}
            <div className="meta">
              <div className="col1">
                <div className="item">
                  <div className="item-title">Type:</div>
                  <div className="item-content">
                    <a href={`/${detail.type}`}>{detail.type}</a>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Date aired:</div>
                  <div className="item-content">
                    <a href={`/filter?year=${detail.release_year}&option=${detail.type}`}>
                      {moment(detail.release_date, 'DD/MM/YYYY').format('ll')}
                    </a>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Status:</div>
                  <div className="item-content">
                    <span>Finished Airing</span>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Genre:</div>
                  <div className="item-content">
                    {detail?.genres.map((genres: any) => (
                      <React.Fragment key={genres.slug}>
                        <a href={`/genre/${detail.type}/${genres.slug}`}>
                          {genres.name}
                        </a>
                        ,
                      </React.Fragment>
                    ))}
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Country:</div>
                  <div className="item-content">
                    {detail?.countries?.map((country: any) => (
                      <React.Fragment key={country.slug}>
                        <a href={`/country/${country.slug}`}>
                          {country.name}
                        </a>
                        ,
                      </React.Fragment>
                    ))}
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Hagtags:</div>
                  <div className="item-content">
                    {detail?.hagtag_films?.split(',').map((hagtag: any) => (
                      <React.Fragment key={hagtag}>
                        <a href={`/hagtag/${hagtag}`}>
                          {hagtag}
                        </a>
                        ,
                      </React.Fragment>
                    ))}
                  </div>
                  <div className="clearfix" />
                </div>
              </div>
              <div className="col2">
                <div className="item">
                  <div className="item-title">Scores:</div>
                  <div className="item-content">
                    <span>{(ratingPoint || ratingProp) ? ((+(ratingPoint || ratingProp) + detail.rating) / 2) : detail.rating}</span>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Duration:</div>
                  <div className="item-content">
                    <span>
                      {detail.duration}
                      {' '}
                      min/ep
                    </span>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Quality:</div>
                  <div className="item-content">
                    <span>{detail.quality}</span>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="item">
                  <div className="item-title">Views:</div>
                  <div className="item-content">
                    <span>{detail.views}</span>
                  </div>
                  <div className="clearfix" />
                </div>
              </div>
              <div className="clearfix" />
            </div>
            <div className="item">
              <div className="item-title">Directors: </div>
              <div className="item-content">
                {persons?.other?.map((director: any) => (
                  <div key={director.id} className="wrapper-avatar">
                    <Image
                      className="person-avatar"
                      src={director?.avatar || '/images/black-slider.jpeg'}
                      width={24}
                      height={24}
                    />
                    <a href={`/user/${director?.slug}.${director?.id}`}>
                      {director?.name}
                    </a>
                  </div>
                ))}
              </div>
              <div className="clearfix" />
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </Wrapper>
  );
};

export default Details;
