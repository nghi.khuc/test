import React from 'react';
import QuickFilter from '../QuickFilter';
import Aside from '../Aside';

const ContentComponent = ({
  children, isShowAside, isShowQuickFilter, config, ...props
}: any) => (
  <div id="main-wrapper">
    <div className="container">
      <div id="player-extend" className="" />
      {isShowAside ? (
        <React.Fragment>
          <div id="main-content">
            {children}
          </div>
          <div id="main-sidebar">
            {!!isShowQuickFilter && (
              <React.Fragment>
                {
                  !!config.contentTopFilter && <div className="mba-block" dangerouslySetInnerHTML={{ __html: config.contentTopFilter }} />
                }
                <QuickFilter {...props} />
                {
                  !!config.contentBotFilter && <div className="mba-block" dangerouslySetInnerHTML={{ __html: config.contentBotFilter }} />
                }
              </React.Fragment>
            )}
            <Aside {...props} />
          </div>
        </React.Fragment>
      ) : (children)}
      <div className="clearfix" />
    </div>
  </div>
);
export default ContentComponent;
