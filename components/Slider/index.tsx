import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';
import LazyLoad from 'react-lazyload';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import { buildUrl } from '~/utils/common';

const ContainerSlider = styled.div`
    .slick-prev, .slick-next{
      z-index: 1;
      opacity: 0;
      transition: .5s all;
      top:32%
    }
    .slick-prev {
      left: 0px;
    }
    .slick-next{
      right: 20px;
    }

    .slick-next:before, .slick-prev:before {
      font-size: 2rem;
      opacity: .75;
      padding: 35px 10px 40px 10px;
      background-color: #fff;
      color: #5a2e98;
    }

    .slick-next:before {
      content:'❯';
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }
    .slick-prev:before {
      content:'❮';
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }
  }
  &:hover{
      .slick-prev, .slick-next{
        opacity: 1;
      }
  }
`;

const replaceType = {
  movie: 'movies',
};

export const SliderComponent = ({ sliders }: any) => {
  const settings = {
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    speed: 500,
    arrows: true,
    adaptiveHeight: true,
    customPaging: (i) => (
      <div className="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
        <span className="swiper-pagination-bullet swiper-pagination-bullet" tabIndex={0} role="button" aria-label={`Go to slide ${i}`} />
      </div>
    ),
  };
  return (
    <ContainerSlider>
      <Slider {...settings}>
        {(sliders || []).map((item: any, index) => {
          const url = buildUrl(item);
          // const seasonNew = item?.season_new;
          // const type = replaceType[item.type] || item.type;
          // const url = `/${type}/${item?.slug}-${item?._id}?ep=1&sv=1${seasonNew && seasonNew > 1 ? `&ss=${seasonNew}` : ''}`;
          return (
            <React.Fragment
              key={index}
            >
              <div id="slider">
                <div className="swiper-wrapper">
                  <div className="swiper-slide swiper-slide-active">
                    <div className="deslide-item">
                      <div className="deslide-cover">
                        <div className="deslide-cover-img">
                          <a href={`${url}`}>
                            <img
                              className="film-poster-img"
                              src={item.backdrop}
                              title={item.title}
                              alt=""
                            />
                          </a>
                        </div>
                      </div>
                      <div className="deslide-item-content">
                        <div className="desi-head-title"><a href="watch">{item.title}</a></div>
                        <div className="desi-description">
                          {item.description}
                        </div>
                        <div className="desi-buttons">
                          <a href={`${url}`} className="btn">Watch now</a>
                        </div>
                      </div>
                      <div className="clearfix" />
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
          );
        })}
      </Slider>
    </ContainerSlider>
  );
};

export default SliderComponent;
