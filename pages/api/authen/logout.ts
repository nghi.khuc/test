import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import VaultURLs from '~/utils/contains';
import {
  clearCookieToken, exception, getCookieToken, HTTP_STATUS, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/auth/logout`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const data = getCookieToken({ req });
    if (data) {
      await axios({
        url: baseURL,
        method: 'POST',
        data: {
          refreshToken: data.refreshToken,
        },
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });
    }
    clearCookieToken({ req, res });
    return res.RESP('oke');
  } catch (e) {
    clearCookieToken({ req, res });
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
    return res.RESP('oke');
  }
};

export default withMiddleware(handler);
