/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from 'react';
import ListFilms from 'components/ListFilms';
import QuickFilter from 'components/QuickFilter';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import {
  getFilterListFilm,
  getHagtag,
} from '~/services/client';
import Paginate from '~/containers/AZList/paginate';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};

const HagTagContainer = ({ type: TypeFilm, ...props }: any) => {
  const router = useRouter();
  const [url] = router.asPath.split('?');
  const pathName = decodeURI(url.split('/')[2]);
  const query = { q: pathName };
  const [listFilms, setListFilm] = useState(() => Array(10).fill(initItem));
  const [paginate, setPaginate] = useState<any>({});
  const fetchListFilm = async () => {
    if (query.q) {
      const getListFilm = await getFilterListFilm({
        hagtags: query.q,
        is_commingsoon: false,
        orderby: 'created_at',
        length: 20,
        page: router.query.page || 1,
        view: true,
      });
      // const getListFilm = await getTag(query);
      setListFilm(getListFilm.data);
      setPaginate(getListFilm);
    }
  };
  useEffect(() => {
    fetchListFilm();
  }, [router]);

  return (
    <React.Fragment>
      {query.q ? (
        <QuickFilter query={query} {...props} isBlockSideBar={false} />
      ) : null}
      <ListFilms listFilms={listFilms} />
      <div className="clearfix" />
      {!!paginate?.page && (
        <Paginate response={paginate} />
      )}
    </React.Fragment>
  );
};
export default HagTagContainer;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;

  const titleKey = CONFIG_TITLE_DES_PAGE.hagtag.key;
  const { keyReplace } = CONFIG_TITLE_DES_PAGE.hagtag;

  const gereSlug = query.slug?.toString() || '';

  const doc = await redisGetValAsync(titleKey);
  const regex = new RegExp(`\\${keyReplace}`, 'g');

  const title = (doc?.title || '').replace(regex, gereSlug);
  const description = (doc?.description || '').replace(regex, gereSlug);

  return {
    props: {
      query,
      title,
      description,
    },
  };
};
