/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import { useRouter } from 'next/router';
import React from 'react';
import { URLs } from 'utils/contains';

interface IProps {
  contentIndex: string
  underSearch: string
  config: any
}

export default function IntroPage(props: IProps) {
  const router = useRouter();
  return (
    <React.Fragment>
      <div id="mw-top">
        <div className="container">
          <div className="mwt-content">
            <div className="mwh-logo">
              <a href="/" className="mwh-logo-div">
                <img src={props?.config?.logo} alt="" />
              </a>
              <div className="clearfix" />
            </div>
            <div className="show-share">
              <div className="addthis_inline_share_toolbox" />
            </div>
            <div id="xsearch" className="home-search">
              <div className="search-content">
                <div id="search-form">
                  <div className="search-submit">
                    <div
                      className="search-icon btn-search"
                      onClick={() => {
                        router.push(URLs.filter);
                      }}
                    >
                      <i className="fa fa-search" />
                    </div>
                  </div>
                  <input
                    onKeyPress={(e) => {
                      if (e.code === 'Enter') {
                        router.push(URLs.filter);
                      }
                    }}
                    type="text"
                    id="search-input"
                    className="form-control search-input"
                    name="keyword"
                    placeholder="Enter anime name..."
                  />
                </div>
              </div>
              <div className="description text-center mb-3" dangerouslySetInnerHTML={{ __html: props.underSearch }} />
              <div className="mw-buttons text-center">
                <div className="block mb-2">
                  <a href={props?.config?.twitter || '/home'} className="btn btn-highlight">
                    <i className="fab fa-twitter" />
                    Connect with us on twitter
                  </a>
                </div>
                <div className="block">
                  <a href="/home" className="btn btn-lg btn-focus more-padding">
                    Go to home
                    page
                  </a>
                </div>
              </div>
              <div className="clearfix" />
            </div>
          </div>
        </div>
      </div>
      <div id="mw-text">
        <div className="container">
          <div dangerouslySetInnerHTML={{ __html: props.contentIndex }} />

          {/* <p>9anime is a free anime website where millions visit to watch anime online.</p>
          <p>
            9anime provides users with various genres including Action, Comedy, Demons, Drama, Historical,
            Romance, Samurai, School, Shoujo Ai, Shounen Supernatural, etc. This is the perfect place to
            expand the imagination of children under 12 years old as well as spread beautiful images of
            friendship, family, teammates, magic, etc. 9anime is committed to keeping you updated with the
            latest releases and providing excellent streaming capabilities for the best experience
            possible.
          </p>
          <h2 className="mt-h-medium">Watch Anime Online Free</h2>
          <p>
            It all started in 2016 when we officially launched the system of file sharing. We boast HD
            quality as well as fast download speed. To provide our users with multiple choices, we use links
            from third party sources like mp4upload, vidstream, streamtape, mycloud. Your Favorite Anime is
            available for live streaming and can reach a huge number of audiences around the world. The
            demand for Japanese anime has steadily been on the rise recently all over the world. When 9anime
            was shut down for the first time, multiple fake copies that are prone to viruses and malware
            came into existence. For your utmost safety, visit our official 9anime website only.
          </p>
          <h2 className="mt-h-medium">What is wrong with 9anime?</h2>
          <p>
            {`  Not everyone knows that anime is not only short animation. Thanks to the widespread promotion of
            Japanese newspapers and television, more and more people from four corners of the Earth know
            about this Japanese art subject. In its home country, Japan, 33% of people watch anime (out of
            125 million, that's more than 40 million). Most great anime series are based on well-known
            comics (manga), games, or ranobe. 9anime hosts a huge collection of anime with topics suitable
            for all ages and genders. No matter what genre you are in the mood for, you can find it here on
            9anime.`}
          </p>
          <h2 className="mt-h-medium">Is KissAnime better than 9anime?</h2>
          <p>
            {`The fact that 9anime is new doesn't make it less competitive. The 9anime team has learnt from
            KissAnime's mistakes to create a site with better quality content library and much more
            outstanding updates than KissAnime. Titles you cannot find on 9anime are highly unlikely to be
            found anywhere else. 9anime is the easiest and fastest place to connect you with your titles of
            interest.`}
          </p>
          <h2 className="mt-h-medium">Is it illegal to use 9 anime?</h2>
          <p>
            Streaming anime on 9anime is not considered illegal in the US. To be exact, watching only anime
            and copyrighted shows is not technically illegal at this time. According to copyright attorneys,
            only when you download or share files, will you be subject to criminal or civil charges.
            Therefore, you are recommended to watch anime free online to avoid any possible issues.
          </p>
          <h2 className="mt-h-medium">Is 9anime safe?</h2>
          <p>
            {`We haven't received any report regarding the site's security. However, for your utmost safety,
            you should take precautionary measures such as a VPN to stay anonymous, anti-virus program and
            AdBlock extension to avoid ads and popups.`}
          </p>
          <h2 className="mt-h-medium">Is 9anime down?</h2>
          <p>
            To know if 9anime is down or having problems, you can check the uptime of the site via
            <a
              href="https://downforeveryoneorjustme.com/"
              target="_blank"
              rel="noreferrer"
            >
              https://downforeveryoneorjustme.com/
            </a>
            , or pay a visit to our social channels.
          </p>
          <h2 className="mt-h-medium">
            {`
          What's the new website of 9anime? Which 9anime is real?
          `}
          </h2>
          <p>
            9anime.vc is currently the only official website of 9anime so please be aware of fake sites.
            Names are easy to fake, but they cannot compete with the superb features that we provide. Here
            are some of the features that will help you differentiate the real 9anime from the clone ones.
            Stay with us for a real fabulous watching experience!
          </p>
          <ul>
            <li>
              <strong>Excellent quality anime:</strong>
              {`Your favorite anime deserves the best site to
              watch it on. Don't settle for low-quality sites when you can watch anime free on 9anime in
              HD quality with no buffering and lagging. We also provide subbed and dubbed versions for
              your utmost convenience. Give our videos a look and you will realize your watching
              experience on 9anime is as different as chalk and cheese from that of clone sites. `}
            </li>
            <li>
              <strong>No app installation needed:</strong>
              {' '}
              Why waste time and device memory to download an
              app when your user experience on the 9anime is as smooth as butter? 9anime is mobile
              friendly and Chromecast supported so you can watch free anime on any device, at any time, as
              long as the Internet is available. All you need to do is to visit 9anime, search for your
              title of choice, and start your anime journey immediately.
            </li>
            <li>
              <strong>Free anime streaming:</strong>
              {' '}
              Funimation, Crunchyroll, etc., can make a dent in
              your bank account. Instead of spending money on a new manga, new dresses, or weekly food,
              you have to pay to watch anime there. But with 9anime, fun is endless and free. Free
              entertainment is the best type of entertainment after all.
            </li>
            <li>
              <strong>Streaming experience:</strong>
              {`Compared to other anime streaming sites, the loading
              speed at Zoro.to is faster. Downloading is just as easy as streaming, you won't have any
              problem saving the videos to watch offline later.`}
            </li>
            <li>
              <strong>Watch unlimited anime online:</strong>
              {' '}
              There are no limits when it comes to watch
              free anime on 9anime. All the titles are available to access by free users. Our team is
              committed to providing you with the best content library possible. Should you not find your
              title of interest, simply send us a request and we will have it updated as soon as possible
              (if it is available somewhere on the Internet).
            </li>
          </ul>
          <h2 className="mt-h-medium">Best alternative to 9anime</h2>
          <p>
            Although we are confident to provide you with the best overall experience, it is
            only wise to have more options in case bad things happen. Some other reliable
            and safe free anime sites you can bookmark include gogoanime, Zoro.to, and
            anime heaven.
          </p>

          <p>&nbsp;</p>
          <div className="block text-center">
            <a href="home" className="btn btn-lg btn-focus more-padding btn-home">
              Go to
              home page
            </a>
          </div>
        </div> */}
        </div>
      </div>
    </React.Fragment>
  );
}
