/* eslint-disable max-len */
import axios from 'axios';
import http from 'https';
import fs from 'fs';
import { trackLog } from '~/services/server';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import VaultURLs, { configTemplate } from '~/utils/contains';
import redis, {
  getCurrent,
  getMonthLeft,
  redisGetAsync, redisGetValAsync, redisZincrbyAsync, timeCachingSystem,
} from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';
import { normalizeFilms } from '.';

export const normalizeFilm = ({
  _id, title, type, original_title, slug, content, rating, quality, release_date,
  release_year, views, total_episodes, description, duration, poster, backdrop, episode_new, countries, genres, directors, keywords, hagtag_films,
}:any = {}) => ({
  _id,
  title,
  type,
  original_title,
  slug,
  content,
  rating,
  quality,
  release_date,
  release_year,
  views,
  total_episodes,
  description,
  duration,
  poster,
  backdrop,
  episode_new,
  keywords,
  hagtag_films,
  countries: countries?.map((country) => ({ id: country.id, name: country.name, slug: country.slug })) || [],
  genres: genres?.map((genre) => ({ id: genre.id, name: genre.name, slug: genre.slug })) || [],
  directors: directors?.map((director) => director.person) || [],
});

const normalizeStream = (items, episode) => ({
  ...episode,
  servers: items.map(({
    id, server_id, episode_id, url, server: { name, slug },
  }) => ({
    id,
    server_id,
    name,
    slug,
    episode_id,
    url,
  })),
});

export const downSaveFile = async (fileName, prefix) => {
  try {
    let folder = fileName.split('/');
    folder.pop();
    folder = folder.join('/');

    const createFolder = prefix ? `public/captions/${prefix}/${folder}` : `public/captions/${folder}`;
    const fileNameWrite = prefix ? `public/captions/${prefix}/${fileName}` : `public/captions/${fileName}`;

    const resp = await fs.promises.mkdir(createFolder, { recursive: true }).catch(() => 'error');

    if (resp === 'error') return false;

    return new Promise((res) => {
      const file = fs.createWriteStream(fileNameWrite, { encoding: 'utf8' });
      http.get(`${VaultURLs.API_URL}/${fileName}`, (response) => {
        response.pipe(file);
        file.on('error', () => {
          res(1);
          file.close();
        });
        file.on('finish', () => {
          res(1);
          file.close();
        });
      });
    });
  } catch (error) {
    return false;
  }
};

const upViewCache = async (id) => {
  const currentDateString = getCurrent().format('DD/MM/YYYY');
  const monthLeft = getMonthLeft();
  const client = redis();
  const key = KEYS_REDIS.TV_RANK_EXPIRE;
  const value = await redisGetAsync(key);
  if (!value) {
    client.set(key, 'oke', 'EX', monthLeft);
    client.expire(KEYS_REDIS.TV_RANK, monthLeft);
  }

  await redisZincrbyAsync(KEYS_REDIS.TV_RANK, 1, `${id}-${currentDateString}`);
};

const upView = async (id) => {
  try {
    await upViewCache(id);
    const baseUpView = `${VaultURLs.API_URL}/api/tv-series/${id}/view/update`;
    await axios.post(baseUpView);
    return true;
  } catch {
    return false;
  }
};

export const getInfoTVFilm = async (id, isUpView = false) => {
  const client = redis();
  const key = `${KEYS_REDIS.TV}:${id}`;

  if (isUpView) {
    await upView(id);
  }

  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const resp = await axios.get(`${VaultURLs.API_URL}/api/tv-series/${id}`);
  const data = JSON.parse(JSON.stringify(normalizeFilm(resp.data.data)));
  const result = {
    data: {
      data,
    },
  };
  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

export const getInfoTVSimilar = async (key, id) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);
  const baseURLSimilar = `${VaultURLs.API_URL}/api/tv-series/${id}/similar?length=15`;

  const resp = await axios.get(baseURLSimilar);

  const result = {
    data: {
      data: normalizeFilms(resp.data.data),
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

export const getInfoPerson = async (key, id) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);
  const baseURLSimilar = `${VaultURLs.API_URL}/api/tv-series/${id}/credits?limit=20`;

  const resp = await axios.get(baseURLSimilar);

  const result = {
    data: {
      data: resp.data.data,
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

export const getInfoTVSeason = async (key, id) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);
  const { az } = await redisGetValAsync(KEYS_REDIS.CONFIG) || configTemplate;

  const baseURLSeasons = `${VaultURLs.API_URL}/api/tv-series/${id}/seasons`;

  const seasonsInfoResp = await axios.get(baseURLSeasons);

  const data = seasonsInfoResp.data.data.sort((a, b) => (az ? b.number - a.number : -b.number + a.number));

  const result = {
    data: {
      data,
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
  return result;
};

export const getInfoTVEpisodes = async (key, id) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);
  const { az } = await redisGetValAsync(KEYS_REDIS.CONFIG) || configTemplate;

  const baseURLSeasons = `${VaultURLs.API_URL}/api/seasons/${id}/episodes?length=1000`;

  const seasonsInfoResp = await axios.get(baseURLSeasons);

  const data = seasonsInfoResp.data.data.sort((a, b) => (az ? b.number - a.number : -b.number + a.number));

  const result = {
    data: {
      data,
    },
  };
  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

const setCaption = async (data) => {
  const files = [...new Set(data.map((item) => item.filename))];

  const client = redis();

  const value = await Promise.all(files.map(async (file) => {
    const is = await redisGetAsync(file);
    return is || file;
  }));

  const filesNotCache = value.filter((item) => item !== 'oke');

  if (!filesNotCache.length) return true;

  filesNotCache.map((file) => client.set(file, 'oke'));

  await Promise.all(filesNotCache.map((file) => {
    const prefix = data.find((item) => item.filename === file).name || '';
    return downSaveFile(file, prefix.toUpperCase());
  }));
  return true;
};

export const getInfoTVStreams = async (key, id, ip) => {
  const client = redis();
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const baseURLSeasons = `${VaultURLs.API_URL}/api/episodes/${id}/stream_urls`;
  const baseURL = (url) => `${VaultURLs.IO_CODE_URL}/api/index.php?url=${url}&ip_client=${ip}`;

  const seasonsInfoResp = await axios.get(baseURLSeasons);

  const data = await Promise.all(seasonsInfoResp.data.data.streamurls.map(async ({ url }) => {
    const rep = await axios.get(baseURL(url));
    return rep.data;
  }));

  const captions = seasonsInfoResp.data.data.captions || [];

  await setCaption(captions);

  const result = {
    data: {
      other: { captions },
      data: seasonsInfoResp.data.data.streamurls.map((stream, index) => ({ ...stream, url: (data[index] as any).data[0].file })),
    },
  };
  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));

  return result;
};

const handler = async (req, res) => {
  try {
    const {
      id, toolTip: tooltip, type, ip,

    } = req.query;

    const key = `${KEYS_REDIS.TV}:${type}-${id}`;
    const configRequest = req.getStructureDynamicRouterServer('');
    if (type === 'season') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoTVSeason(key, id);
      return res.RESP(info);
    }

    if (type === 'episode') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoTVEpisodes(key, id);
      return res.RESP(info);
    }

    if (type === 'stream') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoTVStreams(key, id, ip);
      return res.RESP(info);
    }

    if (type === 'similar') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoTVSimilar(key, id);
      return res.RESP(info);
    }
    if (type === 'person') {
      if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
      const info = await getInfoPerson(key, id);
      return res.RESP(info);
    }

    const info = await getInfoTVFilm(id, !tooltip);
    return res.RESP(info);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
