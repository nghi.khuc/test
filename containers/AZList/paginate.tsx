/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import qs from 'qs';
import omit from 'lodash/omit';

const Paginate = ({ response = {} }: any) => {
  const router = useRouter();
  const [url] = router.asPath.split('?');
  const [pageState, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(1);

  const query: any = omit(router.query, 'slug');

  useEffect(() => {
    if (response?.lastPage) {
      setLastPage(response?.lastPage);
    }
  }, [response?.lastPage]);
  useEffect(() => query?.page && setPage(query?.page), [query?.page]);

  const onChangePage = (event) => {
    setPage(event.target.value);
    // window.location.href = `?page=${response?.page}`;
  };
  const getParam = (page) => qs.stringify({
    ...query,
    page,
  });

  const onSubmitSearchPage = () => {
    // router.push(`${url}?page=${pageState}`);
    router.push(`${url}?${getParam(pageState)}`);
  };

  const handleNextPage = () => {
    if (+pageState === +lastPage) return;
    if (+pageState + 1 > lastPage) {
      router.push(`${url}?${getParam(pageState)}`);
      setPage(pageState);
      return;
    }
    // router.push(`${url}?page=${+pageState + 1}`);
    setPage(+pageState + 1);
    router.push(`${url}?${getParam(+pageState + 1)}`);
  };

  const handlePrevPage = () => {
    if (+pageState === 1) return;
    if (+pageState - 1 <= 1) {
      router.push(`${url}?${getParam(undefined)}`);
      setPage(1);
      return;
    }
    // router.push(`${url}?page=${+pageState - 1}`);
    setPage(+pageState - 1);
    router.push(`${url}?${getParam(+pageState - 1)}`);
  };

  return (
    <div className="anime-pagination mt-2 mb-2">
      <div className="ap_-nav">
        <div className="ap__-btn ap__-btn-prev" onClick={handlePrevPage}>
          <a
            className={`btn btn-sm btn-focus more-padding ${+pageState === 1 ? 'disabled' : ''}`}
          >
            <i className="fas fa-angle-left" />
            {' '}
            Previous
          </a>
        </div>
        <div className="ap__-input">
          <div className="btn btn-sm btn-blank">page</div>
          <input
            className="form-control form-control-dark input-page"
            defaultValue={pageState || 1}
            onChange={onChangePage}
            value={pageState}
            data-url={`${url}?page=`}
            data-toggle="tooltip"
            data-placement="top"
            title="Enter a number"
          />
          <button onClick={onSubmitSearchPage} type="button" className="btn btn-sm btn-focus btn-go-page">go</button>
          <div className="btn btn-sm btn-blank">
            {`of ${lastPage}`}
          </div>
        </div>
        <div className="ap__-btn ap__-btn-next" onClick={handleNextPage}>
          <a className={`btn btn-sm btn-focus more-padding ${+pageState === +lastPage ? 'disabled' : ''}`}>
            Next
            {' '}
            <i className="fas fa-angle-right" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Paginate;
