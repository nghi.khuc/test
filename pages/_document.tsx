import Document, {
  Html, Head, Main, NextScript,
} from 'next/document';
import Script from 'next/script';
import { KEYS_REDIS } from '~/utils/config';
import { configTemplate } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';

export const IGNORE = /(\/admin.*)/i;
class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    const configPage = await redisGetValAsync(KEYS_REDIS.CONFIG) || configTemplate;
    return { ...initialProps, configPage, isAdmin: IGNORE.test(ctx.req.url) };
  }

  render() {
    const { configPage, isAdmin } = this.props as any;
    const {
      analytic,
    } = configPage;

    return (
      <Html lang="en">
        <Head>
          <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
          {
            isAdmin ? (
              <meta name="robots" content="noindex" />
            ) : <meta name="robots" content="index,follow" />
          }
          <meta httpEquiv="content-language" content="en" />
          {/* <meta name="description" content={description || ''} />
          <meta property="og:description" content={description || ''} />
          <meta property="og:title" content="9anime Homepage: Best free anime to watch in HD quality" /> */}
          {/* <meta name="keywords" content="9anime, 9 anime, 9animeto,
          latest anime, watch anime online, free anime, anime free, online anime, anime website" /> */}
          {/* <meta property="og:url" content="https://9anime.vc/home" /> */}
          <meta property="og:type" content="website" />
          <meta property="og:image" content="" />
          <meta property="og:image:width" content="650" />
          <meta property="og:image:height" content="350" />
          <meta name="apple-mobile-web-app-status-bar" content="#202125" />
          <meta name="theme-color" content="#202125" />
          <link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />

          {
            !!analytic && (
              <Script
                id="analytic"
                dangerouslySetInnerHTML={{
                  __html: analytic,
                }}
              />
            )
          }

          <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"
            integrity="sha512-jGsMH83oKe9asCpkOVkBnUrDDTp8wl+adkB2D+//JtlxO4SrLoJdhbOysIFQJloQFD+C4Fl1rMsQZF76JjV0eQ=="
            crossOrigin="anonymous"
          />

          <script
            type="text/javascript"
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"
          />
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />

          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" />

          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
