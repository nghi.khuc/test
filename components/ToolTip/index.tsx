/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useMemo, useState, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';

import { setItemCodeActive, setPosition } from 'stores/slices/FilmSlice';
import {
  postFavoriteFilm, deleteFavoriteFilm, getUserProfileFavoriteMovie, getUserProfileFavoriteTV,
} from '~/services/client';
import { buildUrl, toastMessage } from '~/utils/common';

const mappingType = {
  'tv-series': 'TV Series',
  movie: 'Movies',
  movies: 'Movies',
};

export default function Qtip() {
  const dispatch = useDispatch();
  const {
    qtipItem, position, itemCodeActive, loadingItem, user,
  } = useSelector((state: any) => ({
    qtipItem: state.filmSlices.qtipItem,
    position: state.filmSlices.position,
    itemCodeActive: state.filmSlices.itemCodeActive,
    loadingItem: state.filmSlices.loadingItem,
    user: state.userSlices?.user,
  }), shallowEqual);
  const [checked, setChecked] = useState(false);
  const onMoveLeave = () => {
    document.body.classList.remove('modal-open');
    dispatch(setPosition({}));
  };
  const itemFilm = { ...qtipItem[itemCodeActive?.code], ...itemCodeActive.item };
  const seasonNew = itemFilm?.season_new;
  const onClickAddFavorite = async (item) => {
    const paramsSubmit = { id: item._id, type: item.type };
    if (checked) {
      deleteFavoriteFilm(paramsSubmit);
      setChecked(false);
      toastMessage({ content: 'This anime has been removed to "Plan to watch" folder.' });
    } else {
      postFavoriteFilm(paramsSubmit);
      setChecked(true);
      toastMessage({ content: 'This anime has been added to "Plan to watch" folder.' });
    }
  };
  const url = buildUrl(itemFilm);
  return (
    <div
      onMouseLeave={onMoveLeave}
      id={`qtip-${itemCodeActive.code}`}
      className="qtip qtip-default  qtip-pos-tl qtip-fixed qtip-pos-tr qtip-pos-br"
      role="alert"
      aria-live="polite"
      aria-atomic="false"
      aria-describedby={`qtip-${itemCodeActive.code}-content`}
      aria-hidden="false"
      data-qtip-id={itemCodeActive.code}
      style={{
        width: 330,
        top: position?.top,
        left: position?.left,
        display: !isEmpty(position) ? 'block' : 'none',
        zIndex: 9999,
      }}
    >
      {loadingItem ? (
        <div className="qtip-tip" style={{ display: 'block' }}>
          <div className="loading-relative" id="search-loading" style={{ display: 'block' }}>
            <div className="loading">
              <div className="span1" />
              <div className="span2" />
              <div className="span3" />
            </div>
          </div>
        </div>
      ) : (
        <div className="qtip-content" id={`qtip-${itemCodeActive.code}-content`} aria-atomic="true">
          <div className="pre-qtip-content">
            <div id="wl-qtip-8143">
              {!user ? (
                <a data-toggle="modal" data-target="#modallogin">
                  <div className="qtip-fav wl-item" data-id="8143" data-type="3" data-page="qtip"><i className="fas fa-plus" /></div>
                </a>
              ) : (
                <div
                  onClick={() => onClickAddFavorite(itemFilm)}
                  className="qtip-fav wl-item"
                  data-id="8143"
                  data-type="3"
                  data-page="qtip"
                >
                  <i className={`fas ${(itemFilm.has_favorite || checked) ? 'fa-check' : 'fa-plus'}`} />
                </div>
              )}

            </div>
            <div className="pre-qtip-title">{itemFilm?.title}</div>
            <div className="pre-qtip-detail">

              <span className="pqd-li">
                {itemFilm?.total_episodes ? (
                  `Episode ${itemFilm?.episode_new || itemFilm.total_episodes}/${itemFilm?.total_episodes || 'Full'}`
                ) : 'Episode Full'}
              </span>
              <div className="tip-right">
                <span className="pqd-li badge badge-type">{mappingType[itemFilm?.type]}</span>
              </div>
              {!!itemFilm.season_new && (
                <div className="tip-right">
                  <span className="pqd-li badge badge-dub">{`SS ${itemFilm?.season_new}`}</span>
                </div>
              )}
              <div className="clearfix" />
            </div>
            <div className="pre-qtip-description">
              {itemFilm?.content}
            </div>
            <div className="pre-qtip-line">
              <span className="stick">Other names: </span>
              {itemFilm?.title}
            </div>
            <div className="pre-qtip-line">
              <span className="stick">Scores: </span>
              {itemFilm?.rating}
            </div>
            <div className="pre-qtip-line">
              <span className="stick">Date aired: </span>
              {moment(itemFilm?.release_date, 'DD/MM/YYYY').format('ll')}
            </div>
            {/* <div className="pre-qtip-line">
                <span className="stick">Status: </span>
                Currently Airing
              </div> */}
            <div className="pre-qtip-line">
              <span className="stick">Genre: </span>
              {itemFilm?.genres?.map((genres: any) => (
                <React.Fragment key={genres.slug}>
                  <a href={`/filter?genres=${genres.id}`}>{genres.name}</a>
                  ,
                </React.Fragment>
              ))}

            </div>
            <div className="pre-qtip-button">
              <a href={url} className="btn btn-block btn-play">
                <i className="fa fa-play-circle mr-1" />
                Watch now!
              </a>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
