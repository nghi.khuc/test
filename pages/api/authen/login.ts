import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import VaultURLs from '~/utils/contains';
import {
  exception, HTTP_STATUS, setCookieToken, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/auth/login`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const { data } = configRequest;
    const { remember, ...other } = data;

    const resp = await axios({ ...configRequest, data: other });
    const tokenInfo = resp.data;
    const { result } = tokenInfo;
    setCookieToken({ req, res }, result, remember);
    return res.RESP('oke');
  } catch (e) {
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
