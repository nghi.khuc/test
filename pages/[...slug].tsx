/* eslint-disable react/button-has-type */
import React from 'react';
import Container from 'components/Filter';
import { GetServerSideProps } from 'next';
import { redisGetValAsync } from '~/utils/redis';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';

export function Home(props) {
  return (
    <Container {...props} />
  );
}

export default Home;
