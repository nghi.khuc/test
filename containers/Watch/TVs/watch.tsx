/* eslint-disable react/jsx-key */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import ReactJWPlayer from 'react-jw-player';
import isEmpty from 'lodash/isEmpty';
import LazyLoad from 'react-lazyload';

import Popup from 'reactjs-popup';
import { useSelector } from 'react-redux';
import ReactStars from 'react-stars';
import BlockControl from '../BlockControl';

import { getTVSeriesById, postErrorFilm, postRating } from '~/services/client';
import { ItemFilm } from '~/stores/slices/FilmSlice/types';

const ContainerWPCover = styled.div`
  background-image: url(${(props) => props.backdrop});
`;

interface IParams {
  itemFilm: ItemFilm,
  episode: number | string,
  sv: number | string,
  type: 'movie' | 'tv-series' | 'movies',
  seasons: any,
  listEpisodes: any,
  listServers: any
  captions: any
  underPlayer: string,
  seasonNumber: number
  openModal: () => void
}

const Movies = ({
  episode, sv, itemFilm, seasons: listSeasons, listEpisodes, listServers, underPlayer, seasonNumber: ss, captions, openModal,
}: IParams) => {
  const router = useRouter();
  const pathName = `/${itemFilm?.type}/${itemFilm?.slug}.${itemFilm._id}`;
  const [stateLight, setLight] = useState('off');
  const [isAutoPlay, setAutoPlay] = useState(true);
  const [isAutoNext, setAutoNext] = useState(false);
  const [listEpisodesState, setListEpisodes] = useState(listEpisodes);
  const [episodeState, setEpisodeState] = useState(episode);
  const [loading, setLoading] = useState(false);
  const [listServerStates, setListServer] = useState(listServers);

  useEffect(() => {
    const isAutoNextLocal = localStorage.getItem('isAutoNext');
    const isAutoPlayLocal = localStorage.getItem('isAutoPlay');
    if (isAutoNextLocal) {
      setAutoNext(JSON.parse(isAutoNextLocal));
    }
    if (isAutoPlayLocal) {
      setAutoPlay(JSON.parse(isAutoPlayLocal));
    }
  }, []);

  useEffect(() => {
    setListEpisodes(listEpisodes);
  }, [listEpisodes]);

  useEffect(() => {
    setListServer(listServers);
  }, [listServers]);

  useEffect(() => {
    setEpisodeState(+episode);
  }, [episode]);

  const onToggleLight = () => {
    setLight('');
    const element: any = document?.getElementById('mask-overlay');
    element.className += 'active';
  };

  const handleAutoNext = () => {
    setAutoNext(!isAutoNext);
    localStorage.setItem('isAutoNext', JSON.stringify(!isAutoNext));
  };

  const handleAutoPlay = () => {
    setAutoPlay(!isAutoPlay);
    localStorage.setItem('isAutoPlay', JSON.stringify(!isAutoPlay));
  };
  const playlist = useMemo(() => [{
    file: listServerStates[+sv - 1]?.url,
    image: itemFilm?.backdrop,
    tracks: captions?.map((caption, index) => ({
      file: `${window.location.origin}/captions/${caption.name.toUpperCase()}/${caption.filename}`,
      label: caption.name.toUpperCase(),
      kind: 'captions',
      default: !index,
    })) || [],
  }], [listServerStates, sv, episodeState, ss, captions]);

  // const playlist = [{
  //   file: listServerStates[+sv - 1]?.url,
  //   image: itemFilm?.backdrop,
  //   tracks: captions?.map((caption, index) => ({
  //     file: `${window.location.origin}/${caption.name.toUpperCase()}/${caption.filename}`,
  //     label: caption.name.toUpperCase(),
  //     kind: 'captions',
  //     default: !index,
  //   })) || [],
  // }];

  const season = useMemo(() => (!isEmpty(listSeasons) ? listSeasons.find((item) => item.number === +ss) : {}), [ss, listSeasons]);

  const idEpisodeActive = useMemo(() => listEpisodesState.find((item) => item.number === episode), [episode]);

  const onChangeEpisode = async (epi) => {
    if (epi > listEpisodesState.length || epi < 1) return;
    const id = itemFilm?._id;
    let url = `${pathName}/server-${sv}/ep-${epi}.html`;
    if (ss && ss > 1) {
      url += `?ss=${ss}`;
    }
    router.push(url);
    const episodeNext = listEpisodesState.find((item) => item.number === epi);
    setLoading(true);
    setEpisodeState(epi);
    const idEpisode = episodeNext?._id;
    // const nameEpisode = episodeNext?.name || '';
    // let url = `/${itemFilm?.type}/${itemFilm?.slug}-${id}.html?ep=${(epi)}&sv=${sv}`;

    const responseStreamsServer = await getTVSeriesById(idEpisode, { type: 'stream' });
    const servers = responseStreamsServer.data || [];
    setListServer(servers);
    setLoading(false);
  };

  const onChangeSeason = async (s) => {
    setLoading(true);
    // const id = itemFilm?._id;
    const url = `${pathName}/server-${1}/ep-${1}.html?ss=${s}`;
    router.push(url);
    const seasonSelected = !isEmpty(listSeasons) ? listSeasons.find((item) => item.number === +s) : {};
    const responseEpisodes = await getTVSeriesById(seasonSelected._id, { type: 'episode' });
    const episodesOfSeasons = responseEpisodes.data || [];

    setListEpisodes(episodesOfSeasons);
    const firstEpisode = episodesOfSeasons[0];
    const idEpisode = firstEpisode?._id;
    // const nameEpisode = firstEpisode?.name || '';
    // const url = `/${itemFilm?.type}/${itemFilm?.slug}-${id}.html?ep=${episode}&sv=${sv}&ss=${s}`;
    setEpisodeState(firstEpisode?.number);
    const responseStreamsServer = await getTVSeriesById(idEpisode, { type: 'stream' });
    const servers = responseStreamsServer.data || [];
    setListServer(servers);
    setLoading(false);
  };

  const changeServer = (id) => {
    setLoading(true);
    const url = `${pathName}/server-${id}/ep-${episode}.html?ss=${ss}`;
    router.push(url);
    // let url = `${pathName}?ep=${(episodeState)}&sv=${id}`;
    // if (ss && +ss > 1) {
    //   url += `&ss=${ss}`;
    // }
    setTimeout(() => {
      setLoading(false);
    }, 500);
  };

  const nextFilm = () => {
    const isAutoNextLocal = localStorage.getItem('isAutoNext');
    const parseLocaleAutoNext = isAutoNextLocal && JSON.parse(isAutoNextLocal);
    const listEpidoesLength = listEpisodesState?.length;
    if (parseLocaleAutoNext && (episodeState < listEpidoesLength)) {
      onChangeEpisode(+episodeState + 1);
    }
    openModal();
  };

  return (
    <React.Fragment>
      <div id="watch-block" className="">
        <div className="player-wrap">
          <div className="wb_-playerarea">
            <ContainerWPCover
              backdrop={itemFilm?.backdrop}
              className="wb__-cover"
            />
            {(loading || isEmpty(listServerStates)) ? (
              <div className="loading-relative loading-box" id="embed-loading">
                <div className="loading">
                  <div className="span1" />
                  <div className="span2" />
                  <div className="span3" />
                </div>
              </div>
            ) : (
              <div style={{ position: 'absolute', width: '100%', height: '100%' }}>
                <ReactJWPlayer
                  onError={() => postErrorFilm({
                    movieType: 'tv-series',
                    movieID: itemFilm?._id,
                    server: sv,
                    episodeId: idEpisodeActive?._id,
                  })}
                  playerId={itemFilm?.slug}
                  isAutoPlay={isAutoPlay}
                  onOneHundredPercent={nextFilm}
                  playerScript="https://content.jwplatform.com/libraries/j9BLvpMc.js"
                  playlist={playlist}
                />
              </div>
            )}
          </div>
        </div>
        <BlockControl
          setAutoNext={handleAutoNext}
          isAutoNext={isAutoNext}
          stateLight={stateLight}
          isAutoPlay={isAutoPlay}
          setAutoPlay={handleAutoPlay}
          episodeState={episodeState}
          onToggleLight={onToggleLight}
          onChangeEpisode={onChangeEpisode}
          ss={ss}
        />
      </div>
      {/** Block tool light, expand ,... */}

      {/** Block swtch server */}

      <div className="player-servers">
        <div id="servers-content">
          <div className="ps_-status">
            <div className="content">
              <div className="server-notice">
                <strong>
                  You are watching
                  <b>{` Episode ${episodeState}`}</b>
                  .
                </strong>
                If current server doesn't work please try other servers below
              </div>
            </div>
          </div>
          <div className="ps_-block ps_-block-sub servers-sub">
            <div className="ps__-title">
              Server:
            </div>
            <div className="ps__-list">
              {!isEmpty(listServerStates) && listServerStates?.map((server: any, idx: number) => (
                <div onClick={() => { changeServer(+idx + 1); }} key={idx} className="item server-item" data-type="sub" data-id="392375" data-server-id="4">
                  <a className={`btn ${+sv === (idx + 1) ? 'active' : ''}`}>{server.server.name}</a>
                </div>
              ))}
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
      {
        !!underPlayer && <div className="mba-block" dangerouslySetInnerHTML={{ __html: underPlayer }} />
      }

      {!isEmpty(listSeasons) ? (
        <React.Fragment>
          {listSeasons?.length > 1 && (
            <div
              key={`episodes-page-${ss}`}
              id={`episodes-page-${ss}`}
              className="episodes-ul"
              data-page={ss}
            >
              <LazyLoad key={`${ss}`} placeholder=".....">
                <section className="block_area block_area-episodes">
                  <div style={{ paddingTop: 10, paddingLeft: 15 }}>Seasons</div>
                  <div className="block_area-content">
                    {listSeasons?.map((seasonItem: any, idx: number) => (
                      <React.Fragment key={idx}>
                        <div
                          onClick={() => onChangeSeason(seasonItem.number)}
                        >
                          <a
                            title={`${seasonItem.number}_ss_${seasonItem.number}`}
                            className={`item ep-item ${seasonItem.number === +ss ? 'active' : ''}`}
                            data-number={seasonItem.number}
                            data-id={seasonItem.number}
                          >
                            <div className="order">{seasonItem.number}</div>
                          </a>
                        </div>
                      </React.Fragment>
                    ))}
                    <div className="clearfix" />
                  </div>
                </section>
              </LazyLoad>
            </div>
          )}

          <LazyLoad key={`${ss}_${season.name}`} placeholder=".....">
            <section className="block_area block_area-episodes">
              <div style={{ paddingTop: 10, paddingLeft: 15 }}>Episodes</div>
              <React.Fragment>
                <div className="block_area-content">
                  <div
                    key={`episodes-page-${season.number - 1}`}
                    id={`episodes-page-${season.number - 1}`}
                    className="episodes-ul"
                    data-page={season.number - 1}
                  >
                    {!isEmpty(listEpisodesState) && listEpisodesState?.map((epi: any, i: number) => {
                      const active = (epi?.number === episodeState) && (season.number === +ss);
                      return (
                        <div
                          key={`${i}_${epi.number}`}
                          onClick={() => onChangeEpisode(epi.number)}
                        >
                          <a
                            title={`${epi.number}_ss_${season.number}`}
                            className={`item ep-item ${active ? 'active' : ''}`}
                            data-number={epi.number}
                            data-id={epi.number}
                          >
                            <div className="order">{epi.number}</div>
                          </a>
                        </div>
                      );
                    })}
                    <div className="clearfix" />
                  </div>
                </div>
              </React.Fragment>

            </section>
          </LazyLoad>
        </React.Fragment>
      ) : (
        <section className="block_area block_area-episodes">
          <div className="block_area-content">
            <div
              key="episodes-page-1"
              id="episodes-page-1"
              className="episodes-ul"
              data-page="1"
            >
              <a
                href={`/${itemFilm?.type}/${itemFilm?.slug}-${itemFilm?._id}?ep=${episodeState}&sv=${sv}}`}
                title={itemFilm?.title}
                className="item ep-item active"
                data-number={1}
                data-id={1}
              >
                <div className="order">{1}</div>
              </a>
              <div className="clearfix" />
            </div>
          </div>

        </section>
      )}
    </React.Fragment>
  );
};

export default Movies;
