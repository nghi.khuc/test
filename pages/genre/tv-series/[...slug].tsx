/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from 'react';
import uniq from 'lodash/uniq';
import ListFilms from 'components/ListFilms';
import QuickFilter from 'components/QuickFilter';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { GetServerSideProps } from 'next';
import omit from 'lodash/omit';
import { getListFilmsTVSeries } from '~/services/client';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';
import Paginate from '~/containers/AZList/paginate';

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};
const FilterContainer = ({ type: TypeFilm, query, ...props }: any) => {
  const router = useRouter();
  const [url] = router.asPath.split('?');
  const pathName = decodeURI(url.split('/')[3]);
  const [listFilms, setListFilm] = useState(() => Array(10).fill(initItem));
  const [paginate, setPaginate] = useState<any>({});
  const listMenus = useSelector((state: any) => state.menuSlices.listMenus);
  const { genres } = listMenus;
  const qgenres: any = genres?.filter((item) => item.slug.trim() === pathName).map((item) => item.id)?.join('');
  const newQuery: any = { genre: qgenres, ...query };
  const fetchListFilm = async () => {
    if (newQuery.genre) {
      const getListFilm = await getListFilmsTVSeries(newQuery);
      setListFilm(getListFilm.data);
      setPaginate(getListFilm);
    }
  };

  useEffect(() => {
    fetchListFilm();
  }, [router, genres]);

  return (
    <React.Fragment>
      {newQuery.genre ? (
        <QuickFilter {...props} query={{ ...query, genres: newQuery.genre }} isBlockSideBar={false} />
      ) : null}
      <ListFilms listFilms={listFilms} />
      <div className="clearfix" />
      {!!paginate?.page && (
        <Paginate response={paginate} />
      )}
    </React.Fragment>
  );
};
export default FilterContainer;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const titleKey = CONFIG_TITLE_DES_PAGE.genres.key;
  const { keyReplace } = CONFIG_TITLE_DES_PAGE.genres;

  const gereSlug = query.slug?.toString() || '';

  const doc = await redisGetValAsync(titleKey);
  const regex = new RegExp(`\\${keyReplace}`, 'g');
  const title = (doc?.title || '').replace(regex, gereSlug);
  const description = (doc?.description || '').replace(regex, gereSlug);
  const paramQuery = omit(query, 'slug');

  return {
    props: {
      query: { ...paramQuery, option: ['tv-series'] },
      title,
      description,
    },
  };
};
