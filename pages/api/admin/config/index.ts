import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import fs from 'fs';
import { promisify } from 'util';
import { FILES_NAME, KEYS_CONTENT, KEYS_REDIS } from '~/utils/config';
import redis, { redisGetValAsync } from '~/utils/redis';
import {
  HTTP_STATUS, verifyCookieAdmin, withMiddleware,
} from '~/utils/run';
import { trackLog } from '~/services/server';
import { configTemplate } from '~/utils/contains';

const key = KEYS_REDIS.CONFIG;

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    const isAccept = verifyCookieAdmin(req);

    if (!configRequest || !isAccept) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const value = await redisGetValAsync(key);

    if (req.method === 'GET') {
      if (value) return res.status(200).json(value);
      return res.status(200).json(configTemplate);
    }
    if (req.method === 'POST') {
      const client = redis();
      const data = req.body;
      if (value
      // && +value?.timeCacheContent === +data.timeCacheContent
      ) {
        KEYS_CONTENT.map((keyContent) => client.expire(keyContent, +data.timeCacheContent));
      }
      const dataString = JSON.stringify(data);
      client.set(key, dataString);
      await promisify(fs.writeFile)(FILES_NAME.CONFIG, dataString);
      return res.status(200).json(data);
    }
    return res.status(401).send('wrong');
  } catch (e) {
    trackLog(req, e);
    return res.status(200).json(configTemplate);
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
