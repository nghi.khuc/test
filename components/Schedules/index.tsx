/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable react/button-has-type */
import React, {
  useEffect, useState, useRef, useCallback, useMemo,
} from 'react';
import moment from 'moment';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import { getSchedules } from '~/services/client';
import { buildUrl, getDaysArray } from '~/utils/common';
import { useGetIsMobile, useScreenSize } from '~/utils/useScreenSize';

const getZone = () => {
  const zone = moment(moment(new Date()).format('DD/MM/YYYY HH:mm'), 'DD/MM/YYYY HH:mm').diff(moment(moment(moment.utc()).format('DD/MM/YYYY HH:mm'), 'DD/MM/YYYY HH:mm'), 'hours');
  if (zone > 9) {
    return `+${zone}`;
  }
  if (zone > 0) return `+0${zone}`;

  if (zone > -9) return `-0${Math.abs(zone)}`;

  return `${zone}`;
};

export default function schedule({ isMobileSsr }: any) {
  const zone = getZone();
  const { isMobile: isMobileCsr } = useScreenSize();
  const isMobile = useGetIsMobile(isMobileSsr, isMobileCsr);

  const now = moment(new Date()).format('DD/MM/YYYY');
  // .add(-7).format('DD/MM/YYYY');
  const [slideIndex, setSlideIndex] = useState(0);
  const [showAppends, setShowAppends] = useState(false);
  const [updateCount, setUpdateCount] = useState(0);
  const [dayActive, setDayActive] = useState(now);
  const [listSchedules, setListSchedules] = useState<any>([]);
  const sliderRef: any = useRef(null);
  const scrollChangeRef: any = useRef(false);
  const fetchApiSchedule = async () => {
    const responseListSchedules = await getSchedules();
    setListSchedules(responseListSchedules.data);
  };

  useEffect(() => {
    const interVal = setInterval(() => {
      showTime();
    }, 1000);
    const dates = moment(new Date()).format('DD');
    sliderRef.current?.slickGoTo(+dates);
    fetchApiSchedule();

    return () => {
      if (interVal) {
        clearInterval(interVal);
      }
    };
  }, []);

  const showTime = () => {
    const time: any = new Date();
    let hour: any = time.getHours();
    let min: any = time.getMinutes();
    let sec: any = time.getSeconds();
    let am_pm: any = 'AM';

    if (hour > 12) {
      hour -= 12;
      am_pm = 'PM';
    }
    if (+hour === 0) {
      hour = 12;
      am_pm = 'AM';
    }

    hour = hour < 10 ? `0${hour}` : hour;
    min = min < 10 ? `0${min}` : min;
    sec = sec < 10 ? `0${sec}` : sec;

    const currentTime = `${hour}:${min}:${sec} ${am_pm}`;
    const elementClock: any = document.getElementById('clock');
    if (elementClock) {
      elementClock.innerHTML = currentTime;
    }
  };

  const listDaysOfWeak = useMemo(() => getDaysArray(), []);

  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: isMobile ? 3 : 7,
    swipeToSlide: true,
    // slidesToScroll: 7,
    afterChange: (index) => {
      setUpdateCount(updateCount + 1);
    },
    beforeChange: (current, next) => {
      setSlideIndex(next);
    },
  };
  const filterListSchedules = useMemo(() => listSchedules.filter((item) => {
    const day = moment(item.time_schedule).format('DD/MM/YYYY');
    return day === dayActive;
  }), [dayActive]);

  const sliceListSchedules = !showAppends ? (filterListSchedules || []).slice(0, 5) : filterListSchedules;
  return (
    <div id="schedule-block">
      <section className="block_area block_area_sidebar block_area-schedule schedule-full">
        <div className="block_area-header">
          <div className="float-left bah-heading mr-2">
            <h2 className="cat-heading">Estimated Schedule</h2>
          </div>
          <div className="float-left bah-time">
            <span className="current-time">
              <span id="timezone">{`GMT${zone}:00`}</span>
              {' '}
              <span id="current-date">{now}</span>
              {' '}
              <span id="clock" />
            </span>
          </div>
          <div className="clearfix" />
        </div>
        <div className="block_area-content">
          <div className="table_schedule">
            <div className="table_schedule-date">
              <div className="swiper-container swiper-container-initialized swiper-container-horizontal">
                <Slider ref={sliderRef} {...settings}>
                  {listDaysOfWeak?.map((item, idx: number) => (
                    <div key={idx} className="swiper-slide day-item" data-date={item.dayisoString}>
                      <div
                        onMouseDown={() => { scrollChangeRef.current = false; }}
                        onMouseMove={(event) => {
                          event.preventDefault();
                          scrollChangeRef.current = true;
                        }}
                        onMouseUp={() => {
                          if (!scrollChangeRef.current) {
                            setDayActive(item.dayisoString);
                          }
                        }}
                        className={`tsd-item ${item.dayisoString === dayActive ? 'active' : ''}`}
                        style={{
                          width: isMobile ? 63.3333 : 112.571,
                          marginRight: isMobile ? 10 : 13,
                          height: isMobile ? 83 : '100%',
                          paddingLeft: 10,
                          paddingRight: 11,
                        }}
                      >
                        <span>{item.week}</span>
                        <div className="date">{item.day}</div>
                      </div>
                    </div>
                  ))}
                </Slider>
                <span className="swiper-notification" aria-live="assertive" aria-atomic="true" />
                <span className="swiper-notification" aria-live="assertive" aria-atomic="true" />
              </div>
              <div className="ts-navigation">
                <button
                  onClick={() => { sliderRef.current?.slickGoTo(slideIndex + 1); }}
                  className="btn tsn-next"
                  tabIndex={0}
                  role="button"
                  aria-label="Next slide"
                  aria-disabled="false"
                >
                  <i className="fas fa-angle-right" />
                </button>
                <button
                  className="btn tsn-prev"
                  onClick={() => { sliderRef.current?.slickGoTo(slideIndex >= 0 && slideIndex - 1); }}
                  tabIndex={0}
                  role="button"
                  aria-label="Previous slide"
                  aria-disabled="false"
                >
                  <i className="fas fa-angle-left" />
                </button>
              </div>
            </div>
            <div className="clearfix" />
            <ul className="ulclear table_schedule-list limit-8">
              {sliceListSchedules.map((film) => {
                const url = buildUrl(film);
                return (
                  <li key={film._id}>
                    <a href={url} className="tsl-link">
                      <div className="time">{moment(film.time_schedule).format('HH:mm')}</div>
                      <div className="film-detail">
                        <h3 className="film-name dynamic-name" data-jname="Deatte 5-byou de Battle">{film.original_title}</h3>
                        <div className="fd-play">
                          <button type="button" className="btn btn-sm btn-play">
                            <i className="fas fa-play mr-2" />
                            {`Episode ${film.episode || 'Full'}`}
                          </button>
                        </div>
                      </div>
                    </a>
                  </li>
                );
              })}
            </ul>
            {filterListSchedules.length > 5 && (
              <button
                onClick={() => { setShowAppends(!showAppends); }}
                id="scl-more"
                className="btn btn-sm btn-block btn-showmore"
              >
                {showAppends ? 'Collapse' : 'Show more'}
              </button>
            )}
          </div>
        </div>
      </section>
    </div>
  );
}
