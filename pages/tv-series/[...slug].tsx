import Container from 'containers/Watch/TVs';
import { GetServerSideProps } from 'next';
import React from 'react';
import { getInfoTVFilm } from '~/pages/api/tv-series/[id]';
import { buildTitle, parseStringToObject } from '~/utils/common';
import { KEYS_REDIS } from '~/utils/config';
import { CONFIG_TITLE_DES_PAGE, TYPE_ADS_PAGE } from '~/utils/contains';
import { redisGetAsync, redisGetValAsync } from '~/utils/redis';

function Watch(props) {
  return (
    <Container {...props} />
  );
}

export const getServerSideProps: GetServerSideProps = async (props) => {
  const { slug }: any = props.query;
  // const [id] = (slug[0] || '').toString().split('-').reverse();
  const [id] = (slug[0] || '').toString().split('.').reverse();

  const titleKey = CONFIG_TITLE_DES_PAGE.tv.key;

  const [underPlayer, resp, topInfoFilm, botInfoFilm, doc] = await Promise.all([
    redisGetAsync(TYPE_ADS_PAGE.watch_under_player.key),
    getInfoTVFilm(id, true),
    redisGetAsync(TYPE_ADS_PAGE.top_information_film.key),
    redisGetAsync(TYPE_ADS_PAGE.under_information_film.key),
    redisGetValAsync(titleKey),
  ]);
  const underPlayerParse = parseStringToObject(underPlayer);
  const topInfoFilmParse = parseStringToObject(topInfoFilm);
  const botInfoFilmParse = parseStringToObject(botInfoFilm);

  const [epString] = slug.reverse();
  const [epText] = epString.replace('.html', '').split('-').reverse();

  const {
    title: TitleData, description, release_year, quality,
  } = resp.data.data;

  const title = buildTitle(doc?.title || '', {
    title: TitleData, release_year, quality, epText,
  });

  return {
    props: {
      title,
      description,
      itemFilm: resp.data.data,
      underPlayer: underPlayerParse?.content || '',
      topInfoFilm: topInfoFilmParse?.content || '',
      botInfoFilm: botInfoFilmParse?.content || '',
    },
  };
};

export default Watch;
