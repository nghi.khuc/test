/* eslint-disable no-bitwise */
/* eslint-disable import/no-self-import */
import Redis from 'redis';
import { promisify } from 'util';
import moment from 'moment';
import configSystem, { KEYS_REDIS } from './config';

const client = Redis.createClient({
  host: configSystem.REDIS.HOST,
  port: configSystem.REDIS.PORT,
  password: configSystem.REDIS.PASSWORD,
});

export const redis = () => client;

export const redisGetAsync = promisify(client.get).bind(client);
export const redisDelAsync = promisify(client.del).bind(client);

export const redisGetValAsync = async (key) => {
  const val = await redisGetAsync(key);
  if (!val) return false;
  return JSON.parse(val);
};

export const redisSetAsync = async (key, result, timeForce?) => {
  const redisAsync = redis();
  const time = timeForce || await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING);
  redisAsync.set(key, JSON.stringify(result), 'EX', time);
};

export const redisZaddAsync = promisify(client.zadd).bind(client);
export const redisZscoreAsync = promisify(client.zscore).bind(client);
export const redisZRevRangeAsync = promisify(client.zrevrange).bind(client);
export const redisZincrbyAsync = promisify(client.zincrby).bind(client);

export const getCurrentDate = (time?: string) => (time ? moment.utc(moment(time, 'DD/MM/YYYY')).add(7, 'hours') : moment.utc(moment()).add(7, 'hours'));
export const getCurrent = () => moment.utc(moment()).add(7, 'hours');

export const getMonthLeft = () => {
  // const tolerance = 0;

  const now = getCurrent();
  const lastMonth = now.clone().endOf('month');
  const valueOfLastMonth = lastMonth.valueOf();
  const valueOfNow = now.valueOf();
  // getCurrent().add(getMonthLeft(), 'seconds').format('DD/MM/YYYY') // * usecase
  return ((valueOfLastMonth - valueOfNow) / 1000) | 0;
};

export const timeCachingContent = async (defaultVal) => {
  const value = await redisGetAsync(KEYS_REDIS.CONFIG);
  if (!value) return defaultVal;

  const { timeCacheContent } = JSON.parse(value);
  if (+timeCacheContent > 0) return +timeCacheContent;
  return defaultVal;
};

export const timeCachingSystem = async (defaultVal) => {
  const value = await redisGetAsync(KEYS_REDIS.CONFIG);
  if (!value) return defaultVal;

  const { timeCacheSetting } = JSON.parse(value);
  if (+timeCacheSetting > 0) return +timeCacheSetting;
  return defaultVal;
};

// const a = await client.ZADD('test', 1, id)
// const b = await client.zscore('test', id);
// const d = await client.zrevrange('test', 0, 20, 'withscores');
// const c = await client.zincrby('test', 1, id);

export default redis;
