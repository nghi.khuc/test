import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import VaultURLs from '~/utils/contains';
import {
  clearCookieToken, exception, getCookieToken, HTTP_STATUS, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const data: any = getCookieToken({ req });
    if (!data) {
      clearCookieToken({ req, res });
      return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    }
    let params = {};
    if (req.query.type === 'movies') {
      params = {
        movie_id: req.query.id,
      };
    } else {
      params = {
        tv_series_id: req.query.id,
      };
    }
    if (req.method === 'POST') {
      const baseURL = `${VaultURLs.API_URL}/api/user/favorite/add`;
      const result = await axios({
        url: baseURL,
        method: 'POST',
        data: params,
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });

      return res.RESP(result);
    }
    if (req.method === 'DELETE') {
      const baseURL = `${VaultURLs.API_URL}/api/user/favorite/delete`;

      const result = await axios({
        url: baseURL,
        method: 'DELETE',
        data: params,
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });

      return res.RESP(result);
    }

    const baseURL = `${VaultURLs.API_URL}/api/user/favorite/list`;

    const result = await axios({
      url: baseURL,
      method: 'POST',
      data: {
        type: req.query.type,
        page: req.query.page || 1,
        limit: 20,
      },
      headers: {
        Authorization: `bearer ${data.token}`,
      },
    });

    return res.RESP(result);
  } catch (e) {
    trackLog(req, e);
    clearCookieToken({ req, res });
    return res.status(200).send(undefined);
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
