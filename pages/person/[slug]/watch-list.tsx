/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
import Container from 'containers/User/WatchListOrigin';
import { useRouter } from 'next/router';
import React from 'react';
import { useSelector } from 'react-redux';
import { deleteFavoriteFilm, getUserProfileFavoriteMovie, getUserProfileFavoriteTV } from '~/services/client';

export function Page() {
  const router = useRouter();
  const user = useSelector((props:any) => props.userSlices?.user);

  const [tab, setTab] = React.useState('movies');
  const [page, setPage] = React.useState(1);
  const [state, setState] = React.useState({
    movies: {},
    tv: {},
  });

  const changeTab = (val) => {
    setPage(1);
    setTab(val);
    getInfo(1, val);
  };

  const getInfo = async (pageParam?, tabParam?) => {
    const newTab = tabParam || tab;
    setPage(pageParam);
    if (state[newTab][pageParam]) return;
    if (newTab === 'movies') {
      const data = await getUserProfileFavoriteMovie(pageParam);
      setState({
        ...state,
        movies: {
          [pageParam]: data,
        },
      });
    }

    if (newTab === 'tv') {
      const data = await getUserProfileFavoriteTV(pageParam);
      setState({
        ...state,
        tv: {
          [pageParam]: data,
        },
      });
    }
  };

  const deleteFilm = (id) => {
    const paramsSubmit = { id, type: tab };
    deleteFavoriteFilm(paramsSubmit);
    const data = state[tab][page].data.filter((item) => item._id !== id);
    setState({
      ...state,
      [tab]: {
        ...state[tab],
        [page]: {
          ...state[tab][page],
          data,
        },
      },
    });
  };

  React.useEffect(() => {
    getInfo();
  }, []);

  return (
    <Container user={user} tab={tab} changeTab={changeTab} setPage={getInfo} page={page} state={state[tab][page]} deleteFilm={deleteFilm} />
  );
}

Page.PAGE_NOT_USE_ASIDE = true;

export default Page;
