import isEmpty from 'lodash/isEmpty';
import React, { useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import Image from 'next/image';
import LazyLoad from 'react-lazyload';
import { fetchFilmById, setItemCodeActive, setPosition } from '~/stores/slices/FilmSlice';
import { ItemFilm } from '~/stores/slices/FilmSlice/types';

import ToolTip from '../ToolTip';
import { buildUrl } from '~/utils/common';

function index({ listFilms, title }: any) {
  const dispatch = useDispatch();

  const { qtipItem, itemCodeActive } = useSelector((state: any) => ({
    qtipItem: state.filmSlices.qtipItem,
    itemCodeActive: state.filmSlices.itemCodeActive,
  }), shallowEqual);

  const onMouseEnter = (item: ItemFilm) => () => {
    if (item.type && item._id) {
      const code = item._id;
      dispatch(setPosition({
        left: 70,
        top: 140,
      }));
      dispatch(setItemCodeActive({
        code,
        item,
        type: 'list_film',
      }));
      if (!qtipItem[code]) {
        dispatch(fetchFilmById({ id: code, type: item.type, toolTip: true }));
      }
    }
  };

  const onMouseLeave = useCallback(() => {
    dispatch(setPosition({}));
    dispatch(setItemCodeActive({}));
  }, []);

  return (
    <section className="block_area block_area-anime none-bg">
      <div className="block_area-header block_area-header-tabs">
        {!!title && (
          <div className="float-left bah-heading mr-4">
            <h2 className="cat-heading">{title}</h2>
          </div>
        )}
        <div className="clearfix" />
      </div>
      <div className="block_area-content block_area-list film_list film_list-grid" onMouseLeave={onMouseLeave}>
        <div className="film_list-wrap">
          {!isEmpty(listFilms) ? listFilms?.map((film: ItemFilm, idx: number) => {
            const url = buildUrl(film);
            return (
              <div
                key={film.id ? `${film.id}-${film.slug}` : idx}
                onMouseEnter={onMouseEnter(film)}
                className="flw-item item-qtip"
              >
                <div className="film-poster" style={{ paddingBottom: 'unset' }}>
                  <div className="tick-item tick-quality">{film.quality}</div>
                  <div className="tick rtl">
                    <div className="tick-item tick-eps">
                      {film.total_episodes ? `Ep ${film.episode_new || film.total_episodes}/${film.total_episodes}` : 'Ep Full'}
                    </div>
                  </div>
                  {!!film.season_new && (
                    <div className="tick ltr">
                      <div className="tick-item tick-sub">
                        {`SS ${film.season_new}`}
                      </div>
                    </div>
                  )}
                  <Image
                    className="film-poster-img lazyload"
                    src={film.poster}
                    alt={film.title}
                    width={186}
                    height={260}
                  />
                  {/* <img
                  data-src={film.poster}
                  src={film.poster}
                  className="film-poster-img lazyload"
                  alt={film.title}
                /> */}
                  <a href={url} className="film-poster-ahref">
                    <i
                      className="fas fa-play"
                    />
                  </a>
                </div>
                <div className="film-detail">
                  <h3 className="film-name">
                    <a
                      href="watch/life-lessons-with-uramichi-oniisan-15594"
                      title="Life Lessons with Uramichi Oniisan"
                      className="dynamic-name"
                      data-jname="Uramichi Oniisan"
                    >
                      {film.title}
                    </a>
                  </h3>
                </div>
                {(itemCodeActive?.code === film._id && itemCodeActive.type === 'list_film') ? <ToolTip /> : null}
                <div className="clearfix" />
              </div>
            );
          }) : null}
        </div>
        <div className="clearfix" />
      </div>

    </section>
  );
}

export default index;
