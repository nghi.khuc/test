/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { loginAction, registerAction, resetPasswordAction } from '~/stores/slices/UserSlices';

// const STATE = {
//   1: 'LOGIN',
//   2: 'FORGOT',
//   3: 'REGISTER',
// };

const FormLogin = () => {
  const [state, setState] = React.useState(1);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const onSubmitValue = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [email, password, remember] = event.target;
      const valueEmail = email.value as string;
      const valuePassword = password.value as string;
      await dispatch(loginAction({ email: valueEmail, password: valuePassword, remember: remember.checked }));
      const element: any = document.getElementById('modallogin');
      const elementModal: any = document.getElementsByClassName('modal-backdrop')[0];
      element.className = element.className.replace('show', '');
      setTimeout(() => {
        element.focus();
        element.style.display = 'none';
        document.body.removeAttribute('class');
        document.body.removeAttribute('style');
        elementModal.remove();
      }, 300);
    } catch {
      console.error('fail login');
    } finally {
      setLoading(false);
    }
  };

  const onResetPassword = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [email] = event.target;
      const valueEmail = email.value as string;
      if (!valueEmail) return;
      const resp: any = await dispatch(resetPasswordAction(valueEmail));
      if (!resp?.payload) return;
      const element: any = document.getElementById('modallogin');
      const elementModal: any = document.getElementsByClassName('modal-backdrop')[0];
      element.className = element.className.replace('show', '');
      setTimeout(() => {
        element.focus();
        element.style.display = 'none';
        document.body.removeAttribute('class');
        document.body.removeAttribute('style');
        elementModal.remove();
      }, 300);
    } catch {
      console.error('fail login');
    } finally {
      setLoading(false);
    }
  };

  const onRegister = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [name, email, password] = event.target;
      const valueEmail = email.value as string;
      const valuePassword = password.value as string;
      const valueName = name.value as string;
      await dispatch(registerAction({ email: valueEmail, password: valuePassword, username: valueName }));
      const element: any = document.getElementById('modallogin');
      const elementModal: any = document.getElementsByClassName('modal-backdrop')[0];
      element.className = element.className.replace('show', '');
      setTimeout(() => {
        element.style.display = 'none';
        document.body.removeAttribute('class');
        document.body.removeAttribute('style');
        elementModal.remove();
        setState(1);
      }, 300);
    } catch {
      console.error('fail register');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="modal fade premodal premodal-login" id="modallogin" tabIndex={-1} role="dialog" aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="tab-content">
            <div id="modal-tab-login" className={`tab-pane ${state === 1 ? 'active show' : ''}`}>
              {/* <div className="modal-login-icon"><img src="/images/icon-login2.png" alt="" /></div> */}
              <div className="modal-header">
                <h5 className="modal-title">Member Login</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="description">9anime - a better place to watch anime online for free!</div>
                <div className="alert alert-danger" id="login-error" style={{ display: 'none' }} />
                <form className="loginform" id="login-form" onSubmit={onSubmitValue}>
                  <div className="form-group w-icon">
                    <input type="text" className="form-control" id="email" placeholder="Your Email" name="email" required />
                    <i className="fas fa-user" />
                  </div>
                  <div className="form-group w-icon">
                    <input type="password" className="form-control" id="password" placeholder="Password" name="password" required />
                    <i className="fas fa-lock" />
                  </div>
                  <div className="form-check custom-control custom-checkbox">
                    <div className="float-left">
                      <input type="checkbox" className="custom-control-input" id="remember" />
                      <label className="custom-control-label" htmlFor="remember">Remember me</label>
                    </div>
                    <div className="float-right">
                      <a href="#" className="link-highlight text-forgot forgot-tab-link" onClick={() => setState(2)}>Forgot password?</a>
                    </div>
                    <div className="clearfix" />
                  </div>
                  <div className="form-group login-btn mb-0">
                    <button type="submit" id="btn-login" className="btn btn-primary btn-block btn-form">Login</button>
                    <div className="loading-relative" id="login-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="modal-footer text-center">
                Don`t have an account?
                {' '}
                <a className="link-highlight register-tab-link" onClick={() => setState(3)}>Register</a>
              </div>
            </div>
            <div id="modal-tab-forgot" className={`tab-pane fade ${state === 2 ? 'active show' : ''}`}>
              <div className="modal-login-icon"><img src="/images/icon-forgot.png" alt="" /></div>
              <div className="modal-header">
                <h5 className="modal-title">Reset Password</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="description">We will send an email to your box, just follow that link to set your new password.</div>
                <form className="loginform" id="forgot-form" onSubmit={onResetPassword}>
                  <div className="form-group w-icon">
                    <input type="text" className="form-control" id="forgot-email" placeholder="Your email" />
                    <i className="fas fa-user" />
                  </div>
                  <div className="form-group login-btn mb-0">
                    <button className="btn btn-primary btn-block btn-form" type="submit">Reset</button>
                  </div>
                </form>
              </div>
              <div className="modal-footer text-center">
                <a className="link-highlight login-tab-link" onClick={() => setState(1)}>Back to Sign-in</a>
              </div>
            </div>
            <div id="modal-tab-register" className={`tab-pane fade ${state === 3 ? 'active show' : ''}`}>
              {/* <div className="modal-login-icon"><img src="/images/icon-register.png" alt="" /></div> */}
              <div className="modal-header">
                <h5 className="modal-title">Register</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="description">When becoming members of the site, you could use the full range of functions.</div>
                <div className="alert alert-danger" id="register-error" style={{ display: 'none' }} />
                <form className="loginform" method="post" id="register-form" onSubmit={onRegister}>
                  <div className="form-group w-icon">
                    <input type="text" className="form-control" id="re-username" placeholder="Your Name" name="name" required />
                    <i className="fas fa-user" />
                  </div>
                  <div className="form-group w-icon">
                    <input type="text" className="form-control" id="re-email" placeholder="Email" name="email" required />
                    <i className="fas fa-envelope" />
                  </div>
                  <div className="form-group w-icon">
                    <input type="password" className="form-control" id="re-password" placeholder="Your password" name="password" required />
                    <i className="fas fa-lock" />
                  </div>
                  <div className="form-group w-icon">
                    <input type="password" className="form-control" id="re-confirmpassword" placeholder="Repeat your password" name="repeatPassword" required />
                    <i className="fas fa-lock" />
                  </div>
                  <div className="form-group login-btn mb-0">
                    <button type="submit" id="btn-register" className="btn btn-primary btn-block btn-form">Register</button>
                    {/* <div className="loading-relative" id="register-loading" style={{ display: 'none' }}> */}
                    <div className="loading-relative" id="register-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="modal-footer text-center">
                Have an account?
                {' '}
                <a className="link-highlight login-tab-link" onClick={() => setState(1)}>Sign-in</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormLogin;
