/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import LazyLoad from 'react-lazyload';
import { postLogout } from '~/services/client';

const Mal = ({ user }:any) => (

  <div className="profile-container">
    <div className="prc_-menu">
      <div className="prc__-icon"><img src={user?.avatar || '/images/user-icon.png'} alt="avatar" /></div>
      <div className="prc__-list">
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}`} className="btn">Edit Profile</a>
        </div>
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}/watch-list`} className="btn ">Watch List</a>
        </div>
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}/mal`} className="btn active">MAL Import</a>
        </div>
        <div
          className="item"
          onClick={async () => {
            await postLogout();
            setTimeout(() => {
              window.location.href = '/home';
            }, 500);
          }}
        >
          <a href="#" className="btn">Logout</a>
        </div>
        <div className="clearfix" />
      </div>
    </div>
    <div className="prc_-main">
      <section className="block_area block_area-profile">
        <div className="block_area-header block_area-header-tabs">
          <div className="float-left bah-heading mr-4">
            <h2 className="cat-heading">MAL Import</h2>
          </div>
          <div className="clearfix" />
        </div>
        <div className="block_area-content">
          <div className="description">
            <p className="mb-0">
              - If an anime is available in your MAL list but not available in the
              site, it will not be imported.
            </p>
            <p className="mb-0">- This process will takes a moment, please be patient.</p>
          </div>
          <form className="form-dark" method="post" id="import-form">
            <input type="hidden" name="erase_list" defaultValue={0} />
            <div className="form-group">
              <div className="row">
                <label className="col-sm-3 col-form-label">MAL username</label>
                <div className="col-sm-9">
                  <input type="text" className="form-control" name="mal_username" required />
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="row">
                <label className="col-sm-3 col-form-label pt-0">Erase your list first</label>
                <div className="col-sm-9">
                  <div className="toggle-onoff off" id="sw-erase-list"><span /></div>
                  <div className="small mt-2">
                    Delete all anime in your list before process the
                    import, otherwise this will merge to your existing list.
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group row text-center mt-5">
              <div className="col-sm-12">
                <button className="submit btn btn-primary more-padding">Import</button>
                <div className="loading-relative" id="import-loading" style={{ display: 'none' }}>
                  <div className="loading">
                    <div className="span1" />
                    <div className="span2" />
                    <div className="span3" />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </div>
    <div className="clearfix" />
  </div>
);
export default Mal;
