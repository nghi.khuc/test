/* eslint-disable react/destructuring-assignment */
import React from 'react';

export default function Logo(props: any) {
  return (
    <a href="/" id="logo" title="">
      <img src={props?.config?.logo} title="" alt="" />
      <div className="clearfix" />
    </a>
  );
}
