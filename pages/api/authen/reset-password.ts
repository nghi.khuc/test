import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import VaultURLs from '~/utils/contains';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/password/reset`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const { email } = req.body;
    const resp = await axios({
      url: `${baseURL}/${email}`,
      method: 'GET',
    });
    const { status, message } = resp.data;
    if (!status) return res.ERROR(HTTP_STATUS.ERROR, message);
    return res.RESP('oke');
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
