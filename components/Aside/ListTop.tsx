/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/prop-types */
import React, { useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
  fetchFilmById, setPosition, setItemCodeActive,
} from 'stores/slices/FilmSlice';
import isEmpty from 'lodash/isEmpty';
import Image from 'next/image';
import LazyLoad from 'react-lazyload';
import styled from 'styled-components';
import { ItemFilm } from '~/stores/slices/FilmSlice/types';
import ToolTip from '../ToolTip';
import { buildUrl } from '~/utils/common';

const LiStyled = styled.li`
  /* cursor: pointer; */
`;

const Loading = ({ index }: any) => (
  <li
    className={index === 0 ? 'item-top' : ''}
  >
    <div className="film-number"><span>{index + 1}</span></div>
    <div
      className="film-poster item-qtip"
    >
      <Image
        className="film-poster-img lazyload"
        src="/images/black-slider.jpeg"
        alt="."
        width={index === 0 ? 330 : 46}
        height={index === 0 ? 160 : 60}
      />
    </div>
    <div className="film-detail">
      <h3 className="film-name">
        <a
          href="#"
          title="loading"
          className="dynamic-name"
          data-jname="loading"
        >
          {' '}
        </a>
      </h3>
      <div className="fd-infor">
        <span className="fdi-item mr-2">
          <i className="fas fa-eye mr-1" />
          {' '}
        </span>
      </div>
    </div>
    <div className="clearfix" />
  </li>
);

function ListTop({ listFilms, page }) {
  const dispatch = useDispatch();
  const { qtipItem, itemCodeActive } = useSelector((state: any) => ({
    qtipItem: state.filmSlices.qtipItem,
    itemCodeActive: state.filmSlices.itemCodeActive,
  }), shallowEqual);

  const onMouseEnter = useCallback((item: ItemFilm) => () => {
    if (item._id) {
      const code = item._id;
      dispatch(setPosition({ left: '-100%', top: 10 }));

      dispatch(setItemCodeActive({ code, item, type: 'top_view' }));

      if (!qtipItem[code]) {
        dispatch(fetchFilmById({ id: code, type: item.type, toolTip: true }));
      }
    }
  }, [itemCodeActive]);

  return (
    <React.Fragment>
      {!isEmpty(listFilms) ? listFilms.map((film: ItemFilm, index) => {
        const url = buildUrl(film);
        return (
          // <LazyLoad key={`${film._id}-${index}`} placeholder={<Loading index={index} />}>
          <LiStyled
            key={`${film._id}-${index}`}
            onMouseLeave={() => { setPosition({}); }}
            className={index === 0 ? 'item-top' : ''}
          >
            <a href={url}>
              <div className="film-number"><span>{((+page - 1) * 10) + (index + 1)}</span></div>
              <div
                className="film-poster item-qtip"
                onMouseEnter={onMouseEnter(film)}
                data-id={film._id}
                data-hasqtip={film._id}
                aria-describedby={`qtip${film._id}`}
              >
                {/* <img
                  data-src={index === 0 ? film.backdrop : film.poster}
                  className="film-poster-img lazyloaded"
                  alt={film.title}
                  src={index === 0 ? film.backdrop : film.poster}
                /> */}
                {/* // 330 160 46 60 */}
                <Image
                  className="film-poster-img lazyload"
                  src={index === 0 ? film.backdrop : film.poster}
                  alt={film.title}
                  width={index === 0 ? 330 : 46}
                  height={index === 0 ? 160 : 60}
                />
              </div>

              <div className="film-detail">
                <h3 className="film-name">
                  <a
                    href={url}
                    title={film.title}
                    className="dynamic-name"
                    data-jname={film.title}
                  >
                    {film.title}
                  </a>
                </h3>
                <div className="fd-infor">
                  <span className="fdi-item mr-2">
                    <i className="fas fa-eye mr-1" />
                    {film.views}
                  </span>
                  {/* <span className="fdi-item">
                <i className="fas fa-heart mr-1" />
                50211
              </span> */}
                </div>
              </div>
              {(itemCodeActive?.code === film._id && itemCodeActive.type === 'top_view') ? <ToolTip /> : null}
            </a>
            <div className="clearfix" />
          </LiStyled>
          // </LazyLoad>
        );
      }) : null}
    </React.Fragment>
  );
}

export default ListTop;
