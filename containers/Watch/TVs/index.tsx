/* eslint-disable indent */
/* eslint-disable eqeqeq */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useEffect, useMemo } from 'react';
import ListFilms from 'components/ListFilms';
import isEmpty from 'lodash/isEmpty';
import { useRouter } from 'next/router';
import LazyLoad from 'react-lazyload';
import qs from 'qs';
import omit from 'lodash/omit';
import Popup from 'reactjs-popup';
import ReactStars from 'react-stars';
import { useSelector } from 'react-redux';
import Details from '../Details';
import DetailCasts from '../DetailCasts';
import Comment from '../Comment';
import WatchTV from './watch';
import { getTVSeriesById, postRating } from '~/services/client';
import { parseURLWatch, toastMessage } from '~/utils/common';

const WatchContainer = ({
  itemFilm, topInfoFilm, botInfoFilm, ...props
}: any) => {
  const [seasons, setSeasons] = useState<any>([]);
  const [similar, setSimilar] = useState<any>([]);
  const [listEpisodes, setListEpisodes] = useState<any>([]);

  const [listServers, setServers] = useState<any>([]);
  const [captions, setCaptions] = useState<any>([]);

  const [episodeState, setEpisodes] = useState<any>(1);
  const [seasonNumber, setSeasonNumber] = useState<any>(1);
  const router = useRouter();
  const { query: { ss } }: any = router;
  const { ep, sv } = parseURLWatch(router.asPath);
  const id = useMemo(() => itemFilm?._id, []);
  const query: any = omit(router.query, 'slug');
  useEffect(() => {
    if (ss) {
      setSeasonNumber(ss);
    }
  }, [ss]);

  const fetchApi = async () => {
    const responseSeasons = await getTVSeriesById(id, { type: 'season' });
    setSeasons(responseSeasons?.data || []);

    const responseSimilars = await getTVSeriesById(id, { type: 'similar' });
    setSimilar(responseSimilars?.data || []);

    const numberSeason = ss || responseSeasons?.data?.length;
    const season = !isEmpty(responseSeasons?.data) ? responseSeasons?.data.find((s) => s.number == numberSeason) : {};
    const [pathName] = decodeURI(router.asPath).split('?');

    const responseEpisodes = await getTVSeriesById(season?._id, { type: 'episode' });
    const episodesOfSeasons = responseEpisodes.data || [];
    setListEpisodes(episodesOfSeasons);
    const firstEpisode = episodesOfSeasons[0];
    // if (!ep) {
    //   setEpisodes(firstEpisode?.number);
    // } else {
    //   setEpisodes(ep);
    // }
    const idEpisode = firstEpisode?._id;
    // const nameEpisode = firstEpisode?.name || '';
    // const pathEp = `${pathName.includes('ep-') ? '' : `/ep-${nameEpisode.replace(/ /g, '-')}-${idEpisode}`}`;
    //

    if (!ss) {
      let newQ = { ...query };
      if (numberSeason > 1) {
        newQ = {
          ...newQ,
          ss: numberSeason,
        };
      }
      window.history.replaceState(window.history.state,
        'option-default-tv-film',
        `${pathName}?${qs.stringify(newQ)}`);

      setSeasonNumber(season?.number);
    }
    const responseStreamsServer = await getTVSeriesById(idEpisode, { type: 'stream' });
    const servers = responseStreamsServer.data || [];
    setCaptions(responseStreamsServer.captions || []);
    setServers(servers);
  };

  useEffect(() => {
    fetchApi();
  }, []);

  const user = useSelector((state: any) => state.userSlices?.user);

  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const openModal = () => setOpen(true);

  const [ratingPoint, setRating] = React.useState();

  const rating = (score) => {
    if (user) {
      setRating(score);
      postRating({ id: itemFilm._id, type: itemFilm.type, score });
      setOpen(false);
      return true;
    }
    toastMessage({ isWarning: true, content: 'Please login' });
    setOpen(false);
    return false;
  };

  return (
    <React.Fragment>
      <Popup open={open} onClose={closeModal}>
        <div className="wrapper-modal-rating">
          <div className="label">{'Rating Film: '}</div>
          <ReactStars
            count={10}
            half={false}
            onChange={rating}
            size={24}
            color2="#ffd700"
          />
        </div>
      </Popup>
      <WatchTV
        {...props}
        sv={sv}
        listServers={listServers}
        captions={captions}
        listEpisodes={listEpisodes}
        seasons={seasons}
        seasonNumber={seasonNumber}
        itemFilm={itemFilm}
        episode={ep}
        openModal={openModal}
      />
      <LazyLoad>
        {
          !!topInfoFilm && <div className="mba-block" dangerouslySetInnerHTML={{ __html: topInfoFilm }} />
        }
        {!isEmpty(itemFilm) && <Details detail={itemFilm} rating={ratingPoint} />}
        {!isEmpty(itemFilm) && <DetailCasts detail={itemFilm} />}
        {
          !!botInfoFilm && <div className="mba-block" dangerouslySetInnerHTML={{ __html: botInfoFilm }} />
        }
        <Comment />
        <ListFilms listFilms={similar} title="Suggestions" />
        {/** Block Tag */}
        <div className="block_area block_area-detail">
          <div className="block_area-content">
            <div className="film-infor">
              <div className="item">
                <div className="item-title">
                  {'Tags: '}
                  {
                    !!itemFilm?.keywords && itemFilm?.keywords.split(',').map((a) => (
                      <a key={a} href={`/tag/${a}`}>
                        {`${a}, `}
                      </a>
                    ))
                  }
                </div>
                <div className="clearfix" />
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>

      </LazyLoad>
    </React.Fragment>
  );
};

export default WatchContainer;
