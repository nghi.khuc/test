import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import fs from 'fs';
import { promisify } from 'util';
import configSystem, {
  FILES_NAME, KEYS_REDIS,
} from '~/utils/config';
import redis, { redisGetAsync, timeCachingContent } from '~/utils/redis';
import {
  exception, HTTP_STATUS, verifyCookieAdmin, withMiddleware,
} from '~/utils/run';
import { trackLog } from '~/services/server';

const keys = {
  1: KEYS_REDIS.INDEX_CONTENT,
  2: KEYS_REDIS.SLIDER_CONTENT,
  3: KEYS_REDIS.FAQ_CONTENT,
};

const files = {
  1: FILES_NAME.INDEX,
  2: FILES_NAME.SLIDER,
  3: FILES_NAME.FAQ,
};

// 1 INDEX
// 2 SLIDER
// 3 FAQ

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    const isAccept = verifyCookieAdmin(req);
    if (!configRequest || !isAccept) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const { type } = req.query;
    const key = keys[type as any];
    const file = files[type as any];

    if (!key) return res.status(401).send('wrong key');

    if (req.method === 'GET') {
      const value = await redisGetAsync(key);
      if (value) return res.status(200).json(JSON.parse(value));
      return res.status(200).json({ content: '' });
    }
    if (req.method === 'POST') {
      const client = redis();
      const data = req.body;
      const dataString = JSON.stringify(data);
      client.set(key, dataString, 'EX', await timeCachingContent(configSystem.EXPIRE_TIME_CACHING));
      await promisify(fs.writeFile)(file, dataString);
      return res.status(200).json(data);
    }
    return res.status(401).send('wrong');
  } catch (e) {
    trackLog(req, e);
    return res.status(200).json({ content: '' });
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
