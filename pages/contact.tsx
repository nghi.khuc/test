/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';

export default function Contact() {
  const [loading, setLoading] = useState(false);
  const onSubmitValue = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();

      const [email, subject, message] = event.target;

      const valueEmail = email.value as string;
      const valueSubject = subject.value as string;
      const valueMessage = message.value as string;
      // await dispatch();
    } catch {
      console.error('fail login');
    } finally {
      setLoading(false);
    }
  };
  return (
    <div className="information_page">
      <div className="container">
        <div className="information_page-wrap">
          <div className="information_page-content">
            <h2 className="h2-heading">Contact Us</h2>
            <p>Please submit your inquiry using the form below and we will get in touch with you shortly.</p>
            <div className="contact-social-icons mt-4 mb-4">
              <div className="block">
                <a href="#" className="btn btn-radius">
                  <div className="icon-rounder icon-rounder-facebook"><i className="fab fa-facebook-f" /></div>
                  <div className="csib-link">Facebook</div>
                  <div className="clearfix" />
                </a>
              </div>
              <div className="block">
                <a href="#" className="btn btn-radius">
                  <div className="icon-rounder icon-rounder-twitter"><i className="fab fa-twitter" /></div>
                  <div className="csib-link">Twitter</div>
                  <div className="clearfix" />
                </a>
              </div>
              <div className="block">
                <a href="#" className="btn btn-radius">
                  <div className="icon-rounder icon-rounder-telegram"><i className="fab fa-telegram-plane" /></div>
                  <div className="csib-link">Telegram</div>
                  <div className="clearfix" />
                </a>
              </div>
              <div className="block">
                <a href="#" className="btn btn-radius">
                  <div className="icon-rounder icon-rounder-discord"><i className="fab fa-discord" /></div>
                  <div className="csib-link">Discord</div>
                  <div className="clearfix" />
                </a>
              </div>
              <div className="block">
                <a href="#" className="btn btn-radius">
                  <div className="icon-rounder icon-rounder-reddit"><i className="fab fa-reddit-alien" /></div>
                  <div className="csib-link">Reddit</div>
                  <div className="clearfix" />
                </a>
              </div>
              <div className="clearfix" />
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-8 col-md-12">
                <form className="form-dark" onSubmit={onSubmitValue}>
                  <div className="form-group">
                    <label htmlFor="contact-email">Your email</label>
                    <input type="email" className="form-control" id="contact-email" placeholder="name@example.com" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="contact-subject">Subject</label>
                    <input type="text" className="form-control" id="contact-subject" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="contact-textarea">Message</label>
                    <textarea
                      className="form-control form-control-textarea"
                      id="contact-textarea"
                      rows={3}
                      cols={3}
                      name="message"
                      maxLength={3000}
                      required={false}
                    />
                  </div>
                  <div className="form-group">
                    <div className="mt-5" />
                    <button type="submit" id="btn-focus" className="btn btn-block btn-lg btn-focus">Submit</button>
                  </div>
                  <div className="form-group contact-btn mb-0">
                    <div className="loading-relative" id="login-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
