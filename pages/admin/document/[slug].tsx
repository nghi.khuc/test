/* eslint-disable jsx-a11y/label-has-associated-control */
import { useRouter } from 'next/router';
import React from 'react';
import {
  getConfigDoc, postConfigDoc,
} from '~/services/client';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { withLogin } from '..';
import { convertStringUpperCaseFirstChar } from '~/utils/common';

function Ads() {
  const router = useRouter();
  const { query: { slug } }: any = router;
  const { type, keyReplace } = CONFIG_TITLE_DES_PAGE[slug];

  const [data, setData] = React.useState<any>();
  const [loading, setLoading] = React.useState(false);

  const fetch = async () => {
    const resp = await getConfigDoc({ type });
    setData(resp);
  };

  React.useEffect(() => {
    fetch();
  }, []);

  const submit = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [
        title,
        description,
      ] = event.target;
      const titleText = title.value as string;
      const descriptionText = description.value as string;
      await postConfigDoc({ title: titleText, description: descriptionText }, type);
      router.push('/admin');
    } catch {
      console.error('post config failed');
    } finally {
      setLoading(false);
    }
  };

  if (!data) return <div>loading</div>;

  const title = convertStringUpperCaseFirstChar(`${CONFIG_TITLE_DES_PAGE[slug]?.key}`);
  return (
    <div className="information_page">
      <div className="container">
        <div className="information_page-wrap">
          <div className="information_page-content">
            <h2 className="h2-heading">{`Page - ${title}`}</h2>
            <p>
              {`Please input the correct KEY: ${keyReplace} `}
            </p>
            <p>
              {`Example: ${keyReplace} TV | Movie ${keyReplace}`}
            </p>
            <div className="row">
              <div className="col-xl-6 col-lg-8 col-md-12">
                <form className="form-dark" onSubmit={submit}>
                  <div className="form-group">
                    <label htmlFor="post-domain">Title</label>
                    <input
                      defaultValue={data?.title}
                      type="text"
                      className="form-control"
                      id="post-domain"
                      placeholder="Title"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-time-cache-content">Description</label>
                    <input
                      defaultValue={data?.description}
                      type="text"
                      className="form-control"
                      id="post-time-cache-content"
                      placeholder="description"
                    />
                  </div>
                  <div className="form-group">
                    <div className="mt-5" />
                    <button type="submit" id="btn-focus" className="btn btn-block btn-lg btn-focus">Submit</button>
                  </div>
                  <div className="form-group post-btn mb-0">
                    <div className="loading-relative" id="login-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withLogin(Ads);
