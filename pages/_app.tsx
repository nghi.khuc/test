import React from 'react';
import { Provider } from 'react-redux';
import { checkMobile, useScreenSize } from 'utils/useScreenSize';
import {
  configTemplate, PAGE_NOT_USE_ASIDE, REGEX_PAGE_USE_QUICK_FILTER, TYPE_ADS_PAGE,
} from 'utils/contains';
import { AppContextType } from 'next/dist/shared/lib/utils';

import Head from 'next/head';
import Layout from '~/components/layouts';
import store from '../stores';
import '../styles/home.css';
import '../styles/globals.css';
import '../styles/styles-commons.css';
import 'reactjs-popup/dist/index.css';
// dist/index.css
import { parseStringToObject } from '~/utils/common';

function MyApp({
  Component, pageProps, isServer, agent, config, isError = false,
}: any) {
  if (isError) return 404;
  const isShowAside = PAGE_NOT_USE_ASIDE.test(pageProps?.router.pathname) || !!Component.PAGE_NOT_USE_ASIDE;
  const isShowQuickFilter = REGEX_PAGE_USE_QUICK_FILTER.test(pageProps?.router.pathname);
  const isMobileSsr = checkMobile(agent);
  const isMobileClient = useScreenSize().isMobile;
  const props = { ...pageProps, isMobileSsr: isServer ? isMobileSsr : isMobileClient };

  const title = React.useMemo(() => pageProps.title || config?.title || '', []);
  const description = React.useMemo(() => pageProps.description || config?.description || '', []);
  return (
    <React.Fragment>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={title} />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <meta name="description" content={description || ''} />
        <meta property="og:description" content={description || ''} />
      </Head>
      <Provider store={store}>
        <Layout {...props} config={config} isShowAside={!isShowAside} isShowQuickFilter={isShowQuickFilter}>
          <Component {...props} config={config} />
        </Layout>
      </Provider>
    </React.Fragment>
  );
}

MyApp.getInitialProps = async (props: AppContextType) => {
  const { ctx, ...other } = props;

  const initProps = {
    isServer: !!ctx?.req,
    pageProps: {
      ...other,
    },
  };

  if (ctx.asPath === '/favicon.ico') return { ...initProps, isError: true };

  if (ctx?.req) {
    const fn = await import('~/utils/run');
    const { redisGetAsync } = await import('~/utils/redis');
    const { KEYS_REDIS } = await import('~/utils/config');
    const [contentTopFooter, contentTopFilter, contentBotFilter] = await Promise.all([
      redisGetAsync(TYPE_ADS_PAGE.top_footer.key),
      redisGetAsync(TYPE_ADS_PAGE.top_filter_sidebar.key),
      redisGetAsync(TYPE_ADS_PAGE.under_filter_sidebar.key),
    ]);

    const contentTopFooterParse = parseStringToObject(contentTopFooter);
    const contentTopFilterParse = parseStringToObject(contentTopFilter);
    const contentBotFilterParse = parseStringToObject(contentBotFilter);

    const configPage = await redisGetAsync(KEYS_REDIS.CONFIG);
    const config = parseStringToObject(configPage) || configTemplate;
    fn.runSession({ req: ctx.req, res: ctx.res });
    const agent = ctx.req.headers['user-agent'];
    return {
      ...initProps,
      agent,
      config: {
        contentTopFooter: contentTopFooterParse?.content || '',
        contentTopFilter: contentTopFilterParse?.content || '',
        contentBotFilter: contentBotFilterParse?.content || '',
        title: config.title,
        description: config.description,
        analytic: config.analytic,
        logo: config.logo,
        footerText: config.footerText,
        contacts: config.contacts,
      },
    };
  }

  return initProps;
};

export default MyApp;
