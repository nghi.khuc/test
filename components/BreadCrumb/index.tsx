import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { BREAD_CRUMB_PAGE, IGNORE_SLUG } from 'utils/contains';

const convertBreadcrumb = (str: string) => {
  if (!str) return '';

  str = decodeURI(str);
  const splitSrt = str.split('.');
  const newStr = /(\d+)/i.test(splitSrt[splitSrt.length - 1]) ? splitSrt.slice(0, splitSrt.length - 1).join(' ') : splitSrt.join(' ');
  return (newStr || str)
    .replace(/-/g, ' ')
    .replace(/oe/g, 'ö')
    .replace(/ae/g, 'ä')
    .replace(/ue/g, 'ü')
    .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
};

interface IPathArray {
  breadcrumb: string;
  href: string;
}

function BreadCrumb() {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState<IPathArray[]>([]);
  useEffect(() => {
    if (router) {
      const linkPath = router.asPath.split('/');
      linkPath.shift();

      const pathArray = linkPath.filter((slug) => !IGNORE_SLUG.test(slug))
        .map((path, i) => ({ breadcrumb: path.split('?')[0], href: `/${linkPath.slice(0, i + 1).join('/')}` }));

      setBreadcrumbs(pathArray);
    }
  }, [router]);

  if (!breadcrumbs.length) {
    return null;
  }
  const pathHome = router.asPath.split('/')[1] === 'home' || !router.asPath.split('/')[1];

  return (
    pathHome ? null
      : (
        <div className="prebreadcrumb">
          <div className="container">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="/home" title="Home">Home</a></li>
                {breadcrumbs.map((breadcrumb, i) => (
                  (breadcrumbs.length - 1) === i ? (
                    <li key={i} className="breadcrumb-item active" aria-current="page">
                      {convertBreadcrumb(BREAD_CRUMB_PAGE[breadcrumb.breadcrumb] || breadcrumb.breadcrumb)}
                    </li>
                  )
                    : (
                      <li key={i} className="breadcrumb-item active" aria-current="page">
                        <a href={breadcrumb.href}>
                          {convertBreadcrumb(BREAD_CRUMB_PAGE[breadcrumb.breadcrumb] || breadcrumb.breadcrumb)}
                        </a>
                      </li>
                    )
                ))}
              </ol>
            </nav>
          </div>
        </div>
      )
  );
}

export default BreadCrumb;
