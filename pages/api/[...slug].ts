import axios from 'axios';
import { trackLog } from '~/services/server';
import configSystem from '~/utils/config';
import VaultURLs from '~/utils/contains';
import redis, { redisGetAsync, timeCachingSystem } from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';

const handler = async (req, res) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const key = `${configRequest.url.replace(`${baseURL}/`, '')}-${configRequest.method}`;
    const client = redis();
    const value = await redisGetAsync(key);
    if (value) return res.RESP(JSON.parse(value));

    const result = await axios(configRequest);
    const dataCache = {
      data: {
        data: result.data.data,
      },
    };
    client.set(key, JSON.stringify(dataCache), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
    return res.RESP(result);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
