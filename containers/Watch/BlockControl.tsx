/* eslint-disable react/require-default-props */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from 'react';

interface IPropControlBlock {
  setAutoNext?: any;
  onToggleLight: () => void;
  setAutoPlay: any;
  ss: any;
  episodeState: any;
  onChangeEpisode: any;
  stateLight: string | number | boolean,
  isAutoPlay: number | boolean,
  isAutoNext?: number | boolean,
}

const BlockControl = ({
  setAutoNext,
  onToggleLight,
  setAutoPlay,
  ss,
  episodeState,
  onChangeEpisode,
  stateLight,
  isAutoPlay,
  isAutoNext,
}: IPropControlBlock) => {
  const [expand, setExpand] = useState(false);
  const onClickExpand = () => {
    const elementWrapper: any = document.getElementById('main-wrapper');
    const elementWatch: any = document.getElementById('watch-block');
    const elementExtenPlayer: any = document.getElementById('player-extend');
    if (!expand) {
      elementWatch.className = 'active';
      elementExtenPlayer.className = 'player-extend-display active';
      elementWrapper.className = 'layout-page layout-page-watch';
      setExpand(true);
    } else {
      elementWatch.className = '';
      elementExtenPlayer.className = 'player-extend-display';
      elementWrapper.className = '';
      setExpand(false);
    }
  };
  return (
    <div className="player-controls">
      <div
        className="pc-item pc-toggle pc-autoplay"
        onClick={() => {
          setAutoPlay();
        }}
      >
        <div className={`toggle-basic quick-settings ${isAutoPlay ? '' : 'off'}`} data-option="auto_play">
          <i className="far fa-check-square tb-on" />
          <i className="far fa-square tb-off" />
          <span className="tb-name"> Auto Play</span>
        </div>
      </div>
      {/* {!!isAutoNext && ( */}
      <div className="pc-item pc-toggle pc-autonext" onClick={() => { setAutoNext(); }}>
        <div className={`toggle-basic quick-settings ${isAutoNext ? '' : 'off'}`} data-option="auto_next">
          <i className="far fa-check-square tb-on" />
          <i className="far fa-square tb-off" />
          <span className="tb-name"> Auto Next</span>
        </div>
      </div>
      {/* )} */}
      <div className="pc-item pc-toggle pc-light" onClick={onToggleLight}>
        <div id="turn-off-light" className={`toggle-basic ${stateLight}`}>
          <span className="tb-name">
            <i className="fas fa-lightbulb" />
            {' '}
            Light
          </span>
        </div>
      </div>
      <div className="pc-item pc-fav" id="watch-list-content" />
      <div onClick={() => onChangeEpisode(+episodeState - 1, ss)} className="pc-item pc-control block-prev">
        <a className="btn btn-sm btn-prev">
          <i className="fas fa-backward" />
          {' '}
          Prev
        </a>
      </div>
      <div onClick={() => onChangeEpisode(+episodeState + 1, ss)} className="pc-item pc-control block-next">
        <a className="btn btn-sm btn-next">
          Next
          {' '}
          <i className="fas fa-forward" />
        </a>
      </div>
      <div id="extend-player" onClick={onClickExpand} className="pc-item pc-zoomtv">
        <div className="toggle-basic">
          <i className="fas fa-expand tb-on" />
          <i className="fas fa-compress tb-off" />
          <span className="tb-name">{expand ? ' Collapse' : ' Expand'}</span>
        </div>
      </div>
      <div className="clearfix" />
    </div>
  );
};
export default BlockControl;
function setAutoNext(arg0: boolean) {
  throw new Error('Function not implemented.');
}
