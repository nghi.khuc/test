/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-children-prop */
import BreadCrumb from 'components/BreadCrumb';
import Content from 'components/Content';
import Footer from 'components/Footer';
import Header from 'components/Header';
import ModalLogin from 'components/Header/ModalLogin';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchApiMenus } from '~/stores/slices/MenuSlices';

const MainLayout = (props: any) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const onClickOverLay = (event) => {
    event.currentTarget.className = '';
  };

  const listMenus = useSelector((state: any) => state.menuSlices.listMenus);

  useEffect(
    () => {
      dispatch(fetchApiMenus());
    }, [router.pathname],
  );

  const { children, isShowAside = true, isShowQuickFilter = true } = props;
  return (
    <React.Fragment>
      <div id="wrapper">
        <Header {...props} subMenus={listMenus} />
        <div className="clearfix" />
        <BreadCrumb />
        <div className="clearfix" />
        <Content {...props} subMenus={listMenus} children={children} isShowAside={isShowAside} isShowQuickFilter={isShowQuickFilter} />
        {
          !!props?.config?.contentTopFooter
          && (
            <div className="container">
              <div className="mba-block" dangerouslySetInnerHTML={{ __html: props.config.contentTopFooter }} />
            </div>
          )
        }
        <Footer {...props} subMenus={listMenus} />
      </div>
      <ModalLogin />
      <div id="mask-overlay" onClick={onClickOverLay} />
    </React.Fragment>
  );
};
export default MainLayout;
