/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import LazyLoad from 'react-lazyload';
import { postLogout } from '~/services/client';
import { buildUrl } from '~/utils/common';

const WatchList = ({ user, casts }: any) => (

  <div className="profile-container">
    <div className="prc_-menu">
      <div className="prc__-icon"><img src={user?.avatar || '/images/user-icon.png'} alt="avatar" /></div>
      {
        !!user && !!casts && (
          <div className="meta mb-3">
            <div className="col1">
              <div className="item">
                <div className="item-title">
                  <span style={{ color: '#666' }}>{'Name: '}</span>
                  {`${user.name}`}
                </div>
                <div className="clearfix" />
              </div>

              {
                !!user.know_for && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Know for: '}</span>
                      {`${user.know_for}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }

              {
                !!user.gender && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Gender: '}</span>
                      {`${user.gender}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }

              {
                !!user.birthday && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Birthday: '}</span>
                      {`${user.birthday}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }

              {
                !!user.deathday && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Deathday: '}</span>
                      {`${user.deathday}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }

              {
                !!user.place && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Place: '}</span>
                      {`${user.place}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }
              {
                !!user.biography && (
                  <div className="item">
                    <div className="item-title">
                      <span style={{ color: '#666' }}>{'Biography: '}</span>
                      {`${user.biography}`}
                    </div>
                    <div className="clearfix" />
                  </div>
                )
              }
            </div>
          </div>
        )
      }
      <div className="prc__-list">
        {
          !casts && (
            <div className="item">
              <a href="/user/profile" className="btn">Edit Profile</a>
            </div>
          )
        }

        <div className="item">
          <a href="/user/watch-list" className="btn active">Watch List</a>
        </div>
        {
          !casts && (
            <div className="item">
              <a href="/user/mal" className="btn">MAL Import</a>
            </div>
          )
        }
        {
          !casts && (
            <div
              className="item"
              onClick={async () => {
                await postLogout();
                setTimeout(() => {
                  window.location.href = '/home';
                }, 500);
              }}
            >
              <a href="#" className="btn">Logout</a>
            </div>
          )
        }
        <div className="clearfix" />
      </div>
    </div>
    <div className="prc_-main">
      <section className="block_area block_area-profile">
        <div className="block_area-header block_area-header-tabs">
          <div className="float-left bah-heading mr-4">
            <h2 className="cat-heading">Watch list</h2>
          </div>
          {/* <div className="float-right bah-tabs">
              <a href="/user/watch-list" className="btn btn-sm active">All</a>
              <a href="/user/watch-list?type=1&sort=" className="btn btn-sm ">Watching</a>
              <a href="/user/watch-list?type=2&sort=" className="btn btn-sm ">On-Hold</a>
              <a href="/user/watch-list?type=3&sort=" className="btn btn-sm ">Plan to watch</a>
              <a href="/user/watch-list?type=4&sort=" className="btn btn-sm ">Dropped</a>
              <a href="/user/watch-list?type=5&sort=" className="btn btn-sm ">Completed</a>
            </div> */}
          <div className="clearfix" />
        </div>
        <div className="block_area-content">
          {/* <div className="description">
              <p className="mb-0">
                - Click to
                <i className="fas fa-folder" />
                {' '}
                icon to move anime to another
                folder.
              </p>
              <p className="mb-0">- Click to Watched/Unwatched to change watch status.</p>
            </div> */}
          {/* <div className="prc__-filter">
              <div className="item">
                <a href="/user/watch-list/random" className="btn btn-sm">Watch a random</a>
              </div>
              <div className="item item-sort">
                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="btn btn-sm">
                  Sort
                  <span>
                    Default
                    <i className="fas fa-angle-down ml-1" />
                  </span>
                </a>
                <div className="dropdown-menu dropdown-menu-native">
                  <a className="dropdown-item active" href="/user/watch-list?type=&sort=">
                    <i className="fas fa-circle" />
                    Default
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=recently-added">
                    <i className="fas fa-circle" />
                    {' '}
                    Recently
                    Added
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=recently-updated">
                    <i className="fas fa-circle" />
                    {' '}
                    Recently
                    Updated
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=score">
                    <i className="fas fa-circle" />
                    {' '}
                    Score
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=a-z">
                    <i className="fas fa-circle" />
                    {' '}
                    Name
                    A-Z
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=released-date">
                    <i className="fas fa-circle" />
                    {' '}
                    Released
                    Date
                  </a>
                  <a className="dropdown-item " href="/user/watch-list?type=&sort=most-watched">
                    <i className="fas fa-circle" />
                    {' '}
                    Most
                    Watched
                  </a>
                </div>
              </div>
              <div className="clearfix" />
            </div> */}
          <div className="film_list-az">
            <div className="anime-block-ul">
              <ul className="ulclear">
                {
                  [...(user?.tvseries || []), ...(user?.movies || [])].map((film: any, index) => (
                    <li key={index}>
                      <div className="film-poster">
                        <LazyLoad placeholder={<img className="film-poster-img ls-is-cached lazyloaded" alt={film.title} src="/images/black-slider.jpeg" />}>
                          <img className="film-poster-img ls-is-cached lazyloaded" alt={film.title} src={film.poster} />
                        </LazyLoad>
                      </div>
                      <div className="film-detail">
                        {/* <div className="btn-remove-fav wl-item" data-type={0} data-id={17278}><i className="fas fa-times" /></div> */}
                        <h3 className="film-name">
                          <a href={`/${buildUrl(film)}`} title={film.title} data-jname={film.title} className="dynamic-name">{film.title}</a>
                        </h3>
                        <div className="fd-infor">
                          <span className="fdi-item mr-2">
                            {`Rating ${film.rating}`}
                          </span>
                          {/* <span className="fdi-item mr-2">
                          <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-folder" />
                            {' '}
                            Plan to watch
                          </a>
                          <div className="dropdown-menu dropdown-menu-model">
                            <div className="dropdown-header">Choose a folder:</div>
                            <a data-type={1} data-id={17278} className="dropdown-item wl-item " href="javascript:;">Watching</a>
                            <a data-type={2} data-id={17278} className="dropdown-item wl-item " href="javascript:;">On-Hold</a>
                            <a data-type={3} data-id={17278} className="dropdown-item wl-item active" href="javascript:;">Plan to watch</a>
                            <a data-type={4} data-id={17278} className="dropdown-item wl-item " href="javascript:;">Dropped</a>
                            <a data-type={5} data-id={17278} className="dropdown-item wl-item " href="javascript:;">Completed</a>
                          </div>
                        </span> */}
                          {/* <span className="fdi-item mr-2">
                              <i className="fas fa-bookmark" />
                              {' '}
                              1
                            </span> */}
                          <span data-id={8933} className="fdi-item watch-status item-watched">
                            <i className="fas fa-eye" />
                            {' '}
                            <span id="watch-status-text">Watched</span>
                          </span>
                        </div>
                      </div>
                      <div className="clearfix" />
                    </li>
                  ))
                }
              </ul>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <div className="anime-pagination" />
    </div>
    <div className="clearfix" />
  </div>
);
export default WatchList;
