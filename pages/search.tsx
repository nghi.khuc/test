/* eslint-disable react/button-has-type */
import React from 'react';
import { GetServerSideProps } from 'next';
import Container from 'components/Filter';

export function SearchPage(props) {
  return (
    <Container {...props} />
  );
}
export default SearchPage;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  return {
    props: {
      query,
    },
  };
};
