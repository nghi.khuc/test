/* eslint-disable no-underscore-dangle */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
  changePassword,
  getUserInfo, ILogin, postLogin, postLogout, postRegister, resetPassword,
} from '~/services/client';
import { toastMessage } from '~/utils/common';

const initialState = {
  user: undefined,
};

export const loginAction = createAsyncThunk(
  'user/login',
  async (payload:ILogin) => {
    try {
      await postLogin(payload);
      const data = await getUserInfo();
      toastMessage({ content: 'Logged in successfully' });
      return data.data;
    } catch (err:any) {
      const errMessage = err?.error?.data?.message;
      toastMessage({ isError: true, content: `Login unsuccessful <br/> ${errMessage || ''}` });
      return undefined;
    }
  },
);

export const resetPasswordAction = createAsyncThunk(
  'user/resetpassword',
  async (email: string) => {
    try {
      await resetPassword(email);
      toastMessage({ content: 'Reset password Success' });
      return true;
    } catch (err:any) {
      toastMessage({ isError: true, content: `${err?.error?.message || 'Reset password fail'}` });
      return undefined;
    }
  },
);

export const registerAction = createAsyncThunk(
  'user/register',
  async (payload:ILogin) => {
    try {
      await postRegister(payload);
      // const data = await getUserInfo();
      toastMessage({ content: 'Register successfully' });
      return undefined;
    } catch (error:any) {
      const errMessage = error?.data?.message;
      toastMessage({ isError: true, content: `Register unsuccessful <br/> ${errMessage || ''}` });
      return undefined;
    }
  },
);

export const getInfoAction = createAsyncThunk(
  'user/info',
  async () => {
    try {
      const data = await getUserInfo();
      return data.data;
    } catch (error) {
      return undefined;
    }
  },
);

export const postChangePassword = createAsyncThunk(
  'user/changePassword',
  async (body:any) => {
    try {
      await changePassword(body);
      return true;
    } catch (error) {
      return undefined;
    }
  },
);

export const logoutAction = createAsyncThunk(
  'user/logout',
  async () => {
    try {
      const data = await postLogout();
      toastMessage({ content: 'Sign out in successfully' });
      return data;
    } catch (error) {
      toastMessage({ isError: true, content: 'Sign out unsuccessful' });
      return undefined;
    }
  },
);

export const userSlices = createSlice({
  name: 'userSlices',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    // builder.addCase(registerAction.fulfilled, (state, action) => {
    //   state.user = action.payload;
    // });
    builder.addCase(loginAction.fulfilled, (state, action) => {
      state.user = action.payload;
    });
    builder.addCase(getInfoAction.fulfilled, (state, action) => {
      state.user = action.payload;
    });
    builder.addCase(logoutAction.fulfilled, (state) => {
      state.user = undefined;
    });
    builder.addCase(postChangePassword.fulfilled, (state) => {
      state.user = undefined;
    });
  },
});

// export const { setPosition, setItemCodeActive } = filmSlices.actions;
// Action creators are generated for each case reducer function

export default userSlices.reducer;
