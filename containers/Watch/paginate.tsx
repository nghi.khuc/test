/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useMemo } from 'react';

const Paginate = ({ response = {}, slug }:any) => {
  const [pageState, setPage] = useState(1);
  const onChangePage = (event) => {
    setPage(event.target.value);
    // window.location.href = `?page=${response?.page}`;
  };
  const onSubmitSearchPage = () => {
    window.location.href = `?page=${pageState}`;
  };

  const buildUrlPagePrevious = useMemo(
    () => ((+response?.page - 1 === 1) ? `/az-list/${slug}` : `/az-list/${slug}?page=${+response?.page - 1}`),
    [response?.page],
  );

  return (
    <div className="anime-pagination mt-2 mb-2">
      <div className="ap_-nav">
        <div className="ap__-btn ap__-btn-prev">
          <a
            href={buildUrlPagePrevious}
            className={`btn btn-sm btn-focus more-padding ${response.page === 1 ? 'disabled' : ''}`}
          >
            <i className="fas fa-angle-left" />
            {' '}
            Previous
          </a>
        </div>
        <div className="ap__-input">
          <div className="btn btn-sm btn-blank">page</div>
          <input
            className="form-control form-control-dark input-page"
            defaultValue={response?.page}
            onChange={onChangePage}
            data-url={`/az-list/${slug}?page=`}
            data-toggle="tooltip"
            data-placement="top"
            title="Enter a number"
          />
          <button onClick={onSubmitSearchPage} type="button" className="btn btn-sm btn-focus btn-go-page">go</button>
          <div className="btn btn-sm btn-blank">
            {`of ${response?.lastPage}`}
          </div>
        </div>
        <div className="ap__-btn ap__-btn-next">
          <a href={`/az-list/${slug}?page=${+response?.page + 1}`} className="btn btn-sm btn-focus more-padding ">
            Next
            {' '}
            <i className="fas fa-angle-right" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Paginate;
