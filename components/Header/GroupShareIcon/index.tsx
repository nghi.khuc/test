/* eslint-disable react/destructuring-assignment */
import React from 'react';

const GroupIconShare = (props: any) => (
  <div className="header-group">
    <div className="socials-group">
      <div className="scg-list">
        {!!props?.config?.contacts?.discord && (
          <div className="item">
            <a
              target="_blank"
              href={props?.config?.contacts?.discord || 'https://twitter.com/9Animevc'}
              className="sc-social-button dc-btn"
              rel="noreferrer"
            >
              <i className="fab fa-discord" />
            </a>
          </div>
        )}
        {!!props?.config?.contacts?.telegram && (
          <div className="item">
            <a
              href={props?.config?.contacts?.telegram || '/#'}
              className="sc-social-button tl-btn"
            >
              <i className="fab fa-telegram-plane" />
            </a>
          </div>
        )}
        {!!props?.config?.contacts?.reddit && (
          <div className="item">
            <a
              href={props?.config?.contacts?.reddit || '/#'}
              className="sc-social-button rd-btn"
            >
              <i
                className="fab fa-reddit-alien"
              />
            </a>
          </div>
        )}
        {!!props?.config?.contacts?.facebook && (
          <div className="item">
            <a
              href={props?.config?.contacts?.facebook || '/#'}
              className="sc-social-button tl-btn"
            >
              <i className="fab fa-facebook" />
            </a>
          </div>
        )}
        {!!props?.config?.contacts?.twitter && (
          <div className="item">
            <a
              href={props?.config?.contacts?.twitter || '/#'}
              className="sc-social-button tl-btn"
            >
              <i className="fab fa-twitter" />
            </a>
          </div>
        )}
        {!!props?.config?.contacts?.email && (
          <div className="item">
            <a
              href={props?.config?.contacts?.email || '/#'}
              className="sc-social-button rd-email"
            >
              <i className="fas fa-envelope" />
            </a>
          </div>
        )}
      </div>
      <div className="clearfix" />
    </div>
  </div>
);

export default GroupIconShare;
