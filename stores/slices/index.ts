import { combineReducers } from '@reduxjs/toolkit';
import filmSlices from './FilmSlice';
import userSlices from './UserSlices';
import menuSlices from './MenuSlices';
import topViewSlices from './TopViewSlices';

export default () => combineReducers({
  filmSlices,
  userSlices,
  menuSlices,
  topViewSlices,
});
