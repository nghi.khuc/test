import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import configSystem from '~/utils/config';
import {
  exception, HTTP_STATUS, setCookieAdmin, verifyCookieAdmin, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const isAccept = verifyCookieAdmin(req);

    if (req.method === 'GET' && isAccept) return res.status(200).send('oke');

    if (req.method === 'POST') {
      const { username, password } = req.body;
      if (!(configSystem.ACCOUNT_ADMIN.username === username
        && configSystem.ACCOUNT_ADMIN.password === password)) return res.status(401).send('wrong password');
      setCookieAdmin({ req, res });

      return res.status(200).send('oke');
    }
    return res.status(401).send('no oke');
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
