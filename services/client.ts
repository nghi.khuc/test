/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import request from 'utils/request';
import { URLs } from '~/utils/contains';
import { getIP } from '~/utils/common';

const mappingType = {
  'tv-series': URLs.tvSeries,
  movie: URLs.movies,
  movies: URLs.movies,
};

const typeMovie = ['movie', 'movies'];

interface IPramsListFilm {
  page?: number,
  length?: number,
  orderby?: string,
  view?: boolean,
}

interface IParamsStreamUrl{
  id: number|string,
  type: 'movie'|'movies'|'tv-series'
}

interface ISlider {
  type: 'movie' | 'tv-series' | 'movies'
}

export const getListGenres = async () => {
  const url = `/api${URLs.genres}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListFilmSimilar = async (params, type, id) => {
  const slug = mappingType[type];
  const url = `/api${slug}/${id}/similar`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getMenus = async () => {
  const url = '/api/menus';
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListTypes = async () => {
  const url = `/api${URLs.types}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getSliders = async () => {
  const url = '/api/sliders';
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListCountry = async () => {
  const url = `/api${URLs.countries}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListFilmBySearch = async (search) => {
  const url = `/api${URLs.search}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params: { q: search },
      },
    },
  );
  return response;
};

export const getListAZFilms = async (params) => {
  const url = `/api${URLs.movies}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getFilmById = async ({ id, type, toolTip }) => {
  if (!type) return {};
  const slug = mappingType[type];
  const url = `/api${slug}/${id}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params: { toolTip },
      },
    },
  );
  return response;
};

export const getListFilmsTVSeries = async (params:Partial<IPramsListFilm>) => {
  const url = `/api${URLs.tvSeries}`;

  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getListFilmsMoviesSeries = async (params:IPramsListFilm) => {
  const url = `/api${URLs.movies}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getUrlStream = async ({ id, type }:IParamsStreamUrl) => {
  const slug = mappingType[type];
  let url;
  if (typeMovie.includes(type)) {
    url = `/api${slug}/${id}/stream_urls`;
  } else {
    url = `/api/episodes/${id}/stream_urls`;
  }
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  const streamsData = response.data;
  const streamsUrl = streamsData.streamurls;
  for (const i in streamsUrl) {
    const files = await getURLIOCodes(streamsUrl[i].url);
    streamsUrl[i].files = files;
  }

  return streamsData;
};

export const getSeasonsTV = async ({ id, type }) => {
  const slug = mappingType[type];
  const url = `/api${slug}/${id}/seasons`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  const seasons = response.data;
  for (const i in seasons) {
    const eps = await getEpisodes(seasons[i]._id);
    seasons[i].episodes = eps;
  }

  return seasons;
};

export const getEpisodes = async (id) => {
  const url = `/api${URLs.seasons}/${id}/episodes`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response.data;
};

export const getURLIOCodes = async (url_stream) => {
  const ip_address = await getIP();
  const url = '/api/url-io';
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params: {
          url: url_stream,
          ip: ip_address,
        },
      },
    },
  );
  return response;
};

export interface ILogin{
    email: string
    password: string
    ip_address?: string
    remember?: boolean
    username?: string
}

export const postLogin = async (body: ILogin) => {
  const ip_address = await getIP();
  const url = '/api/authen/login';
  const response = await request(
    {
      url,
      options: {
        body: { ...body, ip_address },
        method: 'POST',
      },
    },
  );
  return response;
};

export const resetPassword = async (email: string) => {
  const url = '/api/authen/reset-password';
  const response = await request(
    {
      url,
      options: {
        body: { email },
        method: 'POST',
      },
    },
  );
  return response;
};

export const postRegister = async (body: ILogin) => {
  const ip_address = await getIP();
  const url = '/api/authen/register';
  const response = await request(
    {
      url,
      options: {
        body: { ...body, ip_address },
        method: 'POST',
      },
    },
  );
  return response;
};

export const postLogout = () => request(
  {
    url: '/api/authen/logout',
    options: {},
  },
);

export const getUserInfo = () => request(
  {
    url: '/api/authen/user',
    options: {
      method: 'GET',
    },
  },
);

export const changePassword = (body) => request(
  {
    url: '/api/authen/change-password',
    options: {
      method: 'POST',
      body,
    },
  },
);

export const getMoviesById = async (id, params) => request(
  {
    url: `/api${URLs.movies}/${id}`,
    options: {
      method: 'GET',
      params,
    },
  },
);

export const getTVSeriesById = async (id, params) => request(
  {
    url: `/api${URLs.tvSeries}/${id}`,
    options: {
      method: 'GET',
      params,
    },
  },
);

interface ITrackError {
  movieID?: string
  movieType?: string,
  server: number | any,
  episodeId: string
}
export const postErrorFilm = (body:ITrackError) => request(
  {
    url: '/api/track-error',
    options: {
      body,
      method: 'POST',
    },
  },
);

export const getFilterListFilm = async (params:any) => {
  const response = await request(
    {
      url: '/api/filter',
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getListFilmTopTv = async (params:any) => {
  const response = await request(
    {
      url: `/api${URLs.tvSeries}/topview`,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getListFilmTopMv = async (params:any) => {
  const response = await request(
    {
      url: `/api${URLs.movies}/topview`,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getAdminConfig = async () => {
  const response = await request(
    {
      url: '/api/admin/config',
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getAdminLogin = async () => {
  const response = await request(
    {
      url: '/api/admin/login',
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const postAdminLogin = async (params:any) => {
  const response = await request(
    {
      url: '/api/admin/login',
      options: {
        method: 'POST',
        body: params,
      },
    },
  );
  return response;
};

export const postAdminConfig = async (params:any) => {
  const response = await request(
    {
      url: '/api/admin/config',
      options: {
        method: 'POST',
        body: params,
      },
    },
  );
  return response;
};

export const postConfigContent = async (params:any, type:any) => {
  const response = await request(
    {
      url: `/api/admin/config/content?type=${type}`,
      options: {
        method: 'POST',
        body: params,
      },
    },
  );
  return response;
};

export const getConfigContent = async (params:any) => {
  const response = await request(
    {
      url: '/api/admin/config/content',
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const postConfigAds = async (params:any, type:any) => {
  const response = await request(
    {
      url: `/api/admin/config/ads?type=${type}`,
      options: {
        method: 'POST',
        body: params,
      },
    },
  );
  return response;
};

export const getConfigAds = async (params:any) => {
  const response = await request(
    {
      url: '/api/admin/config/ads',
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getUserProfileWatchList = async (id:any) => {
  const response = await request(
    {
      url: `/api/person/${id}`,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getUserProfileWatchListFilms = async (id:any) => {
  const response = await request(
    {
      url: `/api/person/${id}/films`,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getUserProfileFavoriteMovie = async (page?) => {
  const response = await request(
    {
      url: `/api/authen/favorite?type=movies&page=${page || 1}`,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getUserProfileFavoriteTV = async (page?) => {
  const response = await request(
    {
      url: `/api/authen/favorite?type=tv-series&page=${page || 1}`,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const postFavoriteFilm = async (params) => {
  const response = await request(
    {
      url: '/api/authen/favorite',
      options: {
        method: 'POST',
        params,
      },
    },
  );
  return response;
};

export const deleteFavoriteFilm = async (params) => {
  const response = await request(
    {
      url: '/api/authen/favorite',
      options: {
        method: 'DELETE',
        params,
      },
    },
  );
  return response;
};

export const getCredits = async ({ type, id }) => {
  const response = await request(
    {
      url: `/api/${type}/${id}`,
      options: {
        method: 'GET',
        params: {
          limit: 20,
          type: 'person',
        },
      },
    },
  );
  return response;
};

export const getSchedules = async () => {
  const response = await request(
    {
      url: `/api${URLs.schedule}`,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getTag = async (params) => {
  const response = await request(
    {
      url: `/api${URLs.tag}`,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getHagtag = async (params) => {
  const response = await request(
    {
      url: `/api${URLs.hagtag}`,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const postConfigDoc = async (params:any, type:any) => {
  const response = await request(
    {
      url: `/api/admin/config/document?type=${type}`,
      options: {
        method: 'POST',
        body: params,
      },
    },
  );
  return response;
};

export const getConfigDoc = async (params:any) => {
  const response = await request(
    {
      url: '/api/admin/config/document',
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getRating = async (body) => {
  const response = await request(
    {
      url: '/api/authen/rating',
      options: {
        method: 'GET',
        body,
      },
    },
  );
  return response;
};

export const postRating = async (body) => {
  const response = await request(
    {
      url: '/api/authen/rating',
      options: {
        method: 'POST',
        body,
      },
    },
  );
  return response;
};
