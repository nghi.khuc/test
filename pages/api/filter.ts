import qs from 'qs';
import { getListFilmsMoviesSeries, getListFilmsTVSeries, trackLog } from '~/services/server';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import redis, { redisGetAsync, timeCachingSystem } from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';

const handler = async (req, res) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const key = `${KEYS_REDIS.FILTER}:${qs.stringify(req.query)}`;
    const client = redis();
    const value = await redisGetAsync(key);
    if (value) return res.RESP(JSON.parse(value));
    const [responseListFilmTVs, responseListFilmMovies] = await Promise.all([
      getListFilmsTVSeries(req.query),
      getListFilmsMoviesSeries(req.query),
    ]);

    const pagination = {
      total: Math.max(...[responseListFilmTVs.total || 0, responseListFilmMovies.total || 0]),
      perPage: Math.max(...[responseListFilmTVs.perPage || 0, responseListFilmMovies.perPage || 0]),
      page: Math.max(...[responseListFilmTVs.page || 0, responseListFilmMovies.page || 0]),
      lastPage: Math.max(...[responseListFilmTVs.lastPage || 0, responseListFilmMovies.lastPage || 0]),
    };

    const result = {
      data: {
        other: pagination,
        data: [
          ...(responseListFilmTVs.data || []),
          ...(responseListFilmMovies.data || []),
        ],
      },
    };
    client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
    return res.RESP(result);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
