import qs from 'qs';
import axios from 'axios';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import VaultURLs from '~/utils/contains';
import redis, { redisGetAsync, timeCachingSystem } from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';
import { trackLog } from '~/services/server';

export const normalizeFilms = (items) => items?.map(({
  _id, title, type, original_title, slug, content, rating, quality, release_date,
  release_year, views, total_episodes, description, duration, poster, backdrop, episode_new,
}) => ({
  _id,
  title,
  type,
  original_title,
  slug,
  content,
  rating,
  quality,
  release_date,
  release_year,
  views,
  total_episodes,
  description,
  duration,
  poster,
  backdrop,
  episode_new,
  has_favorite: false,
}));

export const getTVFilms = async (query) => {
  const client = redis();
  const key = `${KEYS_REDIS.TVS}:${query}`;
  const value = await redisGetAsync(key);
  if (value) return JSON.parse(value);

  const url = `${VaultURLs.API_URL}/api/tv-series?${query}`;

  const resp = await axios({
    url,
    method: 'GET',
  });

  const result = {
    data: {
      other: resp.data,
      data: normalizeFilms(resp.data.data),
    },
  };

  client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_CACHING));
  return result;
};

const handler = async (req, res) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/tv-series`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const { orderby, ...other } = req.query;
    // const query = orderby === 'views' ? { ...other, view: true } : req.query;
    const { genre: genreOther, type: typeOther, country: countryOther } = other;

    const genre = genreOther && genreOther.split(',');
    const type = typeOther && typeOther.split(',');
    const country = countryOther && countryOther.split(',');
    const query = orderby === 'views' ? {
      ...other, view: true, genre, type, country,
    } : {
      ...req.query, genre, type, country,
    };

    const result = await getTVFilms(qs.stringify(query));

    return res.RESP(result);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
