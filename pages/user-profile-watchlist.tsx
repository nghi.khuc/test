/* eslint-disable react/button-has-type */
import React from 'react';
import Container from 'containers/User/WatchList';
import { useRouter } from 'next/router';
import { getUserProfileWatchList, getUserProfileWatchListFilms } from '~/services/client';

export function Page() {
  const router = useRouter();
  const [user, setUser] = React.useState({
    avatar: '/images/user-icon.png',
    movies: [],
    tvseries: [],
  });

  const getInfo = async () => {
    const { id } = router.query;
    const [info, films] = await Promise.all([getUserProfileWatchList(id), getUserProfileWatchListFilms(id)]);
    setUser({
      ...info.data,
      movies: films.data?.movies?.map((film) => ({ ...film, type: 'movies' })) || [],
      tvseries: films.data?.tvseries?.map((film) => ({ ...film, type: 'tv-series' })) || [],
    });
  };

  React.useEffect(() => {
    getInfo();
  }, []);
  return (
    <Container user={user} />
  );
}

Page.PAGE_NOT_USE_ASIDE = true;

export default Page;
