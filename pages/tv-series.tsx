/* eslint-disable react/button-has-type */
import React, { useEffect, useState } from 'react';
import Container from 'containers/TVSeries';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import { getSliders, getListFilmsTVSeries } from '~/services/client';
import { KEYS_REDIS } from '~/utils/config';
import { redisGetAsync } from '~/utils/redis';
import { parseStringToObject } from '~/utils/common';
import { TYPE_ADS_PAGE } from '~/utils/contains';

interface IArray {

}

const initSlider = [
  {
    backdrop: '/images/black-slider.jpeg',
  },
];

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};

export function TVSeriesComponent(props) {
  const router = useRouter();
  const page: any = router.query?.page || 1;
  const [listFilmsTVs, setListFilmsTVs] = useState(() => Array(10).fill(initItem));
  const [paginate, setPaginate] = useState<any>({});
  const [sliders, setSliders] = useState<IArray[]>(initSlider);

  const fetchSlider = async () => {
    const result = await getSliders();
    const newSliders = result?.data.tv || [];
    setSliders(newSliders);
  };

  const fetchTV = async () => {
    setListFilmsTVs(Array(10).fill(initItem));
    const responseFilmsTV = await getListFilmsTVSeries({
      page, length: 20, orderby: 'update_at',
    });
    setPaginate(responseFilmsTV);
    setListFilmsTVs(responseFilmsTV.data || []);
  };
  useEffect(() => {
    if (router) {
      fetchSlider();
      fetchTV();
    }
  }, [router]);

  return (
    <Container {...props} paginate={paginate} listFilmsTVs={listFilmsTVs} sliders={sliders} />
  );
}
export default TVSeriesComponent;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const keyContentSLider = KEYS_REDIS.SLIDER_CONTENT;
  const keyAdsSlider = TYPE_ADS_PAGE.under_slider_tv.key;
  // TYPE_ADS_PAGE.home_under_search.key;
  const keyMovieIndex = TYPE_ADS_PAGE.home_under_movie_recently_updated.key;
  const [contentSlider, adsSlider, contentUnderMovie] = await Promise.all([
    redisGetAsync(keyContentSLider),
    redisGetAsync(keyAdsSlider),
    redisGetAsync(keyMovieIndex),
  ]);
  const contentSliderParse = parseStringToObject(contentSlider);
  const adsSliderParse = parseStringToObject(adsSlider);
  const contentUnderMoviePage = parseStringToObject(contentUnderMovie);
  return {
    props: {
      query,
      contentUnderSlider: contentSliderParse?.content || '',
      adsSliderParse: adsSliderParse?.content || '',
      contentUnderMovie: contentUnderMoviePage?.content || '',
    },
  };
};
