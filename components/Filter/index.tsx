import React, { useEffect, useState } from 'react';
import uniq from 'lodash/uniq';
import ListFilms from 'components/ListFilms';
import QuickFilter from 'components/QuickFilter';
import { useRouter } from 'next/router';
import querystring from 'querystring';
import {
  getListFilmsMoviesSeries, getListFilmBySearch, getListFilmsTVSeries, getFilterListFilm,
} from '~/services/client';
import Paginate from '~/containers/AZList/paginate';

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};
const EXCEPTION_URL = /(.*.recently-updated$|.*.recently-added$)/g;

const FilterContainer = ({ type: TypeFilm, ...props }: any) => {
  let typeFilm = TypeFilm || 'movies';
  const router = useRouter();
  const [listFilms, setListFilm] = useState(() => Array(10).fill(initItem));
  const [inforList, setInforList] = useState<any>({});
  const query = { ...router.query, ...props?.query };
  if (query?.option) {
    typeFilm = query?.option;
  }
  const isAllowPushUrl = EXCEPTION_URL.test(router.asPath);
  const fetchListFilm = async (paramsQuery) => {
    if (!paramsQuery.option && !isAllowPushUrl) {
      window.history.replaceState(window.history.state, 'option-default-movie', `/filter?${querystring.stringify({ ...paramsQuery, option: typeFilm })}`);
    }

    if (router.pathname === '/search' && paramsQuery?.keyword) {
      const responseFilms: any = await getListFilmBySearch(paramsQuery?.keyword || '');
      setListFilm(responseFilms.data);
      return;
    }

    const mapTypes = `${paramsQuery?.types || ''},${paramsQuery.status || ''}`;
    const typesFilter = uniq(mapTypes.split(',')).filter((item) => !!item)?.join(',') || undefined;
    const newQuery = {
      genre: paramsQuery?.genres, type: typesFilter, orderby: paramsQuery?.sort, ...paramsQuery,
    };

    if (paramsQuery.is_commingsoon) {
      newQuery.is_commingsoon = true;
    }

    delete newQuery.genres;
    delete newQuery.types;
    delete newQuery.sort;
    delete newQuery.status;
    delete newQuery.option;

    if (!typeFilm || typeFilm === 'all') {
      const { data, ...pagination } = await getFilterListFilm(newQuery);
      setListFilm(data);
      setInforList(pagination);
      return;
    }
    const response = typeFilm === 'tv-series' ? await getListFilmsTVSeries(newQuery) : await getListFilmsMoviesSeries(newQuery);
    setListFilm(response.data);
    setInforList(response);
  };

  useEffect(() => {
    fetchListFilm(query);
  }, [router]);

  return (
    <React.Fragment>
      <QuickFilter query={router.query} {...props} isBlockSideBar={false} />
      <ListFilms listFilms={listFilms} />
      <Paginate response={inforList} />
      <div className="clearfix" />
    </React.Fragment>
  );
};
export default FilterContainer;
