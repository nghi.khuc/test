import { useState, useEffect } from 'react';

import { css } from 'styled-components';

const getScreenWidth = () => {
  if (typeof window === 'undefined') return undefined;
  return window.innerWidth;
};

const getIsMobile = () => typeof window !== 'undefined' && window.innerWidth < 768;

const getIsLargeMobile = () => (
  typeof window !== 'undefined'
  && window.innerWidth >= 768
  && window.innerWidth < 1112
);

export const useScreenSize = () => {
  // const [screenWidth, setScreenWidth] = useState(
  //   getScreenWidth()
  // );
  const [isMobile, setIsMobile] = useState(getIsMobile());
  const [isLargeMobile, setIsLargeMobile] = useState(getIsLargeMobile());

  // useEffect(() => {
  //   const handleResize = () => {
  //     // setScreenWidth(getScreenWidth());
  //     setIsMobile(getIsMobile());
  //     setIsLargeMobile(getIsLargeMobile());
  //   };

  //   window.addEventListener('resize', handleResize);

  //   return () => {
  //     window.removeEventListener('resize', handleResize);
  //   };
  // }, []);

  return {
    isMobile,
    isLargeMobile,
    // screenWidth,
  };
};

export const useGetIsMobile = (isMobileSsr, isMobileCsr) => {
  const [isMobile, setIsMobile] = useState(isMobileSsr);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setIsMobile(isMobileCsr);
    } else {
      setIsMobile(isMobileSsr);
    }
  }, [isMobileCsr]);

  return isMobile;
};

export const checkMobile = (userAgent) => {
  if (/Tablet|iPad/i.test(userAgent)) {
    return false;
  }
  return (
    /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(userAgent)
    || /\b(Android|Windows Phone|iPod)\b/i.test(userAgent)
  );
};

export const breakpoints = {
  sm: 768,
};

export const deviceLayout = {
  mb: (...args) => css`
    @media (max-width: ${breakpoints.sm - 1}px) {
      ${css(...args)};
    }
  `,
  pc: (...args) => css`
    @media (min-width: ${breakpoints.sm}px) {
      ${css(...args)};
    }
  `,
};
