/* eslint-disable react/button-has-type */
import React, { useEffect, useState } from 'react';
import Container from 'containers/Movies';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import { getSliders, getListFilmsMoviesSeries } from '~/services/client';
import { KEYS_REDIS } from '~/utils/config';
import { redisGetAsync } from '~/utils/redis';
import { parseStringToObject } from '~/utils/common';
import { TYPE_ADS_PAGE } from '~/utils/contains';

interface IArray {

}

const initSlider = [
  {
    backdrop: '/images/black-slider.jpeg',
  },
];

const initItem = {
  poster: '/images/black-slider.jpeg',
  quality: 'HD',
};

export function Home(props) {
  const router = useRouter();
  const page: any = router.query?.page || 1;
  const [listFilmMovies, setListFilmMovies] = useState(() => Array(10).fill(initItem));
  const [sliders, setSliders] = useState<IArray[]>(initSlider);
  const [paginate, setPaginate] = useState<any>({});

  const fetchSlider = async () => {
    const result = await getSliders();
    const newSliders = result?.data.movie || [];
    setSliders(newSliders);
  };

  const fetchMove = async () => {
    // set lazy load
    setListFilmMovies(Array(10).fill(initItem));
    // call api response films
    const responseFilmsMovies = await getListFilmsMoviesSeries({
      page, length: 20, orderby: 'update_at',
    });

    setListFilmMovies(responseFilmsMovies.data || []);
    setPaginate(responseFilmsMovies);
  };

  useEffect(() => {
    if (router) {
      fetchSlider();
      fetchMove();
    }
  }, [router]);

  return (
    <Container {...props} paginate={paginate} listFilmMovies={listFilmMovies} sliders={sliders} />
  );
}
export default Home;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const keyContentSLider = KEYS_REDIS.SLIDER_CONTENT;

  const keyAdsSlider = TYPE_ADS_PAGE.under_slider_movie.key;
  // TYPE_ADS_PAGE.home_under_search.key;
  const keyMovieIndex = TYPE_ADS_PAGE.home_under_movie_recently_updated.key;
  const [contentSlider, adsSlider, contentUnderMovie] = await Promise.all([
    redisGetAsync(keyContentSLider),
    redisGetAsync(keyAdsSlider),
    redisGetAsync(keyMovieIndex),
  ]);
  const contentSliderParse = parseStringToObject(contentSlider);
  const adsSliderParse = parseStringToObject(adsSlider);
  const contentUnderMoviePage = parseStringToObject(contentUnderMovie);
  return {
    props: {
      query,
      contentUnderSlider: contentSliderParse?.content || '',
      adsSliderParse: adsSliderParse?.content || '',
      contentUnderMovie: contentUnderMoviePage?.content || '',
    },
  };
};
