/* eslint-disable no-underscore-dangle */
import { createSlice, createAsyncThunk, AsyncThunk } from '@reduxjs/toolkit';
import * as SearchApi from 'services/client';
import isEmpty from 'lodash/isEmpty';
import { ParamsById, ItemFilm } from './types';

const initialState = {
  listFilms: [],
  listFilmsTopView: [],
  loading: false,
  loadingItem: false,
  qtipItem: {},
  position: {},
  itemCodeActive: '',
};

const noramlizeItemFilm = (data) => {
  if (isEmpty(data)) return [];
  if (typeof data === 'object') {
    if (data[0]) {
      return data.map((item:ItemFilm) => ({
        ...item,
        id: item.id,
        code: item._id,
        title: item.title,
        type: item.type,
        original_title: item.original_title,
        slug: item.slug,
        is_comingsoon: item.is_comingsoon,
        content: item.content,
        rating: item.rating,
        release_date: item.release_date,
        release_year: item.release_year,
        duration: item.duration,
      }));
    }
    return {
      [data._id]: {
        ...data,
        id: data.id,
        code: data._id,
        title: data.title,
        type: data.type,
        original_title: data.original_title,
        slug: data.slug,
        is_comingsoon: data.is_comingsoon,
        content: data.content,
        rating: data.rating,
        release_date: data.release_date,
        release_year: data.release_year,
        duration: data.duration,
      },
    };
  }
  return [];
};
export const fetchFilmBySearch = createAsyncThunk(
  'anime/search',
  async (search:any) => {
    const response = await SearchApi.getListFilmBySearch(search);
    return response.data;
  },
);

export const fetchFilmById = createAsyncThunk(
  'anime/get_film_by_id',
  async ({ id, type, toolTip }: ParamsById) => {
    const response = await SearchApi.getFilmById({ id, type, toolTip });
    return response.data;
  },
);

export const filmSlices = createSlice({
  name: 'filmSlices',
  initialState,
  reducers: {
    setPosition(state, action) {
      state.position = action.payload;
    },
    setItemCodeActive(state, action) {
      state.itemCodeActive = action.payload;
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchFilmBySearch.pending, (state:any) => {
      state.loading = true;
      state.listFilms = [];
    });
    builder.addCase(fetchFilmBySearch.fulfilled, (state, action) => {
      state.listFilms = noramlizeItemFilm(action.payload);
      state.loading = false;
    });
    builder.addCase(fetchFilmBySearch.rejected, (state) => {
      state.loading = false;
      state.listFilms = [];
    });

    builder.addCase(fetchFilmById.pending, (state) => {
      state.loadingItem = true;
    });
    builder.addCase(fetchFilmById.fulfilled, (state, action) => {
      const newData = noramlizeItemFilm(action.payload);
      state.loadingItem = false;
      state.qtipItem = { ...state.qtipItem, ...newData };
    });
    builder.addCase(fetchFilmById.rejected, (state) => {
      state.loadingItem = false;
      state.qtipItem = {};
    });
  },
});

export const { setPosition, setItemCodeActive } = filmSlices.actions;
// Action creators are generated for each case reducer function

export default filmSlices.reducer;
