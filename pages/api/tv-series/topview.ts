/* eslint-disable max-len */
import { trackLog } from '~/services/server';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import redis, {
  getCurrentDate, redisDelAsync, redisGetValAsync, redisSetAsync, redisZRevRangeAsync,
} from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';
import { getTVFilms } from '.';
import { getInfoTVFilm } from './[id]';

const getRankWithType = async (ids, objMapView, typeParam) => {
  const type = +typeParam;
  if (![1, 2, 3].includes(+type)) Error('type no match');

  const currentDate = getCurrentDate();
  const currentDay = currentDate.format('DD');
  const currentWeek = currentDate.isoWeek();

  let asyncInfos = [];
  const keysMap:any = [];

  if (type === 1) {
    asyncInfos = ids.reduce((result, key) => {
      const [id, time] = key.split('-');
      if (!time) return result;
      const date = getCurrentDate(time).format('DD');
      if (date === currentDay) {
        keysMap.push(key);
        return [...result, getInfoTVFilm(id)];
      }
      return result;
    }, []);
  }

  if (type === 2) {
    asyncInfos = ids.reduce((result, key) => {
      const [id, time] = key.split('-');
      const date = getCurrentDate(time).isoWeek();
      if (date === currentWeek) {
        keysMap.push(key);
        return [...result, getInfoTVFilm(id)];
      }
      return result;
    }, []);
  }

  if (type === 3) {
    // month type 3
    asyncInfos = ids.reduce((result, key) => {
      const [id] = key.split('-');
      keysMap.push(key);
      return [...result, getInfoTVFilm(id)];
    }, []);
  }

  const resp = await Promise.all(asyncInfos);
  const result = resp.map((data:any) => {
    const { _id } = data.data.data;
    const totalView = keysMap.reduce((total, key) => {
      const [id] = key.split('-');
      if (_id === id) return total + objMapView[key];
      return total;
    }, 0);

    return ({ ...data.data.data, views: +totalView });
  });

  const final = result
    .filter((item) => !!item._id)
    .reduce((data, item) => ({ ...data, [item._id]: item }), {});

  return Object.values(final);
};

// Delete All key Redis of Topview
// await redisDelAsync(`${KEYS_REDIS.TV_RANK_VAL}-3`);
// await redisDelAsync(`${KEYS_REDIS.TV_RANK_VAL}-2`);
// await redisDelAsync(`${KEYS_REDIS.TV_RANK_VAL}-1`);
// await redisDelAsync(KEYS_REDIS.TV_RANK);
// await redisDelAsync(KEYS_REDIS.TV_RANK_EXPIRE);

const handler = async (req, res) => {
  try {
    const { type, page: pageParam = 1 } = req.query;
    const page = pageParam || 1;
    const limit = 10;

    const configRequest = req.getStructureDynamicRouterServer('');
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    // today type 0
    if (!type || `${type}` === '0') {
      const infosTop = await getTVFilms(`page=${page || 1}&length=${limit}&view=true`);
      return res.RESP(infosTop);
    }
    const key = `${KEYS_REDIS.TV_RANK_VAL}-${type}`;
    const val = await redisGetValAsync(key);
    if (val) {
      const result = {
        data: {
          data: val.slice((page - 1) * limit, page * limit),
          total: val.length,
          perPage: limit,
          page,
          lastPage: Math.ceil(val.length / 10),
        },
      };
      return res.RESP(result);
    }
    const ranks = await redisZRevRangeAsync(KEYS_REDIS.TV_RANK, 0, 100, 'withscores');

    const ids = (ranks || []).filter((_, index) => !(index % 2));
    const views = (ranks || []).filter((_, index) => (index % 2));
    const objMapView = ids.reduce((result, id, index) => ({ ...result, [id]: +views[index] }), {});
    const info = await getRankWithType(ids, objMapView, type);

    if (info.length) {
      redisSetAsync(key, info, configSystem.EXPIRE_TIME_TOP_VIEW);
    }

    // pagination

    const result = {
      data: {
        data: info.slice((page - 1) * limit, page * limit),
        total: info.length,
        perPage: limit,
        page,
        lastPage: Math.ceil(info.length / 10),
      },
    };

    return res.RESP(result);
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
