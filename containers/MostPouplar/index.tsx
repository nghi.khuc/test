/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable react/button-has-type */
import React from 'react';
import ListFilms from 'components/ListFilms';

interface IProps {
  listFilms: [],
}

const MovieComponent = ({ listFilms }: IProps) => (
  <React.Fragment>
    <ListFilms listFilms={listFilms} title="Most Popular" />
    <div className="clearfix" />
  </React.Fragment>
);
export default MovieComponent;
