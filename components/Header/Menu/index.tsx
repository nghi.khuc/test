/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { useScreenSize, useGetIsMobile } from 'utils/useScreenSize';
import MenuMobile from './Mobile';
import MenuPC from './PC';

const Menu = (props: any) => {
  const { isMobile: isMobileCsr } = useScreenSize();
  const isMobile = useGetIsMobile(props.isMobileSsr, isMobileCsr);

  return (
    isMobile ? (
      <MenuMobile {...props} />
    ) : (
      <MenuPC {...props} />
    )

  );
};

export default Menu;
