import React, { useState, useCallback } from 'react';
import { menuConfig } from '~/components/config/menu';

const MENU_EXCEPTION = ['tv-series', 'movies'];
const Menu = ({ subMenus }: any) => {
  const [menuActive, setMenuActive] = useState('');
  const onHover = useCallback((event) => {
    setMenuActive(event.target.title);
  }, [menuActive]);

  return (
    <div id="site_menu">
      <div className="site_menu-icon"><i className="fa fa-bars" /></div>
      <div id="header_menu">
        <ul className="nav header_menu-list">
          {menuConfig.map((menu) => {
            const activeSubMenu = menuActive === menu.name ? 'active' : '';
            const subMenusList = (subMenus[menu.key] || menu.subMenus) || [];
            const addSub = MENU_EXCEPTION.includes(menu.key) ? subMenus?.genres : [];
            const newSub = subMenusList.concat(addSub);
            const subLength = newSub?.length || 0;
            const widthSizeXL = subLength > 40 && 'size-xl';
            const widthSizeL = subLength > 20 && 'size-l';
            const widthSizeM = subLength >= 8 && 'size-m';
            const width = widthSizeXL || widthSizeL || widthSizeM || '';
            return (
              <div key={menu.name}>
                <li className="nav-item" onMouseMove={onHover}>
                  <a href={menu.link} title={menu.name}>{menu.name}</a>
                </li>
                {menu.hasSubMenu ? (
                  <div className={`header_menu-sub ${width} ${activeSubMenu}`}>
                    <ul className="sub-menu">
                      {newSub?.map((sub: any, idx) => {
                        let subLink = `${menu?.subLink}${sub?.slug || ''}`;
                        if (sub?.subKey === 'genres') {
                          subLink = `/genre/${menu.key}/${sub?.slug}`;
                        }
                        return (
                          <li key={idx}>
                            <a href={`${subLink}`} title={sub?.name}>{sub?.name}</a>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                ) : null}
              </div>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Menu;
