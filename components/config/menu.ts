export const menuConfig = [
  {
    link: '/home',
    mobileLink: '/home',
    subLink: '#',
    name: 'Home',
    key: 'home',
    hasSubMenu: false,
  },
  {
    link: '/movies',
    mobileLink: '',
    subLink: '/movies/',
    name: 'Movies',
    key: 'movies',
    subKey: 'genres',
    hasSubMenu: true,
    subMenus: [
      {
        slug: 'recently-updated',
        name: 'Updated',
        key: 'updated',
      },
      {
        slug: 'recently-added',
        name: 'Addded',
        key: 'added',
      },
    ],
  },
  {
    link: '/tv-series',
    mobileLink: '',
    subLink: '/tv-series/',
    name: 'TV Series',
    key: 'tv-series',
    subKey: 'genres',
    hasSubMenu: true,
    subMenus: [
      {
        slug: 'recently-updated',
        name: 'Updated',
        key: 'updated',
      },
      {
        slug: 'recently-added',
        name: 'Addded',
        key: 'added',
      },
    ],
  },
  {
    link: '/',
    mobileLink: '',
    subLink: '/',
    name: 'Types',
    key: 'types',
    hasSubMenu: true,
    subMenus: [],
  },
  {
    link: '/',
    mobileLink: '',
    subLink: '/',
    name: 'Country',
    key: 'country',
    hasSubMenu: true,
    subMenus: [],
  },
  {
    link: '/filter?status=6',
    mobileLink: '/filter?status=6',
    subLink: '/filter?status=6',
    name: 'Ongoing',
    key: 'ongoing',
    hasSubMenu: false,
  },
  {
    link: '/filter?is_comingsoon=1',
    mobileLink: '/filter?is_comingsoon=1',
    subLink: '/filter?is_comingsoon=1',
    name: 'Upcoming',
    key: 'upcoming',
    hasSubMenu: false,
  },
];

export default menuConfig;
