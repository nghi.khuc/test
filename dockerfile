FROM node:14-alpine AS base
ENV PORT 3000
EXPOSE 3000

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app
# COPY /usr/src/app/.env.local.template /usr/src/app/.env.production

RUN npm run build

CMD "npm" "run" "start" 
