export const PAGE_NOT_USE_ASIDE = /(\/$|faq$|contact$|admin.*)/i;
export const REGEX_PAGE_USE_QUICK_FILTER = /(\/$|home$|movies$|tv-series$|movies\/watch.*|tv-series\/watch.*)/i;
export const TOP_VIEW = {
  TOP: 0,
  DAY: 1,
  WEEK: 2,
  MONTH: 3,
};

export const TYPE_CONTENT_PAGE = {
  post_index: {
    type: 1,
    key: 'post_index',
    link: '/admin/language/en/post_index',
  },
  home_under_slider: {
    type: 2,
    key: 'home_under_slider',
    link: '/admin/language/en/home_under_slider',
  },
  faq_post: {
    type: 3,
    key: 'faq_post',
    link: '/admin/language/en/faq_post',
  },
};

export const TYPE_ADS_PAGE = {
  under_search_index: {
    type: 1,
    key: 'ads_under_search_index',
    file: 'static/ads/index_under_search.json',
    link: '/admin/ads/under_search_index',
  },
  home_under_search: {
    type: 2,
    key: 'ads_home_under_search',
    file: 'static/ads/home_under_search.json',
    link: '/admin/ads/home_under_search',
  },
  home_under_movie_recently_updated: {
    type: 3,
    key: 'ads_home_under_movie_recently_updated',
    file: 'static/ads/home_under_movie_recently_updated.json',
    link: '/admin/ads/home_under_movie_recently_updated',
  },
  watch_under_player: {
    type: 4,
    key: 'ads_watch_under_player',
    file: 'static/ads/watch_under_player.json',
    link: '/admin/ads/watch_under_player',
  },
  under_slider_movie: {
    type: 5,
    key: 'ads_under_slider_movie',
    file: 'static/ads/under_slider_movie.json',
    link: '/admin/ads/under_slider_movie',
  },
  under_slider_tv: {
    type: 6,
    key: 'ads_under_slider_tv',
    file: 'static/ads/under_slider_tv.json',
    link: '/admin/ads/under_slider_tv',
  },
  under_filter_sidebar: {
    type: 7,
    key: 'ads_under_filter_sidebar',
    file: 'static/ads/under_filter_sidebar.json',
    link: '/admin/ads/under_filter_sidebar',
  },
  top_filter_sidebar: {
    type: 8,
    key: 'ads_top_filter_sidebar',
    file: 'static/ads/top_filter_sidebar.json',
    link: '/admin/ads/top_filter_sidebar',
  },
  top_footer: {
    type: 0,
    key: 'ads_top_footer',
    file: 'static/ads/top_footer.json',
    link: '/admin/ads/top_footer',
  },
  top_information_film: {
    type: 10,
    key: 'ads_top_information_film',
    file: 'static/ads/top_information_film.json',
    link: '/admin/ads/top_information_film',
  },
  under_information_film: {
    type: 11,
    key: 'ads_under_information_film',
    file: 'static/ads/under_information_film.json',
    link: '/admin/ads/under_information_film',
  },
};

export const CONFIG_TITLE_DES_PAGE = {
  type: {
    type: 0,
    keyReplace: '[type_film]',
    key: 'document_type',
    file: 'static/document/type.json',
    link: '/admin/document/type',
  },
  genres: {
    type: 1,
    keyReplace: '[Genre]',
    key: 'document_genres',
    file: 'static/document/genres.json',
    link: '/admin/document/genres',
  },
  country: {
    type: 2,
    keyReplace: '[Country]',
    key: 'document_country',
    file: 'static/document/country.json',
    link: '/admin/document/country',
  },
  director: {
    type: 3,
    keyReplace: '[people]',
    key: 'document_director',
    file: 'static/document/director.json',
    link: '/admin/document/director',
  },
  cast: {
    type: 4,
    keyReplace: '[people]',
    key: 'document_cast',
    file: 'static/document/cast.json',
    link: '/admin/document/cast',
  },
  tv: {
    type: 5,
    keyReplace: '[Title], [Release_Year], [Quality], [Ep_Name]', //! specify, import direct at page serverside
    key: 'document_tv',
    file: 'static/document/tv.json',
    link: '/admin/document/tv',
  },
  movie: {
    type: 6,
    keyReplace: '[Title], [Release_Year], [Quality]', //! specify, import direct at page serverside
    key: 'document_movie',
    file: 'static/document/movie.json',
    link: '/admin/document/movie',
  },
  tag: {
    type: 7,
    keyReplace: '[Tags]',
    key: 'document_tag',
    file: 'static/document/tag.json',
    link: '/admin/document/tag',
  },
  hagtag: {
    type: 8,
    keyReplace: '[Hagtag]',
    key: 'document_hagtag',
    file: 'static/document/hagtag.json',
    link: '/admin/document/hagtag',
  },
  az: {
    type: 9,
    keyReplace: '[AZ]',
    key: 'document_az',
    file: 'static/document/az.json',
    link: '/admin/document/az',
  },
};

export const TYPE_VIEWS = {
  TV_SERIES: 'tv-series',
  MOVIES: 'movies',
};
export const BREAD_CRUMB_PAGE = {
  ongoing: 'Ongoing ',
  upcoming: 'Top Upcoming',
  'recently-updated': 'Recently Updated',
  'recently-added': 'Recently Added',
  watch: 'Watching ',
  filter: 'Filter ',
  user_profile_watchlist: 'User Profile Watchlist',
  tag: 'Tag',
  hagtag: 'Hagtag',
};
export const IGNORE_SLUG = /(\/$|watch$|server.*$|ep.*$|person$|#$)/i;

export const URLs = {
  home: '/home',
  search: '/search',
  genres: '/genres',
  types: '/types',
  slider: '/slider',
  movies: '/movies',
  tvSeries: '/tv-series',
  seasons: '/seasons',
  countries: '/countries',
  trackError: '/error/log/player',
  userProfile: '/person',
  schedule: '/schedule',
  tag: '/tag',
  hagtag: '/hagtag',
  filter: '/filter',
};

export const VaultURLs:any = {
  development: {
    API_URL: 'https://admindata.iocodes.com',
    IO_CODE_URL: 'https://iocodes.com',
    API_LOG: 'https://enqdjkdhn8yns7v.m.pipedream.net',
  },
}.development;

export const TOP_VIEW_TYPE = {
  TODAY: '1',
  WEEK: '2',
  MONTH: '3',
};

export const configTemplate = {
  domain: '',
  logo: '',
  timeCacheContent: 0,
  timeCacheSetting: 0,
  footerText: '',
  contacts: {
    reddit: '',
    discord: '',
    telegram: '',
    twitter: '',
    facebook: '',
    email: '',
  },
  az: true,
  title: '',
  description: '',
  analytic: '',
};

export default VaultURLs;
