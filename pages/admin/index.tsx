/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState, useMemo } from 'react';
import { getAdminLogin, postAdminLogin } from '~/services/client';
import { convertStringUpperCaseFirstChar } from '~/utils/common';
import { TYPE_ADS_PAGE, TYPE_CONTENT_PAGE, CONFIG_TITLE_DES_PAGE } from '~/utils/contains';

export const withLogin = (Component) => (props) => {
  const [loading, setLoading] = React.useState(true);
  const fetchConfig = async () => {
    try {
      await getAdminLogin();
      setLoading(false);
    } catch (error) {
      window.location.href = '/home';
    }
  };
  useEffect(() => {
    fetchConfig();
  }, []);

  if (loading) return <div>loading</div>;
  return <Component {...props} />;
};

export default function Admin() {
  const [loading, setLoading] = useState(false);

  const [values, setValues] = useState('');

  const fetchConfig = async () => {
    const responseConfig = await getAdminLogin();
    setValues(responseConfig);
  };
  useEffect(() => {
    fetchConfig();
  }, []);

  const onSubmitValue = async (event) => {
    setLoading(true);
    try {
      event.preventDefault();
      const [
        usernameText,
        passwordText,
      ] = event.target;
      const username = usernameText.value as string;
      const password = passwordText.value as string;
      const params = {
        username,
        password,
      };
      const data = await postAdminLogin(params);
      setValues(data);
    } catch {
      console.error('login failed');
    } finally {
      setLoading(false);
    }
  };
  const addPageAds = useMemo(() => Object.keys(TYPE_ADS_PAGE).map((page: any) => (
    <React.Fragment key={page}>
      <a href={TYPE_ADS_PAGE[page].link}>{`ADS Page - ${convertStringUpperCaseFirstChar(page)}`}</a>
      <br />
    </React.Fragment>
  )), []);

  const addPageDocument = useMemo(() => Object.keys(CONFIG_TITLE_DES_PAGE).map((page: any) => (
    <React.Fragment key={page}>
      <a href={CONFIG_TITLE_DES_PAGE[page].link}>{`Document - ${convertStringUpperCaseFirstChar(page)}`}</a>
      <br />
    </React.Fragment>
  )), []);

  const addPageContent = useMemo(() => Object.keys(TYPE_CONTENT_PAGE).map((page: any) => (
    <React.Fragment key={page}>
      <a href={TYPE_CONTENT_PAGE[page].link}>{`Content Page - ${convertStringUpperCaseFirstChar(page)}`}</a>
      <br />
    </React.Fragment>
  )), []);

  if (values) {
    return (
      <div>
        <a href="/admin/post">Page Config</a>
        <br />
        {addPageAds}
        {addPageContent}
        {addPageDocument}
      </div>
    );
  }
  return (
    <div className="information_page">
      <div className="container">
        <div className="information_page-wrap">
          <div className="information_page-content">
            <h2 className="h2-heading">Login</h2>
            <p />
            <div className="row">
              <div className="col-xl-6 col-lg-8 col-md-12">
                <form className="form-dark" onSubmit={onSubmitValue}>
                  <div className="form-group">
                    <label htmlFor="post-domain">UserName</label>
                    <input
                      type="text"
                      className="form-control"
                      id="post-domain"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="post-time-cache-content">Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="post-time-cache-content"
                    />
                  </div>

                  <div className="form-group">
                    <div className="mt-5" />
                    <button type="submit" id="btn-focus" className="btn btn-block btn-lg btn-focus">Submit</button>
                  </div>
                  <div className="form-group post-btn mb-0">
                    <div className="loading-relative" id="login-loading" style={{ display: loading ? 'block' : 'none' }}>
                      <div className="loading">
                        <div className="span1" />
                        <div className="span2" />
                        <div className="span3" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
