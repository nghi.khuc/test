/* eslint-disable max-len */
import React from 'react';

const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const arrayChars = chars.split('');

const ListChars = ({ slug }:any) => (
  <ul className="ulclear az-list">
    <li><a href="/az-list">All</a></li>
    <li><a href="/az-list/other">#</a></li>
    <li><a href="/az-list/0-9">0-9</a></li>
    {
      arrayChars.map((item) => (
        <li key={item} className={item === slug ? 'active' : ''}>
          <a
            href={`/az-list/${item}`}
          >
            {item}
          </a>
        </li>
      ))
    }
  </ul>
);
export default ListChars;
