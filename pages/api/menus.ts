import { getListCountry, getListGenres, getListTypes } from '~/services/server';
import configSystem, { KEYS_REDIS } from '~/utils/config';
import VaultURLs from '~/utils/contains';
import redis, { redisGetAsync, timeCachingSystem } from '~/utils/redis';
import { exception, HTTP_STATUS, withMiddleware } from '~/utils/run';

const normalizeGenre = (data) => data.map((item) => ({
  ...item,
  subKey: 'genres',
}));

const normalizeType = (data) => data.map((item) => ({
  ...item,
  slug: `type/${item.slug}`,
}));

const normalizeCountry = (data) => data.map((item) => ({
  ...item,
  slug: `country/${item.slug}`,
}));

const handler = async (req, res) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    const key = KEYS_REDIS.MENUS;
    const client = redis();
    const value = await redisGetAsync(key);
    if (value) return res.RESP(JSON.parse(value));
    const [resGenres, resTypes, resCountry] = await Promise.all([getListGenres(), getListTypes(), getListCountry()]);
    const genres = normalizeGenre(resGenres.data);
    const types = normalizeType(resTypes.data);
    const country = normalizeCountry(resCountry.data);
    const result = {
      data: {
        data: {
          genres,
          types,
          country,
        },
      },
    };
    client.set(key, JSON.stringify(result), 'EX', await timeCachingSystem(configSystem.EXPIRE_TIME_MENUS));
    return res.RESP(result);
  } catch (e) {
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
