/* eslint-disable no-new */
/* eslint-disable no-throw-literal */
// import 'isomorphic-unfetch';
import { nanoid } from '@reduxjs/toolkit';
import { CookieSerializeOptions, serialize } from 'cookie';
import crypto from 'crypto-js';
import jwt from 'jsonwebtoken';
import { NextApiRequest, NextApiResponse } from 'next';
import querystring from 'querystring';
// import Cors from 'cors';
import configSystem from './config';

if (typeof window !== undefined) {
  new Error('Just server side!');
}
//* Server-side
export interface IToken {
  token: string
  refreshToken: string
}

export interface ISession {
  req: NextApiRequest
  res: any
}

export const HTTP_STATUS = {
  OK: 200,
  INTERNAL_ERROR: 500,
  AUTHENTICATION_ERROR: 401,
  ERROR: 400,
};

const responseSuccess = (res, data, other = {}) => {
  res.status(200).json({
    isSuccess: true,
    data,
    ...other,
  });
};

const responseErrors = (res, status, message, data = {}) => {
  res.status(status).json({
    isSuccess: false,
    message,
    data,
  });
};

export const exception = (e:any = {}) => {
  if (e?.response) {
    return ({
      status: e?.response?.status,
      message: String(e?.message || e?.response?.message),
      data: e?.response?.data,
    });
  }
  return ({
    status: e.status,
    message: String(e.messsage),
  });
};

const encodeCookie = (jwtString: string) => crypto.AES.encrypt(jwtString, configSystem.SECRET_COOKIE).toString();

const decodeCookie = (jwtString: string) => {
  if (!jwtString) return undefined;
  const bytes = crypto.AES.decrypt(jwtString, configSystem.SECRET_COOKIE);
  return JSON.parse(bytes.toString(crypto.enc.Utf8));
};

export const setCookie = (
  req: NextApiRequest,
  res: NextApiResponse,
  name: string,
  value: unknown,
  cookieOptions?: CookieSerializeOptions,
) => {
  const options = { ...cookieOptions } || {};
  const stringValue = typeof value === 'object' ? JSON.stringify(value) : String(value);

  if ('maxAge' in options) {
    if (options.maxAge) {
      options.expires = new Date(Date.now() + options.maxAge);
      options.maxAge /= 1000;
    }
  }
  const cookie = serialize(name, String(stringValue), {
    httpOnly: true,
    sameSite: true,
    secure: true,
    encode: encodeCookie,
    path: '/',
    ...options,
  });
  res.setHeader('Set-Cookie', cookie);
};

const generateToken = (data: string, secret: string, expiresIn: number) => jwt.sign({
  data,
}, secret, { expiresIn });

const verifySessionID = (Session) => {
  try {
    if (process.env.NEXT_PUBLIC_ENV === 'dev') return true;
    if (!Session) return false;
    const { token } = decodeCookie(Session);
    jwt.verify(token, configSystem.SECRET_SESSION);
    return true;
  } catch {
    return false;
  }
};

const verifyToken = (token) => {
  try {
    return jwt.verify(token, configSystem.SECRET_TOKEN);
  } catch {
    return '';
  }
};

const verifyRefreshToken = (token) => {
  try {
    return jwt.verify(token, configSystem.SECRET_REFRESH_TOKEN);
  } catch {
    return '';
  }
};

export const runSession = async ({ req, res }) => {
  const isExpireToken = verifySessionID(req.cookies[configSystem.KEY_SESSION]);
  if (!isExpireToken) {
    const sessionID = nanoid();
    const token = generateToken(sessionID, configSystem.SECRET_SESSION, configSystem.EXPIRE_SESSION);
    setCookie(req, res, configSystem.KEY_SESSION, { token });
  }
};

export const setCookieAdmin = ({ req, res }) => {
  const sessionID = nanoid();
  const tokenNew = generateToken(sessionID, configSystem.SECRET_SESSION_ADMIN, 60 * 60 * 24); // 1d

  setCookie(req, res, configSystem.KEY_SESSION_ADMIN, tokenNew, { encode: (val) => val });
};

export const verifyCookieAdmin = (req) => {
  try {
    if (!req.cookies[configSystem.KEY_SESSION_ADMIN]) return false;
    const data = req.cookies[configSystem.KEY_SESSION_ADMIN];
    jwt.verify(data, configSystem.SECRET_SESSION_ADMIN);
    return true;
  } catch (error) {
    return false;
  }
};

export const setCookieToken = ({ req, res }, value, isRemember?: boolean) => {
  const { token, refreshToken } = value;
  const tokenNew = generateToken(token, configSystem.SECRET_TOKEN, isRemember ? configSystem.EXPIRE_TIME_HAS_REMEMBER : configSystem.EXPIRE_TOKEN);
  const reFreshTokenNew = generateToken(refreshToken, configSystem.SECRET_REFRESH_TOKEN,
    isRemember ? configSystem.EXPIRE_TIME_HAS_REMEMBER : configSystem.EXPIRE_REFRESH_TOKEN);
  setCookie(req, res, configSystem.KEY_TOKEN, { tokenNew, reFreshTokenNew },
    {
      maxAge: (isRemember ? configSystem.EXPIRE_TIME_HAS_REMEMBER : configSystem.EXPIRE_REFRESH_TOKEN) * 1000,
      path: '/',
    });
};
export const getCookieToken = ({ req }) => {
  if (!req.cookies[configSystem.KEY_TOKEN]) return undefined;
  const { tokenNew, reFreshTokenNew } = decodeCookie(req.cookies[configSystem.KEY_TOKEN]);
  const [token, refreshToken] = [verifyToken(tokenNew), verifyRefreshToken(reFreshTokenNew)];
  return { token: token.data, refreshToken: refreshToken.data };
};

export const clearCookieToken = ({ req, res }) => {
  setCookie(req, res, configSystem.KEY_TOKEN, '', {
    maxAge: 1000,
    path: '/',
  });
};

function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }

      return resolve(result);
    });
  });
}

// const cors = Cors();

export const withMiddleware = (handler) => async (req, res) => {
  req.getStructureDynamicRouterServer = (baseUrl = '') => {
    const {
      method, query, body: data, headers,
    } = req;
    const { slug = [], ...other } = query;
    const headersNew = { ...headers };
    delete headersNew.host;

    const path = [...slug].join('/');
    const queryParams = querystring.stringify(other);

    const isExpireToken = verifySessionID(req.cookies[configSystem.KEY_SESSION]);
    if (!isExpireToken || !req.headers.referer) return null;

    return {
      url: `${baseUrl}${path ? `/${path}` : ''}${queryParams ? `?${queryParams}` : ''}`,
      method,
      data: data || undefined,
    };
  };

  res.RESP = (resp: any = {}) => {
    if (typeof resp !== 'object') {
      responseSuccess(res, resp.toString());
    } else {
      const { data } = resp;
      const { data: dataFinal, ...other } = data || {};
      responseSuccess(res, data?.data || data.result, data?.other || other);
    }
  };

  res.ERROR = (status = 500, message, data = {}) => {
    responseErrors(res, status, message, data);
  };

  return handler(req, res);
};
