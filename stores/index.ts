import { configureStore } from '@reduxjs/toolkit';

import loggerMiddleware from './middleware/logger';
import rootReducer from './slices';

const store = configureStore({
  reducer: rootReducer(),
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(loggerMiddleware),
});
export default store;
