/* eslint-disable react/button-has-type */
import React, { useEffect, useState } from 'react';
import Container from 'containers/AZList';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import { getFilterListFilm, getListAZFilms } from '~/services/client';
import { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';

const AzListBySlug = () => {
  const router = useRouter();
  const { query: { slug, page } } = router;
  const firstSlug = slug ? slug[0] : '';
  const [data, setResponse] = useState([]);
  const fectApi = async () => {
    const response = await getFilterListFilm({
      page: page || 1,
      length: 20,
      az: firstSlug,
    });
    setResponse(response);
  };
  useEffect(() => {
    fectApi();
  }, [router]);
  return (
    <Container response={data} slug={firstSlug} />
  );
};

export default AzListBySlug;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;

  const titleKey = CONFIG_TITLE_DES_PAGE.az.key;
  const { keyReplace } = CONFIG_TITLE_DES_PAGE.az;

  const gereSlug = query.slug?.toString() || '';
  const doc = await redisGetValAsync(titleKey);
  const regex = new RegExp(`\\${keyReplace}`, 'g');
  const title = (doc?.title || '').replace(regex, gereSlug);
  const description = (doc?.description || '').replace(regex, gereSlug);

  return {
    props: {
      query,
      title,
      description,
    },
  };
};
