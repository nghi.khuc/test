/* eslint-disable no-underscore-dangle */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import * as SearchApi from 'services/client';
import { TOP_VIEW } from '~/utils/contains';

const mappingType = {
  [TOP_VIEW.TOP]: 'top',
  [TOP_VIEW.DAY]: 'day',
  [TOP_VIEW.WEEK]: 'week',
  [TOP_VIEW.MONTH]: 'month',
};

const initialState = {
  fetchedMovie: false,
  fetchedTV: false,
  tvs: {
    top: [],
    day: [],
    week: [],
    month: [],
  },
  movies: {
    top: [],
    day: [],
    week: [],
    month: [],
  },
};

export const fetchFilmTopViewMovie = createAsyncThunk(
  'film/get_film_top_view_movie',
  async ({ type, page }:any) => {
    try {
      const response = await SearchApi.getListFilmTopMv({ type: type === 0 ? undefined : type, page: page || 1 });
      return { ...response, type: mappingType[type] };
    } catch (error) {
      return { type: mappingType[type], data: [] };
    }
  },
);

export const fetchFilmTopViewTV = createAsyncThunk(
  'film/get_film_top_view_tv',
  async ({ type, page }:any) => {
    try {
      const response = await SearchApi.getListFilmTopTv({ type: type === 0 ? undefined : type, page: page || 1 });
      return { ...response, type: mappingType[type] };
    } catch (error) {
      return { type: mappingType[type], data: [] };
    }
  },
);

export const topViewSlices = createSlice({
  name: 'topViewSlices',
  initialState,
  reducers: {

  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchFilmTopViewTV.pending, (state) => {
      state.fetchedTV = false;
    });
    builder.addCase(fetchFilmTopViewTV.fulfilled, (state, action:any) => {
      const { page, lastPage } = action.payload;
      state.tvs = {
        ...state.tvs,
        ...action.payload,
        [action.payload.type]: {
          ...state.tvs[action.payload.type],
          page,
          lastPage,
          [page]: action.payload.data,
        },
      };
      state.fetchedTV = true;
    });
    builder.addCase(fetchFilmTopViewTV.rejected, (state, action:any) => {
      state.fetchedTV = false;
      state.tvs = {
        ...state.tvs,
        [action.payload.type]: [],
      };
    });

    builder.addCase(fetchFilmTopViewMovie.pending, (state) => {
      state.fetchedMovie = false;
    });
    builder.addCase(fetchFilmTopViewMovie.fulfilled, (state, action:any) => {
      const { page, lastPage } = action.payload;
      state.movies = {
        ...state.movies,
        ...action.payload,
        [action.payload.type]: {
          ...state.movies[action.payload.type],
          page,
          lastPage,
          [page]: action.payload.data,
        },
      };
      state.fetchedMovie = true;
    });
    builder.addCase(fetchFilmTopViewMovie.rejected, (state, action:any) => {
      state.fetchedTV = false;
      state.movies = {
        ...state.movies,
        [action.payload.type]: [],
      };
    });
  },
});

// Action creators are generated for each case reducer function

export default topViewSlices.reducer;
