/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
import Container from 'containers/User/WatchList';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import React from 'react';
import { getUserProfileWatchList, getUserProfileWatchListFilms } from '~/services/client';
import VaultURLs, { CONFIG_TITLE_DES_PAGE } from '~/utils/contains';
import { redisGetValAsync } from '~/utils/redis';
import request from '~/utils/request';

export function Page(props: any) {
  const router = useRouter();
  const [user, setUser] = React.useState<any>(props?.data || {});

  const getInfo = async () => {
    const { slug } = router.query;
    const [id] = (slug || '').toString().split('.').reverse();
    const films = await getUserProfileWatchListFilms(id);
    setUser({
      ...user,
      movies: films.data?.movies?.map((film) => ({ ...film, type: 'movies' })) || [],
      tvseries: films.data?.tvseries?.map((film) => ({ ...film, type: 'tv-series' })) || [],
    });
  };

  React.useEffect(() => {
    getInfo();
  }, []);
  return (
    <Container user={user} casts />
  );
}

Page.PAGE_NOT_USE_ASIDE = true;

export default Page;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const [id] = (query?.slug || '').toString().split('.').reverse();
  const resp = await request(
    {
      url: `${VaultURLs.API_URL}/api/person/${id}`,
      options: {
        method: 'GET',
      },
    },
  ).catch(() => ({ data: {} }));
  const { data } = resp;
  const titleKey = data.know_for === 'Acting' ? CONFIG_TITLE_DES_PAGE.cast.key : CONFIG_TITLE_DES_PAGE.director.key;
  const keyReplace = data.know_for === 'Acting' ? CONFIG_TITLE_DES_PAGE.cast.keyReplace : CONFIG_TITLE_DES_PAGE.director.keyReplace;
  const gereSlug = query.slug?.toString() || '';

  const doc = await redisGetValAsync(titleKey);
  const regex = new RegExp(`\\${keyReplace}`, 'g');
  const title = (doc?.title || '').replace(regex, gereSlug);
  const description = (doc?.description || '').replace(regex, gereSlug);
  return {
    props: {
      query,
      data,
      title,
      description,
    },
  };
};
