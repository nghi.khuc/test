import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import VaultURLs from '~/utils/contains';
import {
  clearCookieToken, exception, getCookieToken, HTTP_STATUS, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/user/account/changepass`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const data = getCookieToken({ req });

    if (!data) {
      clearCookieToken({ req, res });
      res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    }

    if (data) {
      await axios({
        url: baseURL,
        method: 'POST',
        data: configRequest.data,
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });
      clearCookieToken({ req, res });
    }
    return res.RESP('oke');
  } catch (e) {
    trackLog(req, e);
    const { status, message, data } = exception(e);
    return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
