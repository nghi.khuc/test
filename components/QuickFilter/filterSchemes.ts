// https://9anime.vc/filter?keyword=&type=2,1&status=1&season=2&language=sub&sort=all&year=2017,2014,2011&genre=14,43

const years = (back) => {
  const year = new Date().getFullYear();
  const newYears = Array.from({ length: back }, (v, i) => year - back + i + 1);
  return newYears.map((item) => ({ id: item, name: item })).sort((a, b) => b.id - a.id);
};

export default [
  {
    type: 'genres',
    name: 'Genre',
    value: [],
    opts: {
      typeInput: 'checkbox',
    },
  },
  {
    type: 'country',
    name: 'Country',
    value: [],
    opts: {
      typeInput: 'checkbox',
    },
  },
  {
    type: 'types',
    name: 'Types',
    value: [],
    opts: {
      typeInput: 'checkbox',
    },
  },
  {
    type: 'year',
    name: 'Year',
    value: years(20),
    opts: {
      typeInput: 'checkbox',
    },
  },
  {
    type: 'status',
    name: 'Status',
    value: [
      {
        id: 2,
        name: 'Finished',
      },
      {
        id: 6,
        name: 'Ongoing',
      },
      {
        id: 3,
        name: 'Not yet aired',
      },
    ],
    opts: {
      typeInput: 'radio',
    },
  },
  {
    type: 'sort',
    name: 'Sort',
    value: [
      {
        id: 'updated_at',
        name: 'Recently Updated',
      },
      {
        id: 'created_at',
        name: 'Recently added',
      },
      {
        id: 'release_date',
        name: 'Release date',
      },
      {
        id: 'views',
        name: 'Views',
      },
      {
        id: 'ranking',
        name: 'Ranking',
      },
      {
        id: 'az',
        name: 'Name A-Z',
      },
    ],
    opts: {
      typeInput: 'radio',
    },
  },
  {
    type: 'option',
    name: 'Option',
    value: [
      {
        id: 'movies',
        name: 'Movies',
      },
      {
        id: 'tv-series',
        name: 'TV Series',
      },
      // {
      //   id: 'all',
      //   name: 'All',
      // },
    ],
    opts: {
      typeInput: 'radio',
    },
  },
];
