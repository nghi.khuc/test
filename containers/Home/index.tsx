/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable react/button-has-type */
import React from 'react';
import Slider from 'components/Slider';
import ListFilms from 'components/ListFilms';
import LazyLoad from 'react-lazyload';
import Schedule from '~/components/Schedules';

interface Home {
  sliders: [],
  listFilmsTVs: [],
  listFilmMovies: []
  contentUnderSlider: string
  adsSliderParse: string
  contentUnderMovie: string
}

const mackHTML = (str) => !!str && str.replace(/(<p>|<\/p>)/gi, '<div></div>');
const HomeComponent = ({
  sliders, listFilmsTVs, listFilmMovies, ...props
}: Home) => (
  (
    <React.Fragment>
      <div className="deslide-wrap">
        <Slider sliders={sliders} />
      </div>
      <div id="text-home">
        {!!props.contentUnderSlider && (
          <div className="alert alert-info">
            <div dangerouslySetInnerHTML={{ __html: mackHTML(props.contentUnderSlider) }} />
          </div>
        )}
      </div>
      <div id="text-home">
        {!!props.adsSliderParse && (
          // <div className="alert alert-info">
          <div dangerouslySetInnerHTML={{ __html: mackHTML(props.adsSliderParse) }} />
          // </div>
        )}
      </div>
      <LazyLoad placeholder="........">
        <ListFilms listFilms={listFilmsTVs} title="TV Series Recently Updated" />
        <ListFilms listFilms={listFilmMovies} title="Movies Recently Updated" />
      </LazyLoad>
      <div className="clearfix" />
      {
        !!props.contentUnderMovie && <span className="size-s" dangerouslySetInnerHTML={{ __html: props.contentUnderMovie }} />
      }
      <Schedule {...props} />
    </React.Fragment>
  )
);
export default HomeComponent;
