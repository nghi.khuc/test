import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import { trackLog } from '~/services/server';
import VaultURLs from '~/utils/contains';
import {
  clearCookieToken, exception, getCookieToken, HTTP_STATUS, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const configRequest = req.getStructureDynamicRouterServer('');
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });

    const data: any = getCookieToken({ req });
    if (!data) {
      clearCookieToken({ req, res });
      return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    }

    if (req.method === 'POST') {
      const baseURL = `${VaultURLs.API_URL}/api/user/rating/create`;
      const result = await axios({
        url: baseURL,
        method: 'POST',
        data: req.body,
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });

      return res.RESP(result);
    }
    if (req.method === 'GET') {
      const baseURL = `${VaultURLs.API_URL}/api/user/rating/get`;

      const result = await axios({
        url: baseURL,
        method: 'POST',
        data: req.body,
        headers: {
          Authorization: `bearer ${data.token}`,
        },
      });

      return res.RESP(result);
    }

    return res.RESP('ok');
  } catch (e) {
    trackLog(req, e);
    clearCookieToken({ req, res });
    return res.status(200).send(undefined);
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
