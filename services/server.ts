/* eslint-disable no-new */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import axios from 'axios';
import VaultURLs, { URLs } from '~/utils/contains';
import request from '~/utils/request';
import { getIP } from '~/utils/common';
import { exception } from '~/utils/run';

if (typeof window !== undefined) {
  new Error('Just server side!');
}

const mappingType = {
  'tv-series': URLs.tvSeries,
  movie: URLs.movies,
  movies: URLs.movies,
};

const typeMovie = ['movie', 'movies'];

interface ISlider {
  type: 'movie' | 'tv-series' | 'movies'
}

interface IPramsListFilm {
  page?: number,
  length?: number,
  orderby?: string,
  view?: boolean,
  genre?: any,
  type?: any,
}

interface IParamsStreamUrl{
  id: number|string,
  type: 'movie' | 'tv-series' | 'movies' | string | string[] | undefined
}

export const getListGenres = async () => {
  const url = `${VaultURLs.API_URL}/api${URLs.genres}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListTypes = async () => {
  const url = `${VaultURLs.API_URL}/api${URLs.types}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};
interface ISlider {
  type: 'movie' | 'tv-series' | 'movies'
}
export const getSliders = async (param: ISlider) => {
  const url = `${VaultURLs.API_URL}/api${URLs.slider}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params: param,
      },
    },
  );
  return response;
};

export const getListCountry = async () => {
  const url = `${VaultURLs.API_URL}/api${URLs.countries}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListFilmsTVSeries = async (params: IPramsListFilm) => {
  const url = `${VaultURLs.API_URL}/api${URLs.tvSeries}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  ).catch(() => []);
  if (!response.status) {
    return {
      data: [],
    };
  }
  return response;
};

export const getFilmById = async ({ id, type }: IParamsStreamUrl) => {
  if (!type) return {};
  const slug = mappingType[(type || '').toString()];
  const url = `${VaultURLs.API_URL}/api${slug}/${id}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListFilmsMoviesSeries = async (params: IPramsListFilm) => {
  const url = `${VaultURLs.API_URL}/api${URLs.movies}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  ).catch(() => []);
  if (!response.status) {
    return {
      data: [],
    };
  }
  return response;
};

export const getUrlStream = async ({ id, type }: IParamsStreamUrl) => {
  const ip_address = await getIP();
  const slug = mappingType[(type || '').toString()];
  let url;
  if (typeMovie.includes((type || '').toString())) {
    url = `${VaultURLs.API_URL}/api${slug}/${id}/stream_urls`;
  } else {
    url = `${VaultURLs.API_URL}/api/episodes/${id}/stream_urls`;
  }
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  const streamsData = response.data;
  const streamsUrl = streamsData?.streamurls;
  for (const i in streamsUrl) {
    const files = await getURLIOCodes(streamsUrl[i].url, ip_address);
    streamsUrl[i].files = files;
  }

  return streamsData;
};

export const getSeasonsTV = async ({ id, type }) => {
  const slug = mappingType[type];
  const url = `${VaultURLs.API_URL}/api${slug}/${id}/seasons`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  const seasons = response.data;
  for (const i in seasons) {
    const eps = await getEpisodes(seasons[i]._id);
    seasons[i].episodes = eps;
  }
  return seasons;
};

export const getEpisodes = async (id) => {
  const url = `${VaultURLs.API_URL}/api${URLs.seasons}/${id}/episodes`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response.data;
};

export const getURLIOCodes = async (url_stream, ip_client) => {
  const url = `${VaultURLs.IO_CODE_URL}/api/index.php?url=${url_stream}&ip_client=${ip_client}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
      },
    },
  );
  return response;
};

export const getListAZFilms = async (params) => {
  const url = `${VaultURLs.API_URL}/api${URLs.tvSeries}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getListFilmSimilar = async (params, type, id) => {
  const slug = mappingType[type];
  const url = `${VaultURLs.API_URL}/api${slug}/${id}/similar`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params,
      },
    },
  );
  return response;
};

export const getListFilmBySearch = async (search) => {
  const url = `${VaultURLs.API_URL}/api${URLs.search}`;
  const response = await request(
    {
      url,
      options: {
        method: 'GET',
        params: { q: search },
      },
    },
  );
  return response;
};

export const trackLog = (req, e, other?) => {
  const {
    method, query, body, url, headers,
  } = req;
  // if (process.env.NEXT_PUBLIC_ENV === 'dev') return '127.0.0.1';
  if (VaultURLs.API_LOG) {
    const { status = '', message = '', data = '' } = exception(e);
    const post = {
      status, message, data, method, query, body, url, headers, other,
    };
    axios.post(VaultURLs.API_LOG, post);
  }
};
