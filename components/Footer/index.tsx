/* eslint-disable max-len */
import React from 'react';
import Image from 'next/image';
import ListChars from './ListChars';
import Content from './Content';
import ToggleTranslate from './ToggleTranslate';

export default function index(props: any) {
  return (
    <div id="footer">
      <div className="container">
        {/* <div className="footer-icon">
          <Image
            src="/images/footer-icon.png"
            alt="9anime"
            width={260}
            height={369}
          />

        </div> */}
        <ToggleTranslate />
        <div className="footer-az">
          <div className="block mb-2">
            <span className="ftaz">A-Z LIST</span>
            <span className="size-s">Searching anime order by alphabet name A to Z.</span>
          </div>
          <ListChars />
          <div className="clearfix" />
        </div>
        <Content {...props} />
      </div>
    </div>
  );
}

// tree
