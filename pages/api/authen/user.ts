import axios from 'axios';
import { NextApiRequestCustom, NextApiResponseCustom } from 'next';
import VaultURLs from '~/utils/contains';
import {
  clearCookieToken,
  exception, getCookieToken, HTTP_STATUS, withMiddleware,
} from '~/utils/run';

const handler = async (req: NextApiRequestCustom, res: NextApiResponseCustom) => {
  try {
    const baseURL = `${VaultURLs.API_URL}/api/auth/info`;
    const configRequest = req.getStructureDynamicRouterServer(baseURL);
    const data = getCookieToken({ req });
    if (!configRequest) return res.status(HTTP_STATUS.AUTHENTICATION_ERROR).json({ data: 'session expired', code: '00000' });
    if (!data) {
      clearCookieToken({ req, res });
      return res.status(HTTP_STATUS.OK).send(undefined);
    }
    const resp = await axios({
      url: baseURL,
      method: 'POST',
      headers: {
        Authorization: `bearer ${data?.token}`,
      },
    });
    return res.RESP(resp);
  } catch (e) {
    clearCookieToken({ req, res });
    return res.status(HTTP_STATUS.OK).send(undefined);
    // const { status, message, data } = exception(e);
    // return res.ERROR(status, message, data);
  }
};

export default withMiddleware(handler);
