/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useCallback } from 'react';
import { menuConfig } from '~/components/config';

const MENU_EXCEPTION = ['tv-series', 'movies'];

const MenuMobile = ({ subMenus }: any) => {
  const [menuActive, setMenuActive] = useState('');
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const onClick = useCallback((event) => {
    if (menuActive && menuActive === event.target.title) {
      setMenuActive('');
    } else {
      setMenuActive(event.target.title);
    }
  }, [menuActive]);

  const onShowMenu = useCallback(() => {
    setShowMenu(!showMenu);
  }, [showMenu]);
  return (
    <div id="site_menu" className={`${showMenu ? 'active' : ''}`}>
      <div onClick={onShowMenu} className="site_menu-icon"><i className="fa fa-bars" /></div>
      <div id="header_menu" className={`${showMenu ? 'active' : ''}`}>
        <ul className="nav header_menu-list">
          {menuConfig.map((menu: any) => {
            const activeSubMenu = menuActive === menu.name ? 'active extend' : '';
            const subMenusList = (subMenus[menu.key] || menu.subMenus) || [];
            const addSub = MENU_EXCEPTION.includes(menu.key) ? subMenus?.genres : [];
            const newSub = subMenusList.concat(addSub);
            return (
              <div key={menu.name}>
                <li className="nav-item" onClick={onClick}>
                  {menu.mobileLink ? (
                    <a href={menu.mobileLink} title={menu.name}>{menu.name}</a>
                  ) : <a title={menu.name}>{menu.name}</a>}
                </li>
                {menu.hasSubMenu ? (
                  <div className={`header_menu-sub size-l ${activeSubMenu}`}>
                    <ul className="sub-menu">
                      {newSub?.map((sub: any, idx) => {
                        let subLink = `${menu?.subLink}${sub?.slug || ''}`;
                        if (sub?.subKey === 'genres') {
                          subLink = `/genre/${menu.key}/${sub?.slug}`;
                        }
                        return (
                          <li key={idx}>
                            <a href={`${subLink}`} title={sub?.name}>{sub?.name}</a>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                ) : null}
              </div>
            );
          })}
        </ul>
      </div>
      <div className="clearfix" />
    </div>
  );
};

export default MenuMobile;
