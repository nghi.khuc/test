/* eslint-disable max-len */
/* eslint-disable react/no-unescaped-entities */
import { GetServerSideProps } from 'next';
import React from 'react';
import { parseStringToObject } from '~/utils/common';
import { KEYS_REDIS } from '~/utils/config';
import { redisGetAsync } from '~/utils/redis';
// import '../styles/home.css';

export default function faq({ contentFaqParse }) {
  return (
    <div>
      <div className="information_page">
        <div className="container">
          <div className="information_page-wrap">
            <div className="information_page-content">
              <h2 className="h2-heading">FAQ</h2>
              <div className="list-group">
                {!!contentFaqParse && <div dangerouslySetInnerHTML={{ __html: contentFaqParse }} />}
                {/* <div className="list-group-item flex-column align-items-start ">
                  <div className="d-flex w-100 justify-content-between">
                    <h4 className="mb-1">Is it legal to watch anime on 9anime.to ?</h4>
                  </div>
                  <p className="mb-1">Absolutely, watching is totally legal. We also do not host the anime, we link to them.</p>
                </div>
                <div className="list-group-item flex-column align-items-start">
                  <div className="d-flex w-100 justify-content-between">
                    <h4 className="mb-1">I can't find my favorite anime, what can I do ?</h4>
                  </div>
                  <p className="mb-1">Please use the Request form and we will upload these anime for you.</p>
                </div>
                <div className="list-group-item flex-column align-items-start">
                  <div className="d-flex w-100 justify-content-between">
                    <h4 className="mb-1">About banner issues</h4>
                  </div>
                  <p className="mb-1">We do not offer supports about banning issues, because no one can prove what's you said is the truth or not. We only support members who contributed to the sites in the past.</p>
                </div>
                <div className="list-group-item flex-column align-items-start">
                  <div className="d-flex w-100 justify-content-between">
                    <h4 className="mb-1">About sexy ads</h4>
                  </div>
                  <p className="mb-1">It's the only ads pirate sites could use, clean ads are banned by copyright owners. However we could remove the extreme one, please report by send us a mail to contact@9anime.to with ads that is too extreme sexy screenshot and ads url. </p>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { query } = ctx;
  const keyContentFaq = KEYS_REDIS.FAQ_CONTENT;
  const contentFaq = await redisGetAsync(keyContentFaq);
  const contentFaqParse = parseStringToObject(contentFaq);
  return {
    props: {
      query,
      contentFaqParse: contentFaqParse?.content || '',
    },
  };
};
