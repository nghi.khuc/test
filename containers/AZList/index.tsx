/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react';
import ListChars from 'components/Footer/ListChars';
import LazyLoad from 'react-lazyload';
import Paginate from './paginate';
import { buildUrl } from '~/utils/common';

const AZList = ({ response = {}, slug }: any) => (
  <section className="block_area block_area-anime">
    <div className="block_area-content block_area-list film_list film_list-v film_list-az">
      <ListChars slug={slug} />
      <div className="anime-block-ul">
        <ul className="ulclear">
          {(response?.data || []).map((item) => (
            <LazyLoad key={item.original_title} placeholder="......">
              <li>
                <div className="film-poster item-qtip" data-id="6074" data-hasqtip="0" aria-describedby="qtip-0">
                  <LazyLoad placeholder={<img data-src={item.poster} className="film-poster-img " alt={item.original_title} src={item.poster} />}>
                    <img data-src={item.poster} className="film-poster-img lazyloaded" alt={item.original_title} src={item.poster} />
                  </LazyLoad>
                </div>
                <div className="film-detail">
                  <h3 className="film-name">
                    <a
                      href={`${buildUrl(item)}`}
                      title={item.original_title}
                      className="dynamic-name"
                      data-jname="12-sai.: Chicchana Mune no Tokimeki"
                    >
                      {item.original_title}
                    </a>
                  </h3>
                  <div className="fd-infor">
                    <span className="fdi-item">{item.release_year}</span>
                    <span className="dot" />
                    <span className="fdi-item fdi-duration">
                      {item.total_episodes ? `Ep ${item.episode_new || item.total_episodes}/${item.total_episodes}` : 'Ep Full'}
                    </span>
                  </div>
                </div>
                <div className="clearfix" />
              </li>
            </LazyLoad>
          ))}
        </ul>
        <div className="clearfix" />
      </div>
      <div className="clearfix" />
      {response?.page ? (
        <Paginate response={response} />
      ) : null}
    </div>
  </section>
);

export default AZList;
