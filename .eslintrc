{
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": ["airbnb",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true,
      "tsx": true
    },
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": ["react", "@typescript-eslint"],
  "settings": {
    "import/resolver": {
      "typescript": {}
    }
  },
  "rules": {
    "no-nested-ternary": 0,
    "no-underscore-dangle": 0,
    "jsx-a11y/no-noninteractive-element-interactions": 0,
    "react/no-array-index-key": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "no-param-reassign": 0,
    "eslint-disable no-param-reassign": 0,
    "react/no-danger": [0], // we will remove affter
    "import/no-extraneous-dependencies": ["off", { "devDependencies": ["**/*.test.js", "**/*.spec.js"] }],
    "@typescript-eslint/no-var-requires": "off",
    "no-tabs": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/no-empty-interface": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "indent": ["error", 2],
    "no-use-before-define": "off",
    "react/react-in-jsx-scope": "off",
    "linebreak-style": 0,
    "camelcase": "off",
    "radix": "off",
    "react/jsx-indent": ["error", 2],
    "react/jsx-indent-props": ["error", 2],
    "react/jsx-props-no-spreading": [0],
    "react/jsx-fragments": ["error", "element"],
    "class-methods-use-this": "off",
    "import/prefer-default-export": "off",
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        "js": "never",
        "mjs": "never",
        "jsx": "never",
        "ts": "never",
        "tsx": "never"
      }
    ],
    "max-len": [
      "error",
      150,
      2,
      {
        "ignoreUrls": true,
        "ignoreComments": false,
        "ignoreRegExpLiterals": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true
      }
    ],
    "react/jsx-wrap-multilines": ["error", {
      "arrow": true,
      "return": true,
      "declaration": true
    }],
    "react/jsx-filename-extension": [1, { "extensions": [".jsx", ".tsx", "ts", "js"] }]
  }
}
