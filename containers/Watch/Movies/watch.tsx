/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ReactJWPlayer from 'react-jw-player';
import { useRouter } from 'next/router';
import isEmpty from 'lodash/isEmpty';

import BlockControl from '../BlockControl';
import { ItemFilm } from '~/stores/slices/FilmSlice/types';
import { postErrorFilm } from '~/services/client';
// import LazyLoad from 'react-lazyload';

const ContainerWPCover = styled.div`
  background-image: url(${(props) => props.backdrop});
`;
interface IStreamData {
  streamurls: string[],
  captions: string[]
}
interface IParams {
  itemFilm: ItemFilm,
  episode: number | string,
  ss: number,
  sv: number,
  seasons: [],
  streamUrls: any
  underPlayer: string
  captions: any[]
  openModal: () => void
}

const Movies = ({
  episode, ss, sv, itemFilm, seasons, streamUrls, underPlayer, captions, openModal,
}: IParams) => {
  const router = useRouter();
  const [stateLight, setLight] = useState('off');
  const [serverActive, setServerActive] = useState(sv);
  const [isAutoPlay, setAutoPlay] = useState(true);
  const [loading, setLoading] = useState(false);
  const [episodeState, setEpisodeState] = useState(episode);

  useEffect(() => {
    setServerActive(+sv);
  }, [sv]);

  const onToggleLight = () => {
    setLight('');
    const element: any = document?.getElementById('mask-overlay');
    element.className += 'active';
  };

  useEffect(() => {
    const isAutoPlayLocal = localStorage.getItem('isAutoPlay');
    if (isAutoPlayLocal) {
      setAutoPlay(JSON.parse(isAutoPlayLocal));
    }
  }, []);

  const handleAutoPlay = () => {
    setAutoPlay(!isAutoPlay);
    localStorage.setItem('isAutoPlay', JSON.stringify(!isAutoPlay));
  };

  const onChangeEpisode = async (epi, season) => {
    // const id = itemFilm._id;
    // setLoading(true);
    // setEpisodeState(epi);

    // let url = `/${itemFilm.type}/${itemFilm.slug}-${id}?ep=${(epi)}&sv=${serverActive}`;
    // if (season && season > 1) {
    //   url += `&ss=${season}`;
    // }
    // router.push(url);

    // setLoading(false);
  };

  const playlist = [{
    file: streamUrls?.url,
    image: itemFilm.backdrop,
    tracks: captions?.map((caption, index) => ({
      file: `${window.location.origin}/captions/${caption.name.toUpperCase()}/${caption.filename}`,
      label: caption.name.toUpperCase(),
      kind: 'captions',
      default: !index,
    })) || [],
  }];

  const changeServer = (id) => {
    setLoading(true);
    // const url = `/${itemFilm.type}/${itemFilm.slug}-${itemFilm._id}.html?ep=${(episode)}&sv=${id}`;
    const url = `/${itemFilm.type}/${itemFilm.slug}.${itemFilm._id}/server-${id}/ep-full.html`;
    setTimeout(() => {
      setServerActive(id);
      setLoading(false);
      router.push(url);
    }, 500);
  };
  return (
    <React.Fragment>
      <div id="watch-block" className="">
        <div className="player-wrap">
          <div className="wb_-playerarea">
            <ContainerWPCover
              backdrop={itemFilm.backdrop}
              className="wb__-cover"
            />
            {(loading || isEmpty(seasons)) ? (
              <div className="loading-relative loading-box" id="embed-loading">
                <div className="loading">
                  <div className="span1" />
                  <div className="span2" />
                  <div className="span3" />
                </div>
              </div>
            ) : (
              <div style={{ position: 'absolute', width: '100%', height: '100%' }}>
                <ReactJWPlayer
                  onError={() => postErrorFilm({
                    movieType: 'movies',
                    movieID: itemFilm._id,
                    server: sv,
                    episodeId: itemFilm?._id,
                  })}
                  onOneHundredPercent={openModal}
                  playerId={itemFilm.slug}
                  isAutoPlay={isAutoPlay}
                  playerScript="https://content.jwplatform.com/libraries/j9BLvpMc.js"
                  playlist={playlist}
                />
              </div>
            )}
          </div>
        </div>
        <BlockControl
          stateLight={stateLight}
          isAutoPlay={isAutoPlay}
          setAutoPlay={handleAutoPlay}
          episodeState={episodeState}
          onToggleLight={onToggleLight}
          onChangeEpisode={onChangeEpisode}
          ss={ss}
        />
      </div>
      {/** Block tool light, expand ,... */}

      {/** Block swtch server */}
      <div className="player-servers">
        <div id="servers-content">
          <div className="ps_-status">
            <div className="content">
              <div className="server-notice">
                <strong>
                  You are watching
                  <b>
                    {` Episode ${episodeState}`}
                  </b>
                  .
                </strong>
                {`
                If current
                server doesn't
                work please try other servers below
                `}

              </div>
            </div>
          </div>
          <div className="ps_-block ps_-block-sub servers-sub">
            <div className="ps__-title">
              Server:
            </div>
            <div className="ps__-list">
              {(Array.isArray(seasons) ? seasons : [seasons])?.map((season: any, idx) => (
                <div onClick={() => { changeServer(+idx + 1); }} key={idx} className="item server-item" data-type="sub" data-id="392375" data-server-id="4">
                  <a className={`btn ${serverActive === (idx + 1) ? 'active' : ''}`}>{season?.server?.name}</a>
                </div>
              ))}
            </div>
            <div className="clearfix" />

          </div>

        </div>
      </div>
      {
        !!underPlayer && <div className="mba-block" dangerouslySetInnerHTML={{ __html: underPlayer }} />
      }

      <section className="block_area block_area-episodes">
        <div className="block_area-content">
          <div
            key="episodes-page-1"
            id="episodes-page-1"
            className="episodes-ul"
            data-page="1"
          >
            <a
              href={`/${itemFilm.type}/${itemFilm.slug}-${itemFilm._id}?ep=${episodeState}&sv=${serverActive}`}
              title={itemFilm.title}
              className="item ep-item active"
              data-number={1}
              data-id={1}
            >
              <div className="order">{1}</div>
            </a>
            <div className="clearfix" />
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default Movies;
