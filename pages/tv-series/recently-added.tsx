/* eslint-disable react/button-has-type */
import React from 'react';
import { GetServerSideProps } from 'next';
import Component from 'components/Filter';

export function FilterPage(props) {
  return (
    <Component {...props} type="tv-series" />
  );
}
export default FilterPage;

export const getServerSideProps: GetServerSideProps = async () => {
  const paramrQuery = {
    page: 1, length: 20, sort: 'created_at', option: 'tv-series',
  };

  return {
    props: {
      query: paramrQuery,
    },
  };
};
