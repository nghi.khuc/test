import Container from 'containers/Watch/Movies';
import { GetServerSideProps } from 'next';
import React from 'react';
import { getInfoMovieFilm } from '~/pages/api/movies/[id]';
import { buildTitle, parseStringToObject } from '~/utils/common';
import { KEYS_REDIS } from '~/utils/config';
import { CONFIG_TITLE_DES_PAGE, TYPE_ADS_PAGE } from '~/utils/contains';
import { redisGetAsync, redisGetValAsync } from '~/utils/redis';

function Watch(props) {
  return (
    <Container {...props} />
  );
}

export const getServerSideProps: GetServerSideProps = async (props) => {
  const { slug }: any = props.query;
  // const [id] = (slug[0] || '').toString().split('-').reverse();
  const [id] = (slug[0] || '').toString().split('.').reverse();
  // const [epName] = (slug[2] || '').split('-').reverse();

  const titleKey = CONFIG_TITLE_DES_PAGE.movie.key;

  const [underPlayer, topInfoFilm, botInfoFilm, resp, doc] = await Promise.all([
    redisGetAsync(TYPE_ADS_PAGE.watch_under_player.key),
    redisGetAsync(TYPE_ADS_PAGE.top_information_film.key),
    redisGetAsync(TYPE_ADS_PAGE.under_information_film.key),
    getInfoMovieFilm(id, true),
    redisGetValAsync(titleKey),
  ]);

  const underPlayerParse = parseStringToObject(underPlayer);
  const topInfoFilmParse = parseStringToObject(topInfoFilm);
  const botInfoFilmParse = parseStringToObject(botInfoFilm);

  const {
    title: TitleData, description, release_year, quality,
  } = resp.data.data;

  const title = buildTitle(doc?.title || '', { title: TitleData, release_year, quality });
  // (doc?.title || '')
  // .replace(/\[Title]/g, TitleData)
  // .replace(/\[Release_Year]/g, release_year)
  // .replace(/\[Quality]/g, quality)
  // .replace(/\[Ep_Name]/g, epName);

  return {
    props: {
      title,
      description,
      itemFilm: resp.data.data,
      underPlayer: underPlayerParse?.content || '',
      topInfoFilm: topInfoFilmParse?.content || '',
      botInfoFilm: botInfoFilmParse?.content || '',
    },
  };
};

export default Watch;
