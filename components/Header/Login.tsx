/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import { useRouter } from 'next/router';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getInfoAction, logoutAction } from '~/stores/slices/UserSlices';
import { URLs } from '~/utils/contains';

const Login = ({ onShowSearchMobile }:any) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((props:any) => props.userSlices?.user);

  React.useEffect(() => {
    dispatch(getInfoAction());
  }, []);

  return (
    <div id="header_right">
      <div id="m-search" onClick={onShowSearchMobile} className="mobile-btn"><i className="fas fa-search" /></div>
      <div id="user-slot">
        <div className={`header_right-user ${user ? 'logged' : ''}`}>
          {
            user ? (
              <div className="dropdown">
                <button type="button" className="btn-user btn btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>{user.fullname || user.username}</span></button>
                <div className="dropdown-menu">
                  <span
                    className="dropdown-item"
                    onClick={() => {
                      window.location.href = `${URLs.userProfile}/${user?.fullname || user?.username}.${user?.id}`;
                    }}
                  >
                    <i className="fas fa-user mr-2" />
                    Profile
                  </span>
                  <span
                    className="dropdown-item"
                    onClick={() => {
                      window.location.href = `${URLs.userProfile}/${user?.fullname || user?.username}.${user?.id}/watch-list`;
                    }}
                  >
                    <i className="fas fa-heart mr-2" />
                    Watch List
                  </span>
                  <span
                    className="dropdown-item"
                    onClick={async () => {
                      await dispatch(logoutAction());
                      if (/(person.*$|user.*$)/i.test(router.pathname)) {
                        window.location.href = URLs.home;
                      }
                    }}
                  >
                    <i className="fas fa-sign-out-alt mr-2" />
                    Logout
                  </span>
                </div>
              </div>
            ) : <a data-toggle="modal" data-target="#modallogin" className="btn-user btn btn-sm btn-primary btn-login">Sign in</a>
          }
        </div>
      </div>
      <div className="clearfix" />
    </div>
  );
};
export default Login;
