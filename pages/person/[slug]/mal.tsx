/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
import Container from 'containers/User/Mal';
import React from 'react';
import { useSelector } from 'react-redux';

export function Page() {
//   const router = useRouter();
  const user = useSelector((props:any) => props.userSlices?.user);

  const getInfo = async () => {
    // const data = await getUserProfileFavorite();
    // const { slug } = router.query;
    // const [, id] = (slug || '').toString().split('.');
    // const [info, films] = await Promise.all([getUserProfileWatchList(id), getUserProfileWatchListFilms(id)]);
    // setUser({
    //   ...info.data,
    //   movies: films.data?.movies?.map((film) => ({ ...film, type: 'movies' })) || [],
    //   tvseries: films.data?.tvseries?.map((film) => ({ ...film, type: 'tv-series' })) || [],
    // });
  };

  React.useEffect(() => {
    getInfo();
  }, []);
  return (
    <Container user={user} />
  );
}

Page.PAGE_NOT_USE_ASIDE = true;

export default Page;
