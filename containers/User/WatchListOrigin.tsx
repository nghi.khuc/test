/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import LazyLoad from 'react-lazyload';
import { postLogout } from '~/services/client';
import { buildUrl } from '~/utils/common';
import Paginate from './Pagination';

const WatchListOrigin = ({
  user, changeTab, tab, setPage, page, state, deleteFilm,
}: any) => (

  <div className="profile-container">
    <div className="prc_-menu">
      <div className="prc__-icon"><img src={user?.avatar || '/images/user-icon.png'} alt="avatar" /></div>
      <div className="prc__-list">
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}`} className="btn">Edit Profile</a>
        </div>
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}/watch-list`} className="btn active">Watch List</a>
        </div>
        <div className="item">
          <a href={`/person/${user?.fullname || user?.username}.${user?.id}/mal`} className="btn ">MAL Import</a>
        </div>
        <div
          className="item"
          onClick={async () => {
            await postLogout();
            setTimeout(() => {
              window.location.href = '/home';
            }, 500);
          }}
        >
          <a href="#" className="btn">Logout</a>
        </div>
        <div className="clearfix" />
      </div>
    </div>
    <div className="prc_-main">
      <section className="block_area block_area-profile">
        <div className="block_area-header block_area-header-tabs">
          <div className="float-left bah-heading mr-4">
            <h2 className="cat-heading">Watch list</h2>
          </div>
          {/* <div className="float-right bah-tabs">
            <a href="/user/watch-list" className="btn btn-sm active">All</a>
            <a href="/user/watch-list?type=1&sort=" className="btn btn-sm ">Watching</a>
            <a href="/user/watch-list?type=2&sort=" className="btn btn-sm ">On-Hold</a>
            <a href="/user/watch-list?type=3&sort=" className="btn btn-sm ">Plan to watch</a>
            <a href="/user/watch-list?type=4&sort=" className="btn btn-sm ">Dropped</a>
            <a href="/user/watch-list?type=5&sort=" className="btn btn-sm ">Completed</a>
          </div> */}
          <div className="clearfix" />
        </div>
        <div className="block_area-content">
          <div className="description">
            <p className="mb-0">
              - Click to
              <i className="fas fa-folder" />
              {' '}
              icon to move anime to another
              folder.
            </p>
            <p className="mb-0">- Click to Watched/Unwatched to change watch status.</p>
          </div>
          <div className="prc__-filter">
            <div className="item item-sort">
              <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="btn btn-sm">
                Option
                <span>
                  {tab === 'movies' ? 'Movies' : 'TV'}
                  <i className="fas fa-angle-down ml-1" />
                </span>
              </a>
              <div className="dropdown-menu dropdown-menu-native">
                <span className={`dropdown-item ${tab === 'movies' ? 'active' : ''}`} onClick={() => changeTab('movies')}>
                  <i className="fas fa-circle" />
                  Movies
                </span>
                <span className={`dropdown-item ${tab === 'tv' ? 'active' : ''}`} onClick={() => changeTab('tv')}>
                  <i className="fas fa-circle" />
                  {' '}
                  TV
                </span>
              </div>
            </div>
            <div className="clearfix" />
          </div>

          {/* <div className="prc__-filter">
            <div className="item">
              <a href="/user/watch-list/random" className="btn btn-sm">Watch a random</a>
            </div>
            <div className="item item-sort">
              <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="btn btn-sm">
                Sort
                <span>
                  Default
                  <i className="fas fa-angle-down ml-1" />
                </span>
              </a>
              <div className="dropdown-menu dropdown-menu-native">
                <a className="dropdown-item active" href="/user/watch-list?type=&sort=">
                  <i className="fas fa-circle" />
                  Default
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=recently-added">
                  <i className="fas fa-circle" />
                  {' '}
                  Recently
                  Added
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=recently-updated">
                  <i className="fas fa-circle" />
                  {' '}
                  Recently
                  Updated
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=score">
                  <i className="fas fa-circle" />
                  {' '}
                  Score
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=a-z">
                  <i className="fas fa-circle" />
                  {' '}
                  Name
                  A-Z
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=released-date">
                  <i className="fas fa-circle" />
                  {' '}
                  Released
                  Date
                </a>
                <a className="dropdown-item " href="/user/watch-list?type=&sort=most-watched">
                  <i className="fas fa-circle" />
                  {' '}
                  Most
                  Watched
                </a>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          */}
          <div className="film_list-az">
            <div className="anime-block-ul">
              <ul className="ulclear">
                {
                  (state?.data || []).map((film: any, index) => (
                    <li key={index}>
                      <div className="film-poster">
                        <LazyLoad placeholder={<img className="film-poster-img ls-is-cached lazyloaded" alt={film.title} src="/images/black-slider.jpeg" />}>
                          <img className="film-poster-img ls-is-cached lazyloaded" alt={film.title} src={film.poster} />
                        </LazyLoad>
                      </div>
                      <div className="film-detail">
                        <div className="btn-remove-fav wl-item" onClick={() => deleteFilm(film._id)}><i className="fas fa-times" /></div>
                        <h3 className="film-name">
                          <a href={`/${buildUrl(film)}`} title={film.title} data-jname={film.title} className="dynamic-name">{film.title}</a>
                        </h3>
                        <div className="fd-infor">
                          <span className="fdi-item mr-2">
                            {`Rating ${film.rating}`}
                          </span>
                          <span className="fdi-item mr-2">
                            <i className="fas fa-bookmark" />
                            {' '}
                            1
                          </span>
                          <span data-id={8933} className="fdi-item watch-status item-watched">
                            <i className="fas fa-eye" />
                            {' '}
                            <span id="watch-status-text">Watched</span>
                          </span>
                        </div>
                      </div>
                      <div className="clearfix" />
                    </li>
                  ))
                }
              </ul>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <Paginate response={state} setPage={setPage} />
      <div className="anime-pagination" />
    </div>
    <div className="clearfix" />
  </div>
);
export default WatchListOrigin;
