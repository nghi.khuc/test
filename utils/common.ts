import isNaN from 'lodash/isNaN';
import moment from 'moment';
import publicIP from 'public-ip';

export const getIP = () => {
  if (process.env.NEXT_PUBLIC_ENV === 'dev') return '127.0.0.1';
  return publicIP.v4();
};

export const convertSecondtoTime = (time) => {
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor((time - (hours * 3600)) / 60);
  const seconds = time - (hours * 3600) - (minutes * 60);
  const secondString = seconds < 10 ? `0${seconds}` : seconds;
  const minuteString = minutes < 10 ? `0${minutes}` : minutes;
  // const hourString = hours < 10 ? `0${hours}` : hours;
  return (`${minuteString}:${secondString}`);
};
export const parseStringToObject = (str) => (str ? JSON.parse(str) : null);

/* eslint-disable no-nested-ternary */
interface IToastMessageSuccess {
  isSuccess?: boolean,
  isError?: boolean,
  content: string
  isWarning?: boolean
}

export const toastMessage = ({
  isSuccess = true, isError = false, isWarning = false, content,
}: IToastMessageSuccess) => {
  if (typeof document !== undefined) {
    const alertExists = document.getElementById('toast-container');
    if (alertExists) {
      alertExists.remove();
    }
    let toastStyle = '';
    if (isSuccess) {
      toastStyle = 'toast-success';
    }
    if (isWarning) {
      toastStyle = 'toast-warning';
    }
    if (isError) {
      toastStyle = 'toast-error';
    }
    const alert = document.createElement('div');
    alert.id = 'toast-container';
    alert.className = 'toast-bottom-right';
    const alertAppend = document.createElement('div');
    alertAppend.className = `toast ${toastStyle}`;
    alertAppend.style.display = 'block';
    const alertAppendContent = document.createElement('div');
    alertAppendContent.className = 'toast-message';
    alertAppend.appendChild(alertAppendContent);
    alert.appendChild(alertAppend);
    alertAppendContent.innerHTML = content || 'Successfully';
    document.body.appendChild(alert);
    setTimeout(() => {
      alert.remove();
    }, 3000);
  }
};

export const convertStringUpperCaseFirstChar = (str: string) => {
  if (!str) return '';
  str = decodeURI(str);
  const splitSrt = str.split(/(-|_)/g);
  const newStr = /(\d+)/i.test(splitSrt[splitSrt.length - 1]) ? splitSrt.slice(0, splitSrt.length - 1).join(' ') : splitSrt.join(' ');
  return (newStr || str)
    .replace(/-/g, ' ')
    .replace(/_/g, ' ')
    .replace(/oe/g, 'ö')
    .replace(/ae/g, 'ä')
    .replace(/ue/g, 'ü')
    .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
};

export const getDaysArray = () => {
  const year = moment().year();
  const monthIndex = moment().month(); // 0..11 instead of 1..12
  const names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const date: any = new Date(year, monthIndex, 1);
  const monthName = monthNames[date.getMonth()];
  const result: any = [];
  while (date.getMonth() === monthIndex) {
    result.push({
      currentDate: date.getDate(),
      dayisoString: moment(date).format('DD/MM/YYYY'),
      day: `${monthName} ${date.getDate()}`,
      week: names[date.getDay()],
    });
    date.setDate(date.getDate() + 1);
  }
  return result;
};

export const buildUrl = (item) => {
  const seasonNew = item?.season_new || item?.season || 1;
  let url = '';
  const nameFlilm = item.slug || (item.title || '').replace(/ /g, '-');
  if (item.type === 'tv-series') {
    url = `/${item.type}/${nameFlilm}.${item?._id}/server-1/ep-1.html?${seasonNew && seasonNew > 1 ? `&ss=${seasonNew}` : ''}`;
  } else {
    url = `/${item.type}/${nameFlilm}.${item?._id}/server-1/ep-full.html`;
  }
  return url;
};

const REGEX_SV_EP = /\/server-(\d+)\/ep-(\d+|\w+)/i;

export const parseURLWatch = (url) => {
  const matches = url.match(REGEX_SV_EP);
  if (matches) {
    const [, server, epi] = matches;

    const sv = isNaN(+server) ? 1 : +server;
    const ep = isNaN(+epi) ? 1 : +epi;

    return {
      ep,
      sv,
    };
  }

  return {
    ep: 1,
    sv: 1,
  };
};

interface IBuildTitle {
  title: string
  release_year: number
  quality: string
  epText?: string
}

export const buildTitle = (titleString: string, {
  title, release_year, quality, epText = '',
}:IBuildTitle) => titleString.replace(/\[Title]/g, title)
  .replace(/\[Release_Year]/g, `${release_year}`)
  .replace(/\[Quality]/g, quality)
  .replace(/\[Ep_Name]/g, epText);
